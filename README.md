# Scheduler

Just a library to schedule some job executions with dependencies between each others.<br/>
The whole displayed in a visual way, allowing to see, in a quick glance, if something is wrong.

Jobs can execute any command lines, shares limited resources (lock)<br/>
Arrows indicate "source job(s) has/have to be executed successfuly/in error/any to execute the targeted job"<br/>
UI allows to see all executions at bottom, open execution with output & error streams reset task and run them again.<br/>
Some builtins job kind allows user to do loops and "waiting to a specific hour" job<br/>

![](images/default_sample_loaded_in_vs.gif)