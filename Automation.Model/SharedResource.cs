﻿using System;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;


namespace Automation.Model
{
    public interface ISharedResource : IHasTypedIdReadable, ICopyable<ISharedResource>, ICloneable
    {
        string Name                 { get; set; }
        uint   MaximumConcurrentUse { get; set; }
        string Comment              { get; set; }
    }
    public class SharedResource : ISharedResource, ITreeNodeWithParentReadable, IIsNamed
    {
        public string                 Name                 { get; set; }
        public uint                   MaximumConcurrentUse { get; set; } = 1;
        public string                 Comment              { get; set; }

        string                      IIsNamed.Name                                { get { return Name + " (" + MaximumConcurrentUse + ")"; } }
        IIdTuple                    IHasClosedIdReadable.Id                      { get { return new IdTuple<string>(Name); } }
        ITypedId                    IHasTypedIdReadable.TypedId                  { get { return (this as IHasTypedIdReadable).MakeTypedId((this as IHasClosedIdReadable).Id); } }
        ITypedId                    IHasTypedIdReadable.MakeTypedId(IIdTuple id) { return id.ToTypedId(typeof(SharedResource)); }
        ITreeNodeWithParentReadable ITreeNodeWithParentReadable.ParentNode       { get { return null; } }

        void ICopyable.CopyFrom(ICopyable source)
        {
            (this as ICopyable<ISharedResource>).CopyFrom(source as ISharedResource);
        }
        void ICopyable<ISharedResource>.CopyFrom(ISharedResource source)
        {
            Name = source.Name;
            MaximumConcurrentUse = source.MaximumConcurrentUse;
            Comment = source.Comment;
        }
        public SharedResource Clone()
        {
            var clone = new SharedResource();
            (clone as ICopyable<ISharedResource>).CopyFrom(this);
            return clone;
        }
        object ICloneable.Clone() { return Clone(); }

        #region Export

        public static SharedResource FromXml(XElement nResource)
        {
            var resource = new SharedResource();
            // ReSharper disable PossibleNullReferenceException
            resource.Name = nResource.Element("Name").Value;
            resource.MaximumConcurrentUse = uint.Parse(nResource.Element("MaximumConcurrentUse").Value);
            resource.Comment = nResource.Element("Comment").Value;
            return resource;
        }

        public XElement ToXml(XElement nResource = null)
        {
            nResource = nResource ?? new XElement("Resource");
            nResource.Add(new XElement("Name", Name));
            nResource.Add(new XElement("MaximumConcurrentUse", MaximumConcurrentUse.ToStringInvariant()));
            nResource.Add(new XElement("Comment", Comment));
            return nResource;
        }

        #endregion Export
    }
}
