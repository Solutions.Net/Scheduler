﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

using TechnicalTools.Diagnostics;

using ThreadState = System.Threading.ThreadState;


namespace Automation.Model
{
    public class GlobalJobScheduler : INotifyPropertyChanged // TODO : Ajouter IDisposed pour plus de propreté...
    {
        public JobDiagram ReferenceDiagram { get; private set; }
        public XElement Layouts { get; private set; }

        // Return only ThreadState.Running or ThreadState.StopRequested or ThreadState.Stopped
        public ThreadState State { get { return _State; } private set { _State = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State))); } } ThreadState _State = ThreadState.Stopped;
        public event PropertyChangedEventHandler PropertyChanged;

        public ConcurrentDictionary<DateTime, DailyJobScheduler> Schedulers { get; } = new ConcurrentDictionary<DateTime, DailyJobScheduler>();
        public DailyJobScheduler LastScheduler { get { return Schedulers.OrderByDescending(kvp => kvp.Key).FirstOrDefault().Value; } }

        public event Action<object, DailyJobScheduler> SchedulerCreated;
        public event Action<object, DailyJobScheduler> SchedulerRemoved;

        public GlobalJobScheduler(JobDiagram referenceDiagram, XElement layouts = null)
        {
            Load(referenceDiagram, layouts);
        }
        public void Load(JobDiagram referenceDiagram, XElement layouts = null)
        {
            lock (_lock)
            {
                if (ReferenceDiagram == null)
                    ReferenceDiagram = referenceDiagram;
                else
                    ReferenceDiagram.LoadFromXml(referenceDiagram.ToXml());
                Layouts = layouts;
            }
        }
        CancellationTokenSource _ctsScheduler;
        Thread _thScheduler;
        readonly object _lock = new object();

        public void Start()
        {
            lock (_lock)
            {
                if (_ctsScheduler != null)
                    return;
                _ctsScheduler = new CancellationTokenSource();
                _thScheduler = new Thread(MainLoop);
                _thScheduler.Start();
            }
        }

        public void Stop()
        {
            lock (_lock)
                _ctsScheduler?.Cancel();
            foreach (var s in Schedulers)
                s.Value.StopStartingNewJob();
        }
        public void Join()
        {
            foreach (var s in Schedulers)
                s.Value.Join();
            _thScheduler?.Join();
        }

        void MainLoop()
        {
            State = ThreadState.Running;
            while (true)
            {
                var now = DateTime.Now;
                var timeToWait = ReferenceDiagram.DailyTimeToInstanciate - now.TimeOfDay;
                if (timeToWait.TotalSeconds < 0)
                    timeToWait += TimeSpan.FromDays(1);

                var cancelled = _ctsScheduler.Token.WaitHandle.WaitOne(timeToWait);
                if (cancelled)
                {
                    State = ThreadState.StopRequested;
                    break;
                }
                // Because in some rare case, DateTime.Now can return some ms before the wanted time (DailyTimeToInstanciate),
                // which can be problematic when DailyTimeToInstanciate is midnight because we the date part is not the one expected
                Thread.Sleep(1000); 
                lock (_lock)
                {
                    InstanciateNow(false);
                }
            }

            LastScheduler?.StopStartingNewJob();
            foreach (var ds in Schedulers.Values)
                ds.StopStartingNewJob();
            foreach (var ds in Schedulers.Values)
                ds.Join();
            _ctsScheduler.Dispose();
            _ctsScheduler = null;
            State = ThreadState.Stopped;
        }

        public void InstanciateNow()
        {
            InstanciateNow(true);
        }
        internal void InstanciateNow(bool fromUserOrView)
        {
            if (Schedulers.Count >= ReferenceDiagram.DailyScheduleHistoryLength)
            {
                var toRemove = Schedulers.OrderBy(kvp => kvp.Key)
                                         .Where(kvp => kvp.Value.State == ThreadState.Stopped)
                                         .Take(Math.Max(0, Schedulers.Count - ReferenceDiagram.DailyScheduleHistoryLength.Value))
                                         .ToList();
                foreach (var kvp in toRemove)
                    RemoveScheduler(kvp.Value);
            }

            var startDate = fromUserOrView ? DateTime.Now : DateTime.Now.Date + ReferenceDiagram.DailyTimeToInstanciate;
            Console.WriteLine("Instanciating a daily scheduler at " + startDate);
            var dailyScheduler = new DailyJobScheduler(startDate, ReferenceDiagram);
            lock (_lock)
            {
                LastScheduler?.StopStartingNewJob();
                var success = Schedulers.TryAdd(dailyScheduler.StartDate, dailyScheduler);
                Debug.Assert(success);
            }
            // We notify view before running the scheduler...
            try { SchedulerCreated?.Invoke(this, dailyScheduler); }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Ignored error during event {nameof(SchedulerCreated)}:");
                Console.Error.WriteLine(ExceptionManager.Instance.Format(ex));
            }
            finally
            {
                // Because once the scheduler is run, it can send message to view too.
                // It is better these messages come after the restore layout event because this way, 
                // view can be fully built before receiving update message
                dailyScheduler.Run();
            }
        }

        public void RemoveScheduler(DailyJobScheduler scheduler)
        {
            if (Schedulers.TryRemove(scheduler.StartDate, out scheduler))
            {
                try { SchedulerRemoved?.Invoke(this, scheduler); }
                catch (Exception ex)
                {
                    Console.Error.WriteLine($"Ignored error during event {nameof(SchedulerRemoved)}:");
                    Console.Error.WriteLine(ExceptionManager.Instance.Format(ex));
                }
            }
        }
    }
}
