using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Algorithm.Graph;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace Automation.Model
{
    public class JobDiagram
    {
        // Must be unique
        public string                              Name                       { get { return _Name; } set { if (string.IsNullOrWhiteSpace(value)) throw new UserUnderstandableException("Name cannot be empty!", null); _Name = value; } } string _Name = "";
        public TimeSpan                            DailyTimeToInstanciate     { get; set; } // Indique quand une montée au plan doit être fait
        public string                              LogPath                    { get { return _LogPath; } set { _LogPath = value.EndsWith("\\") ? value : string.IsNullOrWhiteSpace(value) ? "" : value + "\\"; } } string _LogPath = "";
        public int?                                DailyScheduleHistoryLength { get; set; }
        public int?                                MaxExecutionContextLength  { get; set; }
        public AliasSet                            Aliases                    { get; private set; } = new AliasSet(new List<Alias>());
        public SharedResourceManager               ResourceManager            { get; private set; }


        public JobDiagram InstanciatedFrom       { get; private set; } // Indique si il s'agit d'une copie monté au plan

               FixedBindingListAdvanced<Job>           Jobs                        { get; } = new FixedBindingListAdvanced<Job>();
               FixedBindingListAdvanced<JobDependency> JobDependencies             { get; } = new FixedBindingListAdvanced<JobDependency>();

        public Dictionary<Job, List<JobDependency>>    JobDependenciesBySourceJob  { get; } = new Dictionary<Job, List<JobDependency>>();
        public Dictionary<Job, List<JobDependency>>    JobDependenciesByTargetJob  { get; } = new Dictionary<Job, List<JobDependency>>();

        public Job CreateNewJob()
        {
            Job job;
            lock (Jobs)
            {
                job = new Job(this)
                {
                    JobName = Jobs.CreateUnique(j => j.JobName, "New Job")
                };
                Jobs.Add(job);
            }
            return job;
        }
        public Job CloneJob(Job jobToClone)
        {
            Debug.Assert(jobToClone.OwnerGroup == this);
            Job clonedJob;
            lock (Jobs)
            {
                clonedJob = jobToClone.Clone(Jobs.CreateUnique(j => j.JobName, Regex.Replace(jobToClone.JobName, @" *([0-9]+|\([0-9]+\)) *$", "")));
                Jobs.Add(clonedJob);
            }
            return clonedJob;
        }
        public void RemoveJob(Job job)
        {
            lock (Jobs)
                Jobs.Remove(job);
        }
        public void AddJobDependency(JobDependency dep)
        {
            lock (JobDependencies)
                JobDependencies.Add(dep);
        }
        public void RemoveJobDependency(JobDependency dep)
        {
            lock (JobDependencies)
                JobDependencies.Remove(dep);
        }

        public JobDiagram()
        {
            ResourceManager = new SharedResourceManager(this, new List<SharedResource>());
            Jobs.BeforeListChange += Jobs_BeforeListChange;
            JobDependencies.ListChangeValidating += JobDependencies_ListChangeValidating;
            JobDependencies.AfterListChange += JobDependencies_AfterListChanged;
        }

        public IReadOnlyCollection<Job> GetJobsAndSubscribe(EventHandler<IAfterChangeEventArgs<Job>> handle)
        {
            lock (Jobs)
            {
                Jobs.AfterListChange += handle;
                return GetJobs();
            }
        }
        public IReadOnlyCollection<Job> GetJobs()
        {
            lock (Jobs)
            {
                return Jobs.ToList();
            }
        }
        public void UnsubscribeFromJobs(EventHandler<IAfterChangeEventArgs<Job>> handle)
        {
            lock (Jobs)
                Jobs.AfterListChange -= handle;
        }

        public IReadOnlyCollection<JobDependency> GetJobDependenciesAndSubscribe(EventHandler<IAfterChangeEventArgs<JobDependency>> handle)
        {
            lock (JobDependencies)
            {
                JobDependencies.AfterListChange += handle;
                return GetJobDependencies();
            }
        }
        public IReadOnlyCollection<JobDependency> GetJobDependencies()
        {
            lock (JobDependencies)
            {
                return JobDependencies.ToList();
            }
        }
        public void UnsubscribeFromJobDependencies(EventHandler<IAfterChangeEventArgs<JobDependency>> handle)
        {
            lock (JobDependencies)
                JobDependencies.AfterListChange -= handle;
        }

        void Jobs_BeforeListChange(object sender, IBeforeChangeEventArgs<Job> e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
                OnBeforeJobAdding(e.ItemAddedOrRemoved);
            else if(e.ListChangedType == ListChangedType.ItemDeleted)
                OnBeforeJobRemoving(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.Reset)
            {
                var removedJobs = JobDependenciesBySourceJob.Keys
                          .Concat(JobDependenciesByTargetJob.Keys)
                          .Except(GetJobs())
                          .ToList();
                foreach (var removedJob in removedJobs)
                    OnBeforeJobRemoving(removedJob);
                var addedJobs = GetJobs().Except(JobDependenciesBySourceJob.Keys)
                                         .Except(JobDependenciesByTargetJob.Keys)
                                         .ToList();
                foreach (var addedJob in addedJobs)
                    OnBeforeJobAdding(addedJob);
            }
            // No other kind of event to handle because properties of JobDependency class are immutable
        }

        void OnBeforeJobAdding(Job job)
        {
            job.PropertyChanging += Job_PropertyChanging;
        }
        void OnBeforeJobRemoving(Job job)
        {
            // ReSharper disable once DelegateSubtraction
            job.PropertyChanging -= Job_PropertyChanging;
            var lst = JobDependenciesBySourceJob.TryGetValueClass(job)?.ToList();
            if (lst != null)
                foreach (var jobDep in lst)
                    JobDependencies.Remove(jobDep);
            lst = JobDependenciesByTargetJob.TryGetValueClass(job)?.ToList();
            if (lst != null)
                foreach (var jobDep in lst)
                    JobDependencies.Remove(jobDep);
        }
        void Job_PropertyChanging(object sender, TechnicalTools.Model.PropertyChangingEventArgs e)
        {
            if (e.PropertyName == nameof(Job.JobKind) && (eJobKind)e.OriginalValue == eJobKind.Restart)
            {
                Debug.Assert((eJobKind)e.NewValue != eJobKind.Restart);
                Debug.Assert(CONTRACT_NoCycleInJobDiagramExceptThroughRestartTask);
                var ex = DetectCycle(ConsideringThisJobNotOfRestartKind: (Job)sender);
                if (ex != null)
                {
                    e.Cancel = true;
                    e.CancelException = new BusinessException("Please remove dependencies that cause cycle before changing the job kind!", null);
                    // e.CancelException.SetStackTrace(new StackTrace(true));
                    throw e.CancelException;
                }
            }
        }



        void JobDependencies_ListChangeValidating(object sender, IValidatingChangeEventArgs<JobDependency> e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                var jobDep = e.ItemAddedOrRemoved;
                var jobs = GetJobs();
                if (!jobs.Contains(jobDep.Source))
                    throw new UserUnderstandableException("You have to add '" + nameof(JobDependency.Source) + "' job prior to adding this dependency!", null);
                if (!jobs.Contains(jobDep.Target))
                    throw new UserUnderstandableException("You have to add '" + nameof(JobDependency.Target) + "' job prior to adding this dependency!", null);
                var jobDepExisting = JobDependenciesBySourceJob.TryGetValueClass(jobDep.Source)?.FirstOrDefault(jd => jd.Target == jobDep.Target);
                if (jobDepExisting != null)
                    throw new UserUnderstandableException("This dependency already exist!", null);
                Debug.Assert(CONTRACT_NoCycleInJobDiagramExceptThroughRestartTask);
                var ex = DetectCycle(jobDep);
                if (ex != null)
                    throw new UserUnderstandableException("Adding this dependency would create cycle in job diagram!", null);
            }
            else if (e.ListChangedType == ListChangedType.Reset)
            {
                DebugTools.Break("Oh gosh...");
                // TODO : Consistency check
                // If Reset event is from a call to method Clear, there is nothing to do
                // If it is a RaiseListChangedEvents = false + lot of changes then... 
                // ... we are screwed at the moment, because we cannot forbid a bad set 
                // of dependencies to be loaded in the list in this event.
                // The probability for this to happen is low anyway.
            }
        }
        // Allow to ensure CONTRACT_NoCycleInJobDiagramExceptWithRestartTask;
        Exception DetectCycle(JobDependency withThisJobDepMore = null, Job ConsideringThisJobNotOfRestartKind = null)
        {           
            Func<Job, IEnumerable<JobDependency>> getEdgesStartingFrom =
                jobSource => jobSource.JobKind == eJobKind.Restart && jobSource != ConsideringThisJobNotOfRestartKind ? Enumerable.Empty<JobDependency>() :
                            (JobDependenciesBySourceJob.TryGetValueClass(jobSource) ?? Enumerable.Empty<JobDependency>())
                            .Concat(withThisJobDepMore != null && withThisJobDepMore.Source == jobSource ? new [] { withThisJobDepMore } : new JobDependency[0]);
       
            try { TopologicalOrderSimple.DoTopologicalSort(GetJobs(), job => getEdgesStartingFrom(job).Select(jd => jd.Target), true); }
            catch (TopologicalOrderSimple.HasCycleException<Job> ex)
            {
                return ex;
            }
            return null;
        }
        public const bool CONTRACT_NoCycleInJobDiagramExceptThroughRestartTask = true;

        void JobDependencies_AfterListChanged(object sender, IAfterChangeEventArgs<JobDependency> e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                var jobDep = e.ItemAddedOrRemoved;
                JobDependenciesBySourceJob.GetValueOrCreateDefault(jobDep.Source).Add(jobDep);
                JobDependenciesByTargetJob.GetValueOrCreateDefault(jobDep.Target).Add(jobDep);
            }
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                var jobDep = e.ItemAddedOrRemoved;
                JobDependenciesBySourceJob.GetValueOrCreateDefault(jobDep.Source).Remove(jobDep);
                JobDependenciesByTargetJob.GetValueOrCreateDefault(jobDep.Target).Remove(jobDep);
            }
            else if (e.ListChangedType == ListChangedType.Reset)
            {
                JobDependenciesBySourceJob.Clear();
                JobDependenciesByTargetJob.Clear();
                foreach (var grp in JobDependencies.GroupBy(jobDep => jobDep.Source))
                    JobDependenciesBySourceJob.Add(grp.Key, grp.ToList());
                foreach (var grp in JobDependencies.GroupBy(jobDep => jobDep.Target))
                    JobDependenciesByTargetJob.Add(grp.Key, grp.ToList());
            }
            // No other kind of event to handle because properties of JobDependency class are immutable
        }

        public virtual XElement Load() { throw new InvalidOperationException(); }
        public virtual void Save(XElement viewInfo = null) { throw new InvalidOperationException(); }
        protected internal virtual JobDiagram CreateInstance() { return new JobDiagram(); }

        public JobDiagram Clone()
        {
            var clone = CreateInstance();
            clone.DeepCopyFrom(this);
            return clone;
        }

        public void Reset()
        {
            InstanciatedFrom = null;
            lock(Jobs)
                Jobs.Clear();
            lock (JobDependencies)
                JobDependencies.Clear();
            JobDependenciesBySourceJob.Clear();
            JobDependenciesByTargetJob.Clear();
        }

        readonly object _copyLock = new object();
        public JobDiagram CreateInstanciation()
        {
            lock (_copyLock)
            {
                var clone = Clone();
                clone.InstanciatedFrom = this;
                return clone;
            }
        }
        public virtual void DeepCopyFrom(JobDiagram diagram)
        {
            lock (_copyLock)
            {
                Reset();

                Name = diagram.Name;
                InstanciatedFrom = diagram.InstanciatedFrom;
                DailyTimeToInstanciate = diagram.DailyTimeToInstanciate;
                LogPath = diagram.LogPath;
                DailyScheduleHistoryLength = diagram.DailyScheduleHistoryLength;
                MaxExecutionContextLength = diagram.MaxExecutionContextLength;
                Aliases = new AliasSet(diagram.Aliases.OriginalSet.Select(a => a.Clone()).ToList());
                ResourceManager = new SharedResourceManager(this, diagram.ResourceManager.OriginalSet.Select(r => r.Clone()).ToList());

                var originalJobToCloned = new Dictionary<Job, Job>();
                foreach (var job in diagram.GetJobs())
                {
                    var cloneJob = job.CreateInstance(this);
                    cloneJob.CopyFrom(job);
                    originalJobToCloned.Add(job, cloneJob);
                    lock (Jobs)
                        Jobs.Add(cloneJob);
                }

                foreach (var jobDep in diagram.JobDependencies)
                {
                    var cloneJobDep = jobDep.CreateInstance(this, originalJobToCloned[jobDep.Source], originalJobToCloned[jobDep.Target]);
                    cloneJobDep.CopyFrom(jobDep);
                    JobDependencies.Add(cloneJobDep);
                }
            }
        }

        #region Export

        public XElement ToXml(XElement nDiagram = null)
        {
            nDiagram = nDiagram ?? new XElement("JobDiagram");
            nDiagram.Add(new XAttribute("Name", Name));
            nDiagram.Add(new XAttribute("ChangedBy", Environment.UserName)); // additional audit info, not mandatory
            nDiagram.Add(new XAttribute("ChangedAtUTC", DateTime.UtcNow));    // additional audit info, not mandatory
            nDiagram.Add(new XElement("InstanciatedFrom", InstanciatedFrom == null ? "" : InstanciatedFrom.Name));
            nDiagram.Add(new XElement("DailyTimeToInstanciate", DailyTimeToInstanciate.ToString("hh':'mm':'ss")));
            nDiagram.Add(new XElement("LogPath", LogPath ?? ""));
            nDiagram.Add(new XElement("DailyScheduleHistoryLength", DailyScheduleHistoryLength));
            nDiagram.Add(new XElement("MaxExecutionContextLength", MaxExecutionContextLength));
            nDiagram.Add(new XElement("ProcessShortcuts", Aliases.OriginalSet
                                                                 .OrderBy(a => a.Shortcut)
                                                                 .Select(a => new XElement("Shortcut", 
                                                                                           new XAttribute("Macro", a.Shortcut), 
                                                                                           new XAttribute("FullProcessPath", a.Value ?? "")))
                                                                 .Cast<object>().ToArray()));

            
            var nSharedResources = new XElement("SharedResources");
            foreach (var sharedResource in ResourceManager.OriginalSet)
                nSharedResources.Add(sharedResource.ToXml(new XElement("Resource")));
            nDiagram.Add(nSharedResources);

            var nJobs = new XElement("Jobs");
            foreach (var job in Jobs)
                nJobs.Add(job.ToXml(new XElement("Job")));
            nDiagram.Add(nJobs);

            var nJobDeps = new XElement("JobDependencies");
            foreach (var jobDep in JobDependencies)
                nJobDeps.Add(jobDep.ToXml(new XElement("JobDependency")));
            nDiagram.Add(nJobDeps);

            return nDiagram;
        }

        public virtual void LoadFromXml(XElement nDiagram, JobDiagram instanciatedFrom = null)
        {
            JobDependencies.Clear(); // must be cleared first
            Jobs.Clear();

            // ReSharper disable PossibleNullReferenceException
            Name = nDiagram.Attribute("Name").Value;

            var instanciatedFromName = nDiagram.Element("InstanciatedFrom").Value;
            if (string.IsNullOrWhiteSpace(instanciatedFromName) && instanciatedFrom != null)
                throw new TechnicalException("Referenced diagram is not specified in file!", null);
            if (!string.IsNullOrWhiteSpace(instanciatedFromName) && (instanciatedFrom == null || instanciatedFrom.Name != instanciatedFromName))
                throw new TechnicalException("Referenced diagram's name in file does not match with provided diagram's name", null);
            Debug.Assert(string.IsNullOrWhiteSpace(instanciatedFromName) && instanciatedFrom == null ||
                         instanciatedFrom != null && instanciatedFromName == instanciatedFrom.Name);
            InstanciatedFrom = instanciatedFrom;

            DailyTimeToInstanciate = TimeSpan.ParseExact(nDiagram.Element("DailyTimeToInstanciate").Value, "hh':'mm':'ss", CultureInfo.InvariantCulture);
            LogPath = nDiagram.Element("LogPath").Value;
            var nShortcuts = nDiagram.Element("ProcessShortcuts");
            var aliases = new List<Alias>();
            foreach (var nShortcut in nShortcuts.Elements("Shortcut"))
                aliases.Add(new Alias()
                {
                    Shortcut = nShortcut.Attribute("Macro").Value,
                    Value = nShortcut.Attribute("FullProcessPath").Value
                });
            Aliases = new AliasSet(aliases);
            var nDailyScheduleHistoryLength = nDiagram.Element("DailyScheduleHistoryLength");
            DailyScheduleHistoryLength = string.IsNullOrWhiteSpace(nDailyScheduleHistoryLength?.Value) ? (int?)null : int.Parse(nDailyScheduleHistoryLength.Value);
            var nMaxExecutionContextLength = nDiagram.Element("MaxExecutionContextLength");
            MaxExecutionContextLength = string.IsNullOrWhiteSpace(nMaxExecutionContextLength?.Value) ? (int?)null : int.Parse(nMaxExecutionContextLength.Value);

            var nSharedResources = nDiagram.Element("SharedResources");
            if (nSharedResources != null)
            {
                var resources = new List<SharedResource>();
                foreach (var nResource in nSharedResources.Elements("Resource"))
                    resources.Add(SharedResource.FromXml(nResource));
                ResourceManager = new SharedResourceManager(this, resources);
            }

            var nJobs = nDiagram.Element("Jobs");
            foreach (var nJob in nJobs.Elements("Job"))
            {
                var job = Job.FromXml(nJob, this);
                lock(Jobs)
                    Jobs.Add(job);
            }

            var nJobDeps = nDiagram.Element("JobDependencies");
            foreach (var nJobDep in nJobDeps.Elements("JobDependency"))
            {
                var jobDep = JobDependency.FromXml(nJobDep, this, GetJobs());
                lock (JobDependencies)
                    JobDependencies.Add(jobDep);
            }
            // ReSharper restore PossibleNullReferenceException
        }

        #endregion Export
    }
}
