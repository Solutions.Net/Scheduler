using System;
using System.Globalization;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Diagnostics;

namespace Automation.Model
{
    [DebuggerDisplay("ExitCode={ExitCode,nq}, Duration={Duration,nq}, StartDate={StartDate,nq}")]
    public class JobExecution : INotifyPropertyChanged
    {
        public Job Job { get; }

        public DateTime      StartDate           { get; internal set; } // better to have a date for log and not only time
        public string        StandardOutput      { get { lock (_lockStdOutputBuilder) return _StandardOutputBuilder.ToString(); } }
        public string        StandardError       { get { lock (_lockStdErrorBuilder) return _StandardErrorBuilder.ToString(); } }
        public int?          ExitCode            { get { return _ExitCode;     } internal set { if (_ExitCode     == value) return; _ExitCode     = value; NotifyPropertyChanged(nameof(ExitCode));     NotifyPropertyChanged(nameof(IsSuccess)); } } int? _ExitCode;
        public string        UnknownError        { get { return _UnknownError; } internal set { if (_UnknownError == value) return; _UnknownError = value; NotifyPropertyChanged(nameof(UnknownError)); NotifyPropertyChanged(nameof(IsSuccess)); } } string _UnknownError;
        public TimeSpan?     Duration            { get { return _Duration;     } internal set { if (_Duration     == value) return; _Duration     = value; NotifyPropertyChanged(nameof(Duration));                                               } } TimeSpan? _Duration;
        public bool          ForcedExecution     { get; internal set; }
        public bool          Killed              { get; internal set; }
        public bool          ResetedByRestartJob { get; internal set; }

        internal Action      Kill                { get; set; }

        public bool?         IsSuccess           { get { return !ExitCode.HasValue ? (bool?)null : UnknownError == null && (Job.SuccessCode == ExitCode.Value || Job.WarningCode == ExitCode.Value); } }

        public JobExecution(Job job)
        {
            Job = job;
            PropertyChanged += JobResult_PropertyChanged;
        }

        void JobResult_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ExitCode))
            {
                _outputStreamWriter?.Close();
                _outputStreamWriter?.Dispose();
                _outputFileStream?.Close();
                _outputFileStream?.Dispose();
                _outputErrorStream?.Close();
                _outputErrorStream?.Dispose();
            }
        }

        string GetLogDirectory()
        {
            if (_logPath == null)
            {
                Debug.Assert(string.IsNullOrWhiteSpace(Job.OwnerGroup.LogPath) || Job.OwnerGroup.LogPath.EndsWith("\\"));
                string tmp = Job.OwnerGroup.LogPath + Job.JobName + "\\" + StartDate.ToString("yyyy-MM-dd HH.mm.ss") + "\\";
                Directory.CreateDirectory(tmp);
                _logPath = tmp;
            }
            return _logPath;
        }
        string _logPath;

        internal void AppendOutput(string str)
        {
            AppendOutput(str, str.Length);
        }
        internal void AppendOutput(byte[] buffer, int length)
        {
            AppendOutput((object)buffer, length);
        }
        void AppendOutput(object bufferOrString, int length)
        {
            Debug.Assert(bufferOrString != null, nameof(bufferOrString) + " != null");
            if (!string.IsNullOrEmpty(Job.OwnerGroup.LogPath))
                try
                {
                    if (_outputFileStream == null)
                        _outputFileStream = new FileStream(GetLogDirectory() + "OUTPUT.log", FileMode.CreateNew, FileAccess.Write, FileShare.Read);
                    if (_outputStreamWriter == null)
                    {
                        if (bufferOrString is string && _outputStreamWriter == null)
                            _outputStreamWriter = new StreamWriter(_outputFileStream,
                                                                   Encoding.UTF8, 1024, // default values
                                                                   true); // so the FileStream Is Not Closed
                        else // bufferOrString is byte[]
                            _outputStreamWriter?.Close(); // Check if this is really needed... maybe only a flush ?
                    }

                    string str = bufferOrString as string;
                    if (str != null)
                        _outputStreamWriter.Write(str);
                    else
                        _outputFileStream.Write((byte[])bufferOrString, 0, length);
                }
                catch
                {
                    // TODO : log (in our log...) cannot do something here right now
                }
            if (_StandardOutputBuilder.Length < 10 * 1024 * 1024 || _outputStreamWriter == null)
                lock (_lockStdOutputBuilder)
                {
                    string str = bufferOrString as string
                              ?? Encoding.UTF8.GetString((byte[])bufferOrString, 0, length);
                    _StandardOutputBuilder.Append(str);
                     if (_StandardOutputBuilder.Length >= 10 * 1024 * 1024 && _outputStreamWriter != null)
                    {
                        _StandardOutputBuilder.AppendLine();
                        _StandardOutputBuilder.AppendLine("Log too long, see the remaining log in log file...");
                    }
                }

            NotifyPropertyChanged(nameof(StandardOutput));
        }
        readonly object _lockStdOutputBuilder = new object();
        StringBuilder _StandardOutputBuilder { get; } = new StringBuilder();
        FileStream    _outputFileStream;
        StreamWriter  _outputStreamWriter;
        

        internal void AppendError(string str)
        {
            if (!string.IsNullOrEmpty(Job.OwnerGroup.LogPath))
                try
                {
                    if (_outputErrorStream == null)
                        _outputErrorStream = new StreamWriter(new FileStream(GetLogDirectory() + "ERROR.log", FileMode.CreateNew, FileAccess.Write, FileShare.Read));
                    _outputErrorStream.Write(str);
                }
                catch
                {
                    // TODO : log (in our log...) cannot do something here right now
                }
            if (_StandardErrorBuilder.Length < 10 * 1024 * 1024 || _outputErrorStream == null)
                lock (_lockStdErrorBuilder)
                {
                    _StandardErrorBuilder.Append(str);
                    if (_StandardErrorBuilder.Length >= 10 * 1024 * 1024 && _outputErrorStream != null)
                    {
                        _StandardErrorBuilder.AppendLine(); 
                        _StandardErrorBuilder.AppendLine("Log too long, see the remaining log in log file...");
                    }
                }
            NotifyPropertyChanged(nameof(StandardError));
        }
        readonly object _lockStdErrorBuilder = new object();
        StringBuilder _StandardErrorBuilder { get; } = new StringBuilder();
        StreamWriter _outputErrorStream;

        public void SaveToDisk()
        {
            var dir = GetLogDirectory();
            if (string.IsNullOrWhiteSpace(dir))
                return;
            var root = ToXml(includeJobDefinition: true);
            var xdoc = new XDocument(root);
            xdoc.Save(dir + "STATUS.log");
        }


        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Export

        public static JobExecution FromXml(XElement nResult, Job job)
        {
            var result = new JobExecution(job);
            // ReSharper disable PossibleNullReferenceException
            result.StartDate = DateTime.ParseExact(nResult.Element("StartTime").Value, "yyyy-MM-dd HH':'mm':'ss", CultureInfo.InvariantCulture);
            result._StandardOutputBuilder.Clear();
            result.AppendOutput(nResult.Element("StandardOutput").Value);
            result._StandardErrorBuilder.Clear();
            result.AppendError(nResult.Element("StandardError").Value);
            result.ExitCode = string.IsNullOrWhiteSpace(nResult.Element("ExitCode").Value) ? (int?)null : int.Parse(nResult.Element("ExitCode").Value);
            result.Duration = string.IsNullOrWhiteSpace(nResult.Element("Duration").Value) ? (TimeSpan?)null : TimeSpan.ParseExact(nResult.Element("Duration").Value, "hh':'mm':'ss", CultureInfo.InvariantCulture);
            result.UnknownError = nResult.Element("UnknownError").Value;
            result.ForcedExecution = bool.Parse(nResult.Element("Forced").Value);
            result.ResetedByRestartJob = nResult.Element("ResetedByRestartJob") != null && bool.Parse(nResult.Element("ResetedByRestartJob").Value);
            // ReSharper restore PossibleNullReferenceException
            return result;
        }

        public XElement ToXml(XElement node = null, bool includeJobDefinition = false)
        {
            node = node ?? new XElement("JobResult");
            node.Add(new XElement("StartTime", StartDate.ToString("yyyy-MM-dd HH':'mm':'ss")));
            node.Add(new XElement("ExitCode", ExitCode));
            node.Add(new XElement("Duration", Duration?.ToString("hh':'mm':'ss")));
            node.Add(new XElement("UnknownError", UnknownError));
            node.Add(new XElement("Forced", ForcedExecution));
            node.Add(new XElement("ResetedByRestartJob", ForcedExecution));
            if (includeJobDefinition)
            {
                var nExecJobDef = Job.ToXml(new XElement("ExecutedJobDefinition"), withDescription: false, withExecutions: false);
                node.Add(nExecJobDef);
            }
            node.Add(new XElement("StandardError", StandardError.ToString()));
            node.Add(new XElement("StandardOutput", StandardOutput.ToString()));
            return node;
        }

        #endregion
    }
}
