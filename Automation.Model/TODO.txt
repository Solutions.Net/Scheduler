﻿FAIT - Quand on kill une tache il faut fermer le stream de log ! (impossible de supprimer le dossier sinon)
FAIT - Quand on ferme un daily scheduler, fermer les fenetres d'execution ouvertes.
FAIT - Quand on double click sur une tache, permettre de selectioner et copier le titre (mais sans editer)
FAIT - Fenetre d'execution plus grosse par defaut

- FAIT Quand l'utilisateur veut fermer la fenetre principale, vérifier que tous les threads sont arrete, sinon refuser
       Ne pas appeler stop et join mais afficher un message d'erreur
  FAIT Ajouter menu contextuel Relaunch (== reset + force run)
  FAIT Pouvoir avoir acces en lecture seule aux ligne des taches désactivé !
  FAIT Click droit => dupliquer tache
  FAIT Mettre la couleur des tache en cours d'execution en vert tres clair plutot que jaune (qu'on confond avec l'orange d'une tache echoué et selectionné)

- Quand uen tache Reset s'execute il faudrait qu'elle compare sa derniere date d'execution pour determiner le temps d'attente
  Exemple : Si la branche a mis 30 minute et que la tache de reset est totue les 1 h, la banche est reexecute au bout d'une heure 30. 
            Il faudrait qu'elle determiner qu'elle peut etre rexecuter au bout de 30 minute apres la finde la premiere execution de la branche
-  Changer JobDependenciesBySourceJob de dictionaire a ILookUpEdit pour eviter les accès via [] qui peuvent thrower (et la possibilité de modifier la ocllection en meme temps qu'on y accede ?)


FAIT - Prévoir la fermeture automatique des onglets / instanciation de daily scheduler au bout de 8 jours par exemple

FAIT - Mettre la date de lancement dans les onglets de scheduling

  Bug: - Le daily scheduler devrait comprendre qu'une transition en success avec une tache source qui a foiré ne peut plus avancer.
       - Au changement d'horaire ete hiver, la date utilisée à la ligne "var success = Schedulers.TryAdd(dailyScheduler.StartDate, dailyScheduler);" (GlobalScheduler) n'est pas bonne
	     Le DailyScheduler ajouté est ajouté a 23h au lieu de minuit ce qui provoque un bug car le dictionaire contient deja celui de la veille

  - Faire en sorte que quand on essaye d'arreter un DailyScheduler, les taches WaitDelay WaitUntil et Restart soit killé
    car elle ne pose pas de probleme a être killé
  - FAIT Ajouter la possibilite de desactiver ET consider en succes une tache dans le planifieur d'execution
  - Quand un tache est en disabled et éconsideree en succesé, ne pas generer de logs...
  - Garder dans un reservoir les JobView, au lieu des les disposer (responsable de 33% de la lenteur au clonage / chargement, les 66% etant du a l'appel a dock devexpress)
    (C'etait avant la refacto => v2)

- FAIT Rappeler dans le fichier _status d'une tache qui s'est terminé, les infos de la tache.
  Cela permettra dans un deuxieme temps d'executer des taches ponctuelles
  Actuellement il n y'a que l'objet execution qui est serialisé dans les fichier _STATUS

- Permettre la saisie d'argument revenant souvent au niveau du scheduler
  et indiquer une si ces argument doivnet etre collé a la fin ou au debut de l'executable en ligne de commande (par defaut : la fin)
  Ensuite dans la saisie des taches, avec une case a cocher indiquer si il faut ajouter les argument partagé.
  Ca permet de mettre a jour les arguments partagé en une seule passe, voir a cacher les argument partagé (qunad il contienne login et mot de passe)

- Systeme de plugin branché sur les evenements : Nouveau scheduler journalier, lancement tache et fin de tache
   - Serialization des info
   - Prevoir une option pour savoir si le pluging sait gerer les evenements concurrentiels (une tache se termine et une autre demarre en meme temps)
     Dans le cas contraire séquentialiser les appels au plugins
   - Ajouter la possibilité de valider l'execution d'une tache au cas ou la condition business serait complexe
     (mais dans ce cas il faut prevoir que le moteur doit se rappeler de la decision... et pour combine de temps
   
   




