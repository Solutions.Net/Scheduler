﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;

namespace Automation.Model
{
    [DebuggerDisplay("{Source.JobName,nq} ---- {SourceJobState,nq} ----> {Target.JobName,nq}")]
    public class JobDependency : IDiagramElement, INotifyPropertyChanged
    {
        public JobDiagram       OwnerGroup      { get; }

        public Job              Source          { get; } // Too complex and time consuming to make editable for now
        public Job              Target          { get; } // Idem + can be null when building Dependency

        [Description("Indique l'état requis que doit avoir la tâche de départ pour que l'éxécution de la tâche d'arrivée soit autorisée.")]
        public eSourceJobState SourceJobState   { get { return _SourceJobState; } set { _SourceJobState = value; NotifyPropertyChanged(nameof(SourceJobState)); } }
        eSourceJobState _SourceJobState;


        public JobDependency(JobDiagram ownerGroup, Job from, Job to)
        {
            OwnerGroup = ownerGroup;
            Source     = from;
            Target     = to;
        }
        protected internal virtual JobDependency CreateInstance(JobDiagram ownerGroup, Job from, Job to) { return new JobDependency(ownerGroup, from, to); }
        protected internal virtual void CopyFrom(JobDependency jobDep)
        {
            SourceJobState = jobDep.SourceJobState;
        }

        internal bool ConditionFilled
        {
            get
            {
                if (Source.Execution?.IsSuccess == null)
                    return false;
                return SourceJobState.HasFlag(eSourceJobState.Success) && Source.Execution.IsSuccess.Value
                    || SourceJobState.HasFlag(eSourceJobState.Error) && !Source.Execution.IsSuccess.Value;
            }
        }

        #region Export

        public static JobDependency FromXml(XElement nJobDep, JobDiagram ownerGroup, IReadOnlyCollection<Job> jobs)
        {
            // ReSharper disable once PossibleNullReferenceException
            var sourceJobName = nJobDep.Element("Source").Value;
            var targetJobName = nJobDep.Element("Target")?.Value; // should always exists

            var source = jobs.Single(job => job.JobName == sourceJobName);
            var target = targetJobName == null ? null : jobs.Single(job => job.JobName == targetJobName); 

            var jobDep = new JobDependency(ownerGroup, source, target);
            // ReSharper disable once PossibleNullReferenceException
            jobDep.SourceJobState = (eSourceJobState)Enum.Parse(typeof(eSourceJobState), nJobDep.Element("SourceJobState").Value);
            return jobDep;
        }

        public XElement ToXml(XElement node = null)
        {
            node = node ?? new XElement("JobDependency");

            node.Add(new XElement("Source", Source.JobName));
            if (Target != null) // should be always true  at this point
                node.Add(new XElement("Target", Target.JobName));
            node.Add(new XElement("SourceJobState", SourceJobState));

            return node;
        }
        #endregion Export

        public event PropertyChangedEventHandler PropertyChanged;
        void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    [Flags]
    public enum eSourceJobState
    {
        UNDEFINED = 0,
        Success = 1,
        Error = 2,
        Any = Success | Error
    }
}
