﻿using System;
using System.Xml.Linq;
using System.Diagnostics;
using System.IO;

using TechnicalTools;
using TechnicalTools.Model;


namespace Automation.Model
{
    public class JobDiagramXml : JobDiagram, IUserInteractiveObject
    {
        readonly string _filename;

        public JobDiagramXml(string filename) { _filename = filename; }

        protected internal override JobDiagram CreateInstance() { return new JobDiagramXml(_filename); }

        public override XElement Load()
        {
            XDocument xdoc;
            try
            {
                xdoc = XDocument.Load(_filename);
            }
            catch (FileNotFoundException ex)
            {
                throw new UserUnderstandableException($"File {_filename} not found!", ex);
            }
            var nRoot = xdoc.Root;
            // ReSharper disable once PossibleNullReferenceException
            Debug.Assert(nRoot.Name == "JobSchedule");
            var nDiagram = nRoot.Element("JobDiagram");
            LoadFromXml(nDiagram);
            var nViewsInfo = nRoot.Element("ViewsInfo");
            return nViewsInfo;
        }
        public override void Save(XElement viewInfo = null)
        {
            var nRoot = new XElement("JobSchedule", ToXml(new XElement("JobDiagram")));
            if (viewInfo != null)
            {
                var nViewsInfo = new XElement("ViewsInfo", viewInfo);
                nRoot.Add(nViewsInfo);
            }
            var xdoc = new XDocument(nRoot);
            xdoc.Save(_filename + ".tmp");
            File_Extensions.BackupFileAndReplaceBy(_filename, _filename + ".tmp");
        }
    }
}
