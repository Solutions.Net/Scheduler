using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Model;


namespace Automation.Model
{
    [DebuggerDisplay("JobName={JobName,nq}, JobKind={JobKind,nq}, ProcessPath= {ProcessPath,nq}, Arguments={Arguments,nq}")]
    public class Job : IDiagramElement, INotifyPropertyChanged
    {
        public JobDiagram OwnerGroup         { get; }
        //public int       JobId  { get; } = System.Threading.Interlocked.Decrement(ref SeedId); static int SeedId = 0;
        public override string ToString() { return JobName; }
        // Must be unique !
        public string     JobName            { get { return _JobName; }            set { OnPropertyChanged(ref _JobName, value?.Trim(), true); } } string _JobName;
        public string     Description        { get { return _Description; }        set { OnPropertyChanged(ref _Description, value?.Trim());; } } string _Description;
        public eJobKind   JobKind            { get { return _JobKind; }            set { OnPropertyChanged(ref _JobKind, value, true); } } eJobKind _JobKind = eJobKind.CommandLine;
        public bool       Enabled            { get { return _Enabled; }            set { OnPropertyChanged(ref _Enabled, value); } } bool _Enabled = true;
        public bool       ConsiderAsSuccess  { get { return _ConsiderAsSuccess; }  set { OnPropertyChanged(ref _ConsiderAsSuccess, value); } } bool _ConsiderAsSuccess;

        // Data for eJobKind.Manual
        public string                            ProcessPath         { get { return _ProcessPath; }         set { OnPropertyChanged(ref _ProcessPath, value); } } string _ProcessPath;
        public string                            ProcessPathReal     { get { return OwnerGroup.Aliases.OriginalSet.FirstOrDefault(a => a.Shortcut == ProcessPath?.Trim())?.Value ?? ProcessPath; } }
        public string                            Arguments           { get { return _Arguments;   }         set { OnPropertyChanged(ref _Arguments,   value); } } string _Arguments;
        public int                               SuccessCode         { get { return _SuccessCode; }         set { OnPropertyChanged(ref _SuccessCode, value); } } int    _SuccessCode;
        public FixedBindingList<SharedResource>  ResourcesRequired   { get; } = new FixedBindingList<SharedResource>();
        

        // Data for eJobK ind.WaitTimeInterval
        public TimeSpan   WaitUntil          { get { return _WaitUntil; }          set { OnPropertyChanged(ref _WaitUntil, value); } } TimeSpan _WaitUntil;

        // Data for eJobKind.WaitDelay and eJobKind.Restart
        public TimeSpan   WaitDelay          { get { return _WaitDelay; }          set { OnPropertyChanged(ref _WaitDelay, value); } } TimeSpan _WaitDelay;

        // Data for eJobKind.WaitTimeInterval
        public TimeSpan   StopAfter          { get { return _StopAfter; }          set { OnPropertyChanged(ref _StopAfter, value); } } TimeSpan _StopAfter;


        // Data for eJobKind.Restart
        public bool       RestartRecursively { get { return _RestartRecursively; } set { OnPropertyChanged(ref _RestartRecursively, value); } } bool _RestartRecursively;

        // Data for eJobKind.WebRequest
        public string     UrlLink            { get { return _UrlLink; }            set { OnPropertyChanged(ref _UrlLink, value); } } string _UrlLink;



        // Data for all eJobKind (used only for eJobKind.WebRequest for now)
        public int?       WarningCode        { get { return _WarningCode;      }   set { OnPropertyChanged(ref _WarningCode, value); } } int? _WarningCode;
        public TimeSpan?  ExpectedDuration   { get { return _ExpectedDuration; }   set { OnPropertyChanged(ref _ExpectedDuration, value); } } TimeSpan? _ExpectedDuration;
        public TimeSpan   TimeOut            { get { return _TimeOut;          }   set { OnPropertyChanged(ref _TimeOut, value); } } TimeSpan _TimeOut;

        public IReadOnlyCollection<JobDependency> ToTargetsDependencies  { get { return OwnerGroup.JobDependenciesBySourceJob.TryGetValueClass(this) ?? new List<JobDependency>(); } }
        public IReadOnlyCollection<JobDependency> FromSourceDependencies { get { return OwnerGroup.JobDependenciesByTargetJob.TryGetValueClass(this) ?? new List<JobDependency>(); } }

        public JobExecution                    Execution        { get { return _Execution; } internal set { OnExecutionChanging(value); } } volatile JobExecution _Execution;
        FixedBindingListAdvanced<JobExecution> ExecutionHistory = new FixedBindingListAdvanced<JobExecution>();

        public void ClearExecutionHistory()
        {
            lock (ExecutionHistory)
                ExecutionHistory.Clear();
        }
        public void AddExecutionHistory(JobExecution exec)
        {
            lock (ExecutionHistory)
                ExecutionHistory.Add(exec);
        }
        public int GetExecutionHistoryCount()
        {
            lock (ExecutionHistory)
                return ExecutionHistory.Count;
        }
        public List<JobExecution> GetExecutionHistoryAndSubscribe(EventHandler<IAfterChangeEventArgs<JobExecution>> handle, bool getCurrent)
        {
            lock (ExecutionHistory)
            {
                ExecutionHistory.AfterListChange += handle;
                return GetExecutionHistory(getCurrent);
            }
        }
        public List<JobExecution> GetExecutionHistory(bool getCurrent)
        {
            lock (ExecutionHistory)
            {
                return ExecutionHistory.Concat(getCurrent ? new JobExecution[] { Execution }.NotNull() : new JobExecution[0]).ToList();
            }
        }
        public void UnsubscribeFromExecutionHistory(EventHandler<IAfterChangeEventArgs<JobExecution>> handle)
        {
            lock (ExecutionHistory)
                ExecutionHistory.AfterListChange -= handle;
        }

        public bool IsExecuting
        {
            get
            {
                var execution = Execution; // fix value to avoid race condition;
                return execution != null && execution.ExitCode == null;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public event TechnicalTools.Model.PropertyChangingEventHandler PropertyChanging;

        internal Job(JobDiagram ownerGroup)
        {
            OwnerGroup = ownerGroup;
            PropertyChanging += (_, e) => { if (e.PropertyName == nameof(JobName)) CheckNewJobName((string)e.NewValue); };
        }
        protected internal virtual Job CreateInstance(JobDiagram ownerGroup) { return new Job(ownerGroup); }
        protected internal virtual void CopyFrom(Job job, string newJobName = null)
        {
            _loading = true;
            try
            {
                JobName = newJobName ?? job.JobName;
                JobKind = job.JobKind;
                Description = job.Description;
                ExpectedDuration = job.ExpectedDuration;
                Enabled = job.Enabled;
                ConsiderAsSuccess = job.ConsiderAsSuccess;

                WaitDelay = job.WaitDelay;
                WaitUntil = job.WaitUntil;
                StopAfter = job.StopAfter;
                RestartRecursively = job.RestartRecursively;
                ProcessPath = job.ProcessPath;
                Arguments = job.Arguments;

                UrlLink = job.UrlLink;
                TimeOut = job.TimeOut;

                SuccessCode = job.SuccessCode;
                WarningCode = job.WarningCode;

                ResourcesRequired.Clear();
                foreach (var rs in job.ResourcesRequired)
                    ResourcesRequired.Add(OwnerGroup.ResourceManager.GetOrAdd(rs.Name));

                Debug.Assert(Execution == null);
                Debug.Assert(ExecutionHistory.Count == 0);
            }
            finally
            {
                _loading = false;
            }
        }

        internal Job Clone(string newUniqueJobName)
        {
            var job = new Job(OwnerGroup);
            job.CopyFrom(this, newUniqueJobName);
            return job;
        }

        public string CheckConfiguration()
        {
            if (JobKind != eJobKind.CommandLine)
                return null;
            if (!File.Exists(ProcessPathReal))
                return "Executable does not exist or is not currently reachable!";
            return null;
        }

        void CheckNewJobName(string newJobName)
        {
            if (OwnerGroup.GetJobs().Where(j => j != this).Any(j => j.JobName == newJobName))
                throw new UserUnderstandableException("This name is already taken, please choose another one!", null);
            var m = reIllegalChars.Match(newJobName);
            if (m.Success)
                throw new UserUnderstandableException($"The job name cannot contain illegal character '{m.Value}'!", null);
        }
        static readonly Regex reIllegalChars = new Regex($"[{Regex.Escape(new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars()))}]", RegexOptions.Compiled);

        void OnExecutionChanging(JobExecution newValue)
        {
            if (Execution != null)
                Execution.PropertyChanged -= Execution_PropertyChanged;
            _Execution = newValue;
            if (Execution != null)
                Execution.PropertyChanged += Execution_PropertyChanged;
            if (!_loading)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Execution)));
        }

        void Execution_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!_loading)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Execution) + "." + e.PropertyName));
        }

        #region Export

        bool _loading;

        public static Job FromXml(XElement nJob, JobDiagram ownerGroup)
        {
            var job = new Job(ownerGroup);
            job._loading = true;
            try
            {
                // ReSharper disable PossibleNullReferenceException
                job.JobName = nJob.Element("JobName").Value;
                job.Enabled = bool.Parse(nJob.Element("Enabled").Value);
                job.ConsiderAsSuccess = bool.Parse(nJob.Element("ConsiderAsSuccess").Value);

                job.JobKind = (eJobKind)Enum.Parse(typeof(eJobKind), nJob.Element("JobKind").Value);
                job.ProcessPath = nJob.Element("ProcessPath").Value;
                job.Arguments = nJob.Element("Arguments").Value;
                var nResourcesRequired = nJob.Element("ResourcesRequired");
                if (nResourcesRequired != null)
                    foreach (var nResourceName in nResourcesRequired.Elements("ResourceName"))
                        job.ResourcesRequired.Add(ownerGroup.ResourceManager.GetOrAdd(nResourceName.Value));
                job.UrlLink = nJob.Element("UrlLink").Value;
                job.TimeOut = TimeSpan.ParseExact(nJob.Element("TimeOut").Value, "hh':'mm':'ss", CultureInfo.InvariantCulture);
                job.SuccessCode = int.Parse(nJob.Element("SuccessCode").Value);
                var warningCode = nJob.Element("WarningCode").Value;
                job.WarningCode = string.IsNullOrWhiteSpace(warningCode) ? (int?)null : int.Parse(warningCode);
                job.WaitUntil = TimeSpan.ParseExact(nJob.Element("WaitUntil").Value, "hh':'mm':'ss", CultureInfo.InvariantCulture);
                job.WaitDelay = TimeSpan.ParseExact(nJob.Element("WaitDelay").Value, "hh':'mm':'ss", CultureInfo.InvariantCulture);
                job.StopAfter = TimeSpan.ParseExact(nJob.Element("StopAfter").Value, "hh':'mm':'ss", CultureInfo.InvariantCulture);
                job.RestartRecursively = bool.Parse(nJob.Element("RestartRecursively").Value);
                job.ExpectedDuration = nJob.Element("ExpectedDuration") == null ? (TimeSpan?)null : TimeSpan.ParseExact(nJob.Element("ExpectedDuration").Value, "hh':'mm':'ss", CultureInfo.InvariantCulture);
                job.Description = nJob.Element("Description").Value;
                // ReSharper restore PossibleNullReferenceException

                job.ExecutionHistory.Clear();
                var nExecutionHistory = nJob.Element("Executions");
                if (nExecutionHistory != null)
                    foreach (var nExecHist in nExecutionHistory.Elements("Execution"))
                        job.ExecutionHistory.Add(JobExecution.FromXml(nExecHist, job));

                return job;
            }
            finally
            {
                job._loading = false;
            }
        }

        public XElement ToXml(XElement nJob = null, bool withDescription = true, bool withExecutions = true)
        {
            nJob = nJob ?? new XElement("Job");
            nJob.Add(new XElement("JobName", JobName));
            nJob.Add(new XElement("Enabled", Enabled.ToString()));
            nJob.Add(new XElement("ConsiderAsSuccess", ConsiderAsSuccess.ToString()));

            nJob.Add(new XElement("JobKind", JobKind));
            nJob.Add(new XElement("ProcessPath", ProcessPath));
            nJob.Add(new XElement("Arguments", Arguments));
            var nResourcesRequired = new XElement("ResourcesRequired");
            foreach (var resource in ResourcesRequired)
                nResourcesRequired.Add(new XElement("ResourceName", resource.Name));
            if (nResourcesRequired.Elements().Any())
                nJob.Add(nResourcesRequired);
            nJob.Add(new XElement("UrlLink", UrlLink));
            nJob.Add(new XElement("TimeOut", TimeOut.ToString("hh':'mm':'ss")));
            nJob.Add(new XElement("SuccessCode", SuccessCode));
            nJob.Add(new XElement("WarningCode", WarningCode.ToString()));
            nJob.Add(new XElement("WaitUntil", WaitUntil.ToString("hh':'mm':'ss")));
            nJob.Add(new XElement("WaitDelay", WaitDelay.ToString("hh':'mm':'ss")));
            nJob.Add(new XElement("StopAfter", StopAfter.ToString("hh':'mm':'ss")));
            nJob.Add(new XElement("RestartRecursively", RestartRecursively.ToString()));
            if (ExpectedDuration != null)
                nJob.Add(new XElement("ExpectedDuration", ExpectedDuration.Value.ToString("hh':'mm':'ss")));
            if (withDescription)
                nJob.Add(new XElement("Description", Description));

            if (withExecutions)
            {
                var nExecutions = new XElement("Executions");
                foreach (var exec in GetExecutionHistory(true))
                    nExecutions.Add(exec.ToXml(new XElement("Execution")));
                if (nExecutions.Elements().Any())
                    nJob.Add(nExecutions);
            }
            return nJob;
        }

        #endregion Export


        void OnPropertyChanged<T>(ref T _backField, T value, bool allowCancel = false, [CallerMemberName] string propertyName = null)
        {
            if (_loading)
            {
                _backField = value;
                return;
            }
            if (_backField == null && value == null ||
                _backField != null && _backField.Equals(value))
                return;
            if (allowCancel)
            {
                var h = PropertyChanging;
                if (h != null)
                {
                    var e = new PropertyChangingEventArgs<T>(propertyName, _backField, value);
                    h.Invoke(this, e);
                    if (e.CancelException != null)
                        if (e.CancelException.StackTrace == null)
                            throw e.CancelException;
                        else
                            ExceptionDispatchInfo.Capture(e.CancelException).Throw(); // Throw preserving stack trace
                    if (e.Cancel)
                        return;
                }
            }
            _backField = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum eJobKind
    {
        [Description("Command Line")]
        CommandLine,
        [Description("Wait until a specific time in day")]
        WaitTimeInterval,
        [Description("Wait for a specific duration")]
        WaitDelay,
        [Description("Web request")]
        WebRequest,
        [Description("Restart targeted job")]
        Restart,

        // test
        [Description("TEST: Task that write a line on output and success")]
        TEST_TaskWritingOnOutput,
        [Description("TEST: Task that write a line on error stream and fail with code 42")]
        TEST_TaskWritingOnError,
        [Description("TEST: Task that make daily scheduler crash")]
        TEST_TaskInternalErrorThrowing,
    }
}
