﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.UI.EditableLayer;

namespace Automation.Model
{
    public class SharedResourceSet : BusinessObjectEditableSet<SharedResource, ISharedResource>, IUserInteractiveObject
    {
        public new IReadOnlyList<SharedResource> OriginalSet { get { return base.OriginalSet; } }

        public SharedResourceSet(List<SharedResource> initialValues)
            : base(initialValues, true, (ISharedResource r) => new IdTuple<string>(r.Name),
                                        (SharedResource r) => new IdTuple<string>(r.Name), true)
        {
            AllowNew = true;
            AllowRemove = true;
            AllowEdit = true;
            Refresh();
            RefreshRessourcesByName();
        }

        // create a new instance of PartnerEnv and add it to current collection
        protected override SharedResource CreateNew()
        {
            return new SharedResource()
            {
                Name = EditingSet.CreateUnique(r => r.Name, "Resource")
            };
        }
        protected override string ValidateProperty(ISharedResource item, Type interfaceType, PropertyInfo property)
        {
            if (property.Name == nameof(ISharedResource.MaximumConcurrentUse))
                if (item.MaximumConcurrentUse == 0)
                    return "Concurrent use must strictly positive!";
            return base.ValidateProperty(item, interfaceType, property);
        }
        protected override string ValidateSet(IEnumerable<ISharedResource> set, IProgress<string> pr = null)
        {
            var res = CheckUnicityOnEditingSet(set, r => r.Name, "All shared resources must have a unique name!");
            if (!string.IsNullOrWhiteSpace(res))
                return res;
            return base.ValidateSet(set, pr);
        }


        public override Action ApplyChangedItemToDatabase(IProgress<string> pr = null)
        {
            var commit = base.ApplyChangedItemToDatabase(pr);
            return () =>
            {
                commit();
                RefreshRessourcesByName();
            };
        }
        readonly Dictionary<string, SharedResource> _RessourcesByName = new Dictionary<string, SharedResource>();

        public SharedResource GetByName(string resourceName)
        {
            lock (_RessourcesByName)
                return _RessourcesByName.TryGetValueClass(resourceName.ToLower());
        }
        void RefreshRessourcesByName()
        {
            lock (_RessourcesByName)
            {
                _RessourcesByName.Clear();
                foreach (var sr in OriginalSet)
                    _RessourcesByName[sr.Name.ToLower()] = sr;
            }
        }
    }
    public class SharedResourceManager : SharedResourceSet
    {
        public JobDiagram Owner { get; }

        public IReadOnlyList<SharedResource> RessourcesUsed { get { lock (_lock) return _RessourcesUsed.Keys.ToList(); } }

        public SharedResourceManager(JobDiagram owner, List<SharedResource> initialValues)
            : base(initialValues)
        {
            Owner = owner;
        }
        readonly object _lock = new object();
        readonly Dictionary<SharedResource, List<JobExecution>> _RessourcesUsed = new Dictionary<SharedResource, List<JobExecution>>();
        readonly Dictionary<JobExecution, List<SharedResource>> _RessourcesByJobExecution = new Dictionary<JobExecution, List<SharedResource>>();

        protected override string ValidateSet(IEnumerable<ISharedResource> set, IProgress<string> pr = null)
        {
            var usedResources = Owner.GetJobs()
                                     .SelectMany(job => job.ResourcesRequired.Select(rr => new { Resource = rr, Job = job }))
                                     .GroupBy(t => t.Resource, t => t.Job)
                                     .ToDictionary(grp => grp.Key, grp => grp.ToList());
            foreach (var usingJobs in usedResources)
                if (Removed.Select(r => GetEditedObject(r)).Contains(usingJobs.Key))
                    return $"Resource {usingJobs.Key.Name} must be kept!" + Environment.NewLine + 
                          "It is used by:" + Environment.NewLine + 
                          usingJobs.Value.Select(job => " - \"" + job.JobName + "\"").Join(Environment.NewLine);
            return base.ValidateSet(set, pr);
        }


        public SharedResource GetOrAdd(string name)
        {
            lock(_lock)
            {
                var sr = GetByName(name);
                if (sr == null)
                {
                    sr = CreateNew();
                    sr.Name = name;
                    AddNew(sr);
                    Commit();
                }
                return sr;
            }
        }

        internal bool TryLockRessourcesFor(JobExecution consumingJobExecution)
        {
            lock (_lock)
            {
                var cancels = new List<Action>();
                var lst = consumingJobExecution.Job.ResourcesRequired.OrderBy(rs => rs.Name).ToList();
                foreach (var rs in lst)
                {
                    List<JobExecution> execs = null;
                    if (!_RessourcesUsed.TryGetValue(rs, out execs))
                    {
                        execs = new List<JobExecution>();
                        _RessourcesUsed.Add(rs, execs);
                    }
                    if (execs.Count < rs.MaximumConcurrentUse)
                    {
                        cancels.Add(() => execs.Remove(consumingJobExecution));
                        execs.Add(consumingJobExecution);
                    }
                    else
                    {
                        foreach (var cancel in cancels)
                            cancel();
                        return false;
                    }
                }
                if (lst.Count > 0)
                    _RessourcesByJobExecution.Add(consumingJobExecution, lst);
                return true;
            }
        }
        internal void UnlockRessourcesFor(JobExecution consumingJobExecution)
        {
            lock (_lock)
            {
                var lst = _RessourcesByJobExecution.TryGetValueClass(consumingJobExecution);
                if (lst == null)
                    return;
                foreach (var rs in lst)
                    _RessourcesUsed[rs].Remove(consumingJobExecution);
            }
        }

        internal IReadOnlyCollection<JobExecution> GetUsingExecutions(SharedResource sharedResource)
        {
            lock (_lock)
            {
                return _RessourcesUsed.TryGetValueClass(sharedResource)?.ToList() 
                    ?? new List<JobExecution>();
            }
        }
    }
}
