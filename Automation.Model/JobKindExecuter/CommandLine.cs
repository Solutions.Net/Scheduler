﻿using System;
using System.Diagnostics;
using System.IO;


namespace Automation.Model.JobKindExecuter
{
    class CommandLine
    {
        public static int Execute(JobExecution exec, string processPath, string arguments)
        {
            // from http://www.c-sharpcorner.com/article/redirecting-standard-inputoutput-using-the-process-class/
            var psi = new ProcessStartInfo("cmd");
            psi.FileName = processPath;
            psi.Arguments = arguments;

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true; // only taken in account if UseShellExecute is false
            psi.WindowStyle = ProcessWindowStyle.Hidden;

            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            Process p = new Process();
            p.StartInfo = psi;
            p.EnableRaisingEvents = true; // https://msdn.microsoft.com/en-us/library/system.diagnostics.process.enableraisingevents(v=vs.110).aspx
            // TODO : https://stackoverflow.com/questions/33716580/process-output-redirection-on-a-single-console-line
            p.OutputDataReceived += (_, e) => { if (e.Data != null) exec.AppendOutput(e.Data + Environment.NewLine); };
            p.ErrorDataReceived += (_, e) => { if (e.Data != null) exec.AppendError(e.Data + Environment.NewLine); };
            p.Start();
            exec.Kill = () => { exec.Killed = true; p.Kill(); };
            try
            {
                StreamWriter sw = p.StandardInput;
                //StreamReader sr = p.StandardOutput;
                //StreamReader err = p.StandardError;
                sw.AutoFlush = true;
                sw.Close();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();

                p.WaitForExit(); // flush the output buffer
                return p.ExitCode;
            }
            finally
            {
                exec.Kill = null;
            }
        }

    }
}
