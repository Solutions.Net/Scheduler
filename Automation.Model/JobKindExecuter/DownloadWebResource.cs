﻿using System;
using System.Diagnostics;
using System.Threading;

using TechnicalTools.Diagnostics;


namespace Automation.Model.JobKindExecuter
{
    class DownloadWebResource
    {
        // Seems better than ExecuteWebRequest_Old because we can stream out the downloaded content during the download
        // So if the webpage downloaded is a business webpage and crash we will have output just before it happens
        public static int Execute(Job job)
        {
            var cancellator = new CancellationTokenSource();

            // TODO: Is caller supposed to handle ObjectDisposedException exception ?
            // What should be the expected behavior if we just dispose cancellator when user call Kill ?
            job.Execution.Kill = () => { job.Execution.Killed = true; cancellator.Cancel(); };
            Exception exception = null;
            try
            {
                var link = job.UrlLink;
                if (!link.ToLower().Trim().StartsWith("http"))
                    link = "http://" + link;
                using (var client = new WebClientWithTimeout((int)job.TimeOut.TotalMilliseconds))
                using (var stream = client.OpenRead(link))
                {
                    Debug.Assert(stream != null, nameof(stream) + " != null");
                    var buffer = new byte[4096];
                    int bytesReceived;
                    while ((bytesReceived = stream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        cancellator.Token.ThrowIfCancellationRequested();
                        job.Execution.AppendOutput(buffer, bytesReceived);
                        if (cancellator.Token.IsCancellationRequested)
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            bool cancelled = cancellator.Token.IsCancellationRequested;
            cancellator.Dispose();
            return TreatWebResourceDownloading(job, cancelled, exception);
        }
        public static int Execute_Old(Job job)
        {
            using (var client = new WebClientWithTimeout((int)job.TimeOut.TotalMilliseconds))
            using (var mre = new ManualResetEvent(false))
            {
                bool cancelled = false;
                Exception ex = null;
                string pageContent = null;
                client.DownloadStringCompleted += (_, e) =>
                {
                    job.Execution.Kill = null;
                    cancelled = e.Cancelled;
                    pageContent = e.Result;
                    ex = e.Error;
                    // ReSharper disable once AccessToDisposedClosure
                    mre.Set();
                };
                client.DownloadStringAsync(new Uri(job.UrlLink));
                // TODO : Check what happens if kill is called right after download is done and client is Disposed
                job.Execution.Kill = () => { job.Execution.Killed = true; client.CancelAsync(); }; 
                mre.WaitOne();
                if (!cancelled && ex == null)
                    job.Execution.AppendOutput(pageContent);
                return TreatWebResourceDownloading(job, cancelled, ex);
            }
        }
        public static int TreatWebResourceDownloading(Job job, bool hasBeenCanceled, Exception ex)
        {
            if (hasBeenCanceled)
            {
                job.Execution.AppendError("User cancelled!");
                return -1;
            }
            if (ex == null)
                return 0;
            job.Execution.AppendError(ExceptionManager.Instance.Format(ex));
            var wex = ex as System.Net.WebException;
            if (wex != null)
            {
                if (wex.Status != (System.Net.WebExceptionStatus.ProtocolError))
                    return -2;
                var response = wex.Response as System.Net.HttpWebResponse;
                if (response == null)
                    return -3;
                return (int)response.StatusCode;
            }
            return -4;
        }

        class WebClientWithTimeout : System.Net.WebClient
        {
            readonly int _timeout;
            public WebClientWithTimeout(int timeout) { _timeout = timeout; }
            protected override System.Net.WebRequest GetWebRequest(Uri uri)
            {
                var w = base.GetWebRequest(uri);
                Debug.Assert(w != null, nameof(w) + " != null");
                w.Timeout = _timeout <= 0 ? -1 : _timeout;
                return w;
            }
        }

    }
}
