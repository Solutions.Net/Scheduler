﻿using System;
using System.Threading;


namespace Automation.Model.JobKindExecuter
{
    class WaitDelayOrUntilOrRestart
    {
        public static int Execute(Job job)
        {
            var timeToWait = job.JobKind == eJobKind.WaitDelay || job.JobKind == eJobKind.Restart
                           ? job.WaitDelay
                           : job.JobKind == eJobKind.WaitTimeInterval
                           ? TimeSpan.FromTicks(Math.Max(0, (job.WaitUntil - DateTime.Now.TimeOfDay).Ticks))
                           : TimeSpan.Zero;
            var cts = new CancellationTokenSource();
            job.Execution.Kill = () => { job.Execution.Killed = true; cts.Cancel(); };
            var cancelled = cts.Token.WaitHandle.WaitOne(timeToWait);
            job.Execution.Kill = null;
            return cancelled ? -1 : 0;
        }

    }
}
