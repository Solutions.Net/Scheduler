using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using ThreadState = System.Threading.ThreadState;

using Automation.Model.JobKindExecuter;


namespace Automation.Model
{
    public class DailyJobScheduler : INotifyPropertyChanged // TODO : Ajouter IDisposed pour plus de propreté...
    {
        public readonly DateTime   StartDate; // Can contains Date only or Date + time offset
        public readonly JobDiagram DiagramInstance;


        // Return only ThreadState.Running or ThreadState.StopRequested or ThreadState.Stopped
        public ThreadState State { get { return _State; }  private set { _State = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State))); } }  ThreadState _State = ThreadState.Stopped;
        public event PropertyChangedEventHandler PropertyChanged;


        public DailyJobScheduler(DateTime startDate, JobDiagram diagramReference)
        {
            StartDate = startDate;
            DiagramInstance = diagramReference.CreateInstanciation();
            Debug.Assert(DiagramInstance.InstanciatedFrom != null);
        }
        Thread _thScheduler;
        CancellationTokenSource _ctsScheduler = new CancellationTokenSource();
        readonly AutoResetEvent _mreSomethingHappened = new AutoResetEvent(false);
        readonly Dictionary<Job, Thread> _jobThreads = new Dictionary<Job, Thread>();
        bool _preventStartingNewThread; // Indicate the state to target... (the property State will sync with)
        readonly ConcurrentDictionary<Job, Job> _jobToRunForced = new ConcurrentDictionary<Job, Job>();
        readonly object _lock = new object();
        
        public void Run()
        {
            lock (_lock)
                if (State == ThreadState.Stopped)
                {
                    _thScheduler = new Thread(() =>
                    {
                        try { MainLoop(); }
                        finally
                        {
                            State = ThreadState.Stopped;
                            _ctsScheduler.Dispose();
                        }
                    });
                    _ctsScheduler = new CancellationTokenSource();
                    _preventStartingNewThread = false;
                    _thScheduler.Start();
                    _mreSomethingHappened.Set();
                }
                else if (State == ThreadState.StopRequested)
                    _preventStartingNewThread = false; // Abort the stopping
        }
       
        // Do not stop thread (because user could force starting) but stop starting new task
        public void StopStartingNewJob()
        {
            lock (_lock)
                if (State == ThreadState.Running)
                {
                    _preventStartingNewThread = true;
                    State = ThreadState.StopRequested;
                    _mreSomethingHappened.Set();
                }
        }
        
        internal void StopForce()
        {
            lock (_lock)
                if (State != ThreadState.Stopped)
                {
                    StopStartingNewJob();
                    _ctsScheduler.Cancel();
                    foreach (var jobExec in _jobThreads.Keys)
                        jobExec.Execution.Kill();
                }
            _mreSomethingHappened.Set();
            Join();
        }
        internal void Join()
        {
            JoinChildThreads();
            _thScheduler?.Join();
        }
        internal void JoinChildThreads()
        {
            var threadsToWait = new List<Thread>();
            lock (_lock)
                threadsToWait.AddRange(_jobThreads.Values);
            foreach (var th in threadsToWait)
                th.Join();
        }
        
        public void KillExecution(Job job)
        {
            Action kill = job.Execution.Kill;
            if (kill == null)
                return;
            try
            {
                kill();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ExceptionManager.Instance.Format(ex));
            }
        }

        public int ResetJobExecution(Job job)
        {
            return ResetJobExecution(job, false, false);
        }
        internal int ResetJobExecution(Job job, bool recursive, bool markExecutionAsReseted)
        {
            lock (_lock)
            {
                return recursive
                    ? ResetJobExecutionRecursive(job, markExecutionAsReseted)
                    : ResetJobExecution(job, job, markExecutionAsReseted);
            }
        }
        
        int ResetJobExecution(Job job, Job requesterJob, bool markExecutionAsReseted)
        {
            int resetCount = 0;
            if (job.Execution != null &&
                (job.Execution.ExitCode != null || job == requesterJob && job.JobKind == eJobKind.Restart))
            {
                if (job.Execution.ExitCode != null) // For Reset task, it will be done later, when the execution object is complete
                    job.AddExecutionHistory(job.Execution);
                job.Execution.ResetedByRestartJob = markExecutionAsReseted;
                // Note that if requesterJob is a restart job and job is reference-equals to job 
                // (it is the usual case after some recursive calls...),
                // the thread executing requesterJob is not done yet. 
                // It will take care of the Execution value in the finally clause of scheduler
                // Moreover, it is better to put this assignement here (above  the foreach loop) to prevent any infinite loop
                job.Execution = null;
                ++resetCount;

                if (job.JobKind == eJobKind.Restart)
                    foreach (var jobDep in job.OwnerGroup.JobDependenciesBySourceJob.TryGetValueClass(job) ?? new List<JobDependency>())
                    {
                        Debug.Assert(!ReferenceEquals(job, jobDep.Target));
                        // Note that, here, requesterJob could be reference-equals to jobDep.Target
                        var resetAccepted = ResetJobExecution(jobDep.Target, requesterJob, markExecutionAsReseted);
                        resetCount += resetAccepted;
                    }
                
                _mreSomethingHappened.Set();
                return resetCount;
            }
            return resetCount;
        }
        int ResetJobExecutionRecursive(Job requesterJob, bool markExecutionAsReseted)
        {
            int resetedTaskCount = 0;
            Debug.Assert(JobDiagram.CONTRACT_NoCycleInJobDiagramExceptThroughRestartTask);
            var jobsReseted = new Queue<Job>();
            jobsReseted.Enqueue(requesterJob); // requesterJob is not reseted (or not yet), but we insert it because it is the seed
            do
            {
                var j = jobsReseted.Dequeue();
                foreach (var jobDep in j.OwnerGroup.JobDependenciesBySourceJob.TryGetValueClass(j) ?? new List<JobDependency>())
                {
                    // Note that, here, requesterJob could be reference-equals to jobDep.Target
                    var resetAccepted = ResetJobExecution(jobDep.Target, requesterJob, markExecutionAsReseted);
                    resetedTaskCount += resetAccepted;
                    if (resetAccepted > 0 &&
                        jobDep.Target.JobKind != eJobKind.Restart) // Stop recursion (see contract assertion above and because handled in ResetJobExecution)
                        jobsReseted.Enqueue(jobDep.Target);
                }
            }
            while (!jobsReseted.IsEmpty());
            //if (job.OwnerGroup.JobDependenciesByTargetJob[job].Any(j => j.Source.Execution == null))
            //    ResetExecution(job, job);
            return resetedTaskCount;
        }
       

        public void ForceRun(Job job/*, bool andAllDependantJobs = false*/)
        {
            lock (_lock)
            {
                if (job.Execution != null)
                    return; // race condition
                if (!_jobToRunForced.TryAdd(job, job))
                    return; // already present in list..
                if (State == ThreadState.Stopped || State == ThreadState.StopRequested)
                    Run();
                else
                    _mreSomethingHappened.Set();
            }
        }

        void MainLoop()
        {
            DateTime lastChange = DateTime.MinValue;
            while (!_preventStartingNewThread)
            {
                State = ThreadState.Running;
                while (_mreSomethingHappened.WaitOne())
                {
                    _ctsScheduler.Token.ThrowIfCancellationRequested();
                    lastChange = lastChange == DateTime.MinValue ? StartDate : DateTime.Now;
                    lock (_lock)
                    {
                        var jobsToRun = FindJobsEligibleForRun()
                                            .OrderByDescending(j => _jobToRunForced.ContainsKey(j))
                                            .ToList();

                        // if no thread running and no job to run
                        if (_jobThreads.Count == 0 && _jobToRunForced.Count == 0 &&
                            // and we have to stop or all tasks are done or will never be executed
                            (_preventStartingNewThread || jobsToRun.Count == 0
                                                        && DiagramInstance.GetJobs().All(job => !ShouldBeExecutedLater(job))))
                        {
                            // State should always sync _preventStartingNewThread
                            // So we set the "target" in case it was not the user that asked for termination.
                            // User can change its mind or opposes to the stopping in the outer loop
                            _preventStartingNewThread = true; 
                            State = ThreadState.StopRequested;
                            break;
                        }

                    
                        foreach (var job in jobsToRun)
                        {
                            if (job.JobKind == eJobKind.Restart && _jobThreads.ContainsKey(job))
                                continue; // can happen if a reset job reset itself and scheduler wake up before the related thread is done and has cleaned itself
                            var execution = new JobExecution(job);
                            if (!DiagramInstance.ResourceManager.TryLockRessourcesFor(execution))
                                continue;
                            Job dummy;
                            execution.ForcedExecution = _jobToRunForced.TryRemove(job, out dummy);
                            execution.StartDate = DateTime.Now;
                            job.Execution = execution; // Must assign before we release the lock because we may have remove job from _jobToRunForced
                            var th = new Thread(() =>
                            {
                                int? exitCode = null;
                                try
                                {
                                    execution.StartDate = DateTime.Now; // assign again to be accurate
                                    exitCode = Execute(job, execution); // Execute is expected to not throw exception...
                                }
                                finally // ...but we never know
                                {
                                    // Complete the filling of "execution" object
                                    execution.Duration = DateTime.Now - execution.StartDate;
                                    execution.Kill = null;

                                    // We set exit code in last because this is what is tested by other thread (without lock)
                                    if (exitCode.HasValue)
                                        execution.ExitCode = exitCode.Value;

                                    DiagramInstance.ResourceManager.UnlockRessourcesFor(execution);

                                    // Deal with task reseted while executing. this is a rare scenario, except for Reset Tasks which often reset themself.
                                    if (job.Execution == null) // see ResetJobExecutionRecursive to know why this can be true
                                        job.AddExecutionHistory(execution);
                                    // Cleaning internal and notifying scheduler to continue
                                    lock (_lock)
                                        _jobThreads.Remove(job);
                                    _mreSomethingHappened.Set();

                                    if (job.Enabled)
                                        try
                                        {
                                            if (job.JobKind != eJobKind.WaitDelay &&
                                                job.JobKind != eJobKind.WaitTimeInterval &&
                                                job.JobKind != eJobKind.Restart)
                                                execution.SaveToDisk();
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.Error.WriteLine("Cannot write job status in disk because:");
                                            Console.Error.WriteLine(ExceptionManager.Instance.Format(ex));
                                        }
                                }
                            });
                            _jobThreads.Add(job, th);
                            th.Start();
                        }
                    }
                }
                JoinChildThreads();
                lock (_lock)
                {
                    // Check user did not opposed to the thread stopping
                    // (or changed his mind if the stopping was because of him).
                    if (_preventStartingNewThread) 
                    {
                        State = ThreadState.Stopped;
                        break; // outer method call already dispose _ctsScheduler
                    }
                    // Otherwhise we continue...
                    _ctsScheduler.Dispose(); // _ctsScheduler has maybe been used.
                    _ctsScheduler = new CancellationTokenSource();
                    _mreSomethingHappened.Set();
                }
            }
        }

        List<Job> FindJobsEligibleForRun()
        {
            var eligibleTasks = new List<Job>();
            foreach (var job in DiagramInstance.GetJobs())
            {
                bool userForced = _jobToRunForced.ContainsKey(job);
                if (job.Execution == null &&  // No execution context
                    (userForced || // user force the task
                        !_preventStartingNewThread && // Or scheduler is not stopping and
                        (job.Enabled || job.ConsiderAsSuccess)  // Enabled Or disabled but to considered as success (execution will skipped the real task later)
                        /* && job.ExecutionHistory.Count == 0)*/ // (disabled because it prevent reseted task) task has never been reseted manually, otherwise the user has to validate for second thrid etc .. execution
                        ))
                {
                    var dependencies = DiagramInstance.JobDependenciesByTargetJob.TryGetValueClass(job);
                    if (userForced || dependencies == null || dependencies.All(jobDep => jobDep.Source.JobKind == eJobKind.Restart
                                                                                      || jobDep.ConditionFilled))
                        eligibleTasks.Add(job);
                }
            }
            return eligibleTasks;
        }

        bool ShouldBeExecutedLater(Job job)
        {
            Debug.Assert(_jobThreads.Count == 0 && _jobToRunForced.Count == 0);

            if (job.JobKind == eJobKind.Restart)
                return true;
            if (job.Execution?.ExitCode != null)
                return false;
            if (!job.Enabled && !job.ConsiderAsSuccess)
                return false;
            var lst = job.OwnerGroup.JobDependenciesByTargetJob.TryGetValueClass(job);
            if (lst == null)
                return true;
            Debug.Assert(JobDiagram.CONTRACT_NoCycleInJobDiagramExceptThroughRestartTask);
            return lst.Any(jobDep => ShouldBeExecutedLater(jobDep.Source)); // We are sure there is no cycle because of the assert
        }

        int Execute(Job job, JobExecution execution)
        {
            try
            {
                if (!execution.ForcedExecution && !job.Enabled && job.ConsiderAsSuccess)
                {
                    CommandLine.Execute(execution, "cmd.exe", "/c echo *** Disabled task considered in success ***");
                    return job.WarningCode ?? job.SuccessCode;
                }
                Debug.Assert(job.Enabled || execution.ForcedExecution);

                if (job.JobKind == eJobKind.CommandLine)
                    return CommandLine.Execute(execution, job.ProcessPathReal, job.Arguments);
                if (job.JobKind == eJobKind.WebRequest)
                    return DownloadWebResource.Execute(job);
                if (job.JobKind == eJobKind.TEST_TaskWritingOnOutput)
                    return CommandLine.Execute(execution, "cmd.exe", "/c echo \"Some text on output stream...\" from directory %CD%");
                if (job.JobKind == eJobKind.TEST_TaskWritingOnError)
                    return CommandLine.Execute(execution, "cmd.exe", "/c echo \"Simulated error with exit code 42\" 1>&2  & exit 42");
                if (job.JobKind == eJobKind.TEST_TaskInternalErrorThrowing)
                    throw new Exception("A dummy exception for testing.");

                //if (job.JobKind == eJobKind.Restart)
                //    return 0; // we are done
                int res = WaitDelayOrUntilOrRestart.Execute(job);
                if (job.JobKind == eJobKind.WaitTimeInterval &&
                    DateTime.Now >= StartDate.Date + (job.StopAfter == TimeSpan.Zero ? TimeSpan.FromDays(1) : job.StopAfter))
                    res = 1;
                // This part is not cancellable
                if (job.JobKind == eJobKind.Restart)
                    ResetJobExecution(job, job.RestartRecursively, true);
                return res;
            }
            catch (Exception ex)
            {
                execution.UnknownError = "Error while running process in scheduler:" + Environment.NewLine
                                            + ExceptionManager.Instance.Format(ex, false);
                return ex.HResult;
            }
        }
    }
}
