﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.UI.EditableLayer;
using TechnicalTools.Model.Cache;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Automation.Model
{
    public interface IAlias : IHasTypedIdReadable, ICopyable<IAlias>, ICloneable
    {
        string Shortcut { get; set; }
        string Value { get; set; }
    }
    public class Alias : IAlias
    {
        public string Shortcut { get; set; }
        public string Value { get; set; }

        IIdTuple IHasClosedIdReadable.Id                       { get { return new IdTuple<string>(Shortcut); } }
        ITypedId  IHasTypedIdReadable.TypedId                  { get { return (this as IHasTypedIdReadable).MakeTypedId((this as IHasClosedIdReadable).Id); } }
        ITypedId  IHasTypedIdReadable.MakeTypedId(IIdTuple id) { return id.ToTypedId(typeof(Alias)); }

        void ICopyable.CopyFrom(ICopyable source)
        {
            (this as ICopyable<IAlias>).CopyFrom(source as IAlias);
        }
        void ICopyable<IAlias>.CopyFrom(IAlias source)
        {
            Shortcut = source.Shortcut;
            Value = source.Value;
        }
        public Alias Clone()
        {
            var clone = new Alias();
            (clone as ICopyable<IAlias>).CopyFrom(this);
            return clone;
        }
        object ICloneable.Clone() { return Clone(); }
    }
    public class AliasSet : BusinessObjectEditableSet<Alias, IAlias>, IUserInteractiveObject
    {
        public new IReadOnlyList<Alias> OriginalSet { get { return base.OriginalSet; } }
        
        public AliasSet(List<Alias> initialValues)
            : base(initialValues, true, (IAlias s) => new IdTuple<string>(s.Shortcut), 
                                        (Alias s) => new IdTuple<string>(s.Shortcut), true)
        {
            AllowNew = true;
            AllowRemove = true;
            AllowEdit = true;
            Refresh();
        }

        // create a new instance of PartnerEnv and add it to current collection
        protected override Alias CreateNew()
        {
            return new Alias();
        }
        protected override string ValidateProperty(IAlias item, Type interfaceType, PropertyInfo property)
        {
            if (property.Name == nameof(IAlias.Shortcut))
            {
                var alias = (item.Shortcut ?? "").Trim(); // space will be allowed 
                if (!reWord.IsMatch(alias)) // match at least one character
                    return "Alias must be a full word!";
            }
            return base.ValidateProperty(item, interfaceType, property);
        }
        static readonly Regex reWord = new Regex(@"^[-_ .\p{L}]+$", RegexOptions.Compiled);
    }
}
