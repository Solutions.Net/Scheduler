﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraLayout.Utils;
using DevExpress.XtraEditors.Controls;

using TechnicalTools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using Automation.Model;


namespace Automation.UI.Winforms
{
    [DebuggerDisplay("View For JobName={Job.JobName,nq}, JobKind={Job.JobKind,nq}, ProcessPath= {Job.ProcessPath,nq}, Arguments={Job.Arguments,nq}")]
    [ToolboxItem(true)]
    public partial class JobDefinitionView : EnhancedXtraUserControl
    {
        public Job           Job       { get; private set; }
        public IViewLockable OwnerView { get; private set; }

        public   bool ReadOnly             { get { return _ReadOnly; } set { _ReadOnly = value; DefineEnabilitiesAndVisibilities(); } } bool _ReadOnly;
        public   bool IsReadOnly           { get { return ReadOnly || Job == null || OwnerView == null; } }

        
        public JobDefinitionView()
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;

            ViewHelper.CreateHandleSafely(() => CreateHandle()); // before an event subscribing

            // Make resizing / drawing etc a LOT faster
            meDescription.Properties.WordWrap = false;

            lytCtlDefinition.Appearance.DisabledLayoutItem.ForeColor = Color.Black;
            Text = "Job Definition";
            DoubleBuffered = true;
            Height = 155;

            lueJobKind.FillWithEnumValues<eJobKind>();
        }
        public void Bind(Job job, IViewLockable ownerView)
        {
            lytCtlDefinition.BeginUpdate();
            UnbindModel();
            Job = job;
            OwnerView = ownerView;
            LoadJobDefinition();
            BindModel();
            DefineEnabilitiesAndVisibilities();
            lytCtlDefinition.EndUpdate();
        }
        public void Unbind()
        {
            lytCtlDefinition.BeginUpdate();
            UnbindModel();
            Job = null;
            OwnerView = null;
            //LoadJobDefinition();
            DefineEnabilitiesAndVisibilities();
            lytCtlDefinition.EndUpdate();
        }

        public void SetJobEnability(bool value)
        {
            chkEnabled.Checked = value;
        }

        void DefineEnabilitiesAndVisibilities()
        {
            lytCtlDefinition.BeginUpdate();

            var isReadOnly = IsReadOnly || (OwnerView?.IsReadOnly ?? false);
            lueJobKind.ReadOnly = isReadOnly;
            chkEnabled.ReadOnly = isReadOnly;
            chkConsiderAsSuccess.ReadOnly = isReadOnly;
            chkResetTasksRecursively.ReadOnly = isReadOnly;
            tseWaitUntil.ReadOnly = isReadOnly;
            tseWaitDelay.ReadOnly = isReadOnly;
            tseStopAfter.ReadOnly = isReadOnly;
            txtProcessPath.ReadOnly = isReadOnly;
            txtProcessArguments.ReadOnly = isReadOnly;
            chkRequiredResources.EditableByUser = !isReadOnly;
            teUrlLink.ReadOnly = isReadOnly;
            tseTimeOut.ReadOnly = isReadOnly;
            meDescription.ReadOnly = isReadOnly;
            tseExpectedDuration.ReadOnly = isReadOnly;
            txtWarningCode.ReadOnly = isReadOnly;

            if (Job != null)
            {
                chkConsiderAsSuccess_LayoutItem.Visibility = Job.Enabled ? LayoutVisibility.Never : LayoutVisibility.Always;
                chkResetTasksRecursively_LayoutItem.Visibility = Job.JobKind == eJobKind.Restart ? LayoutVisibility.Always : LayoutVisibility.Never;

                tseWaitUntil_LayoutItem.Visibility = Job.JobKind == eJobKind.WaitTimeInterval ? LayoutVisibility.Always : LayoutVisibility.Never;

                tseWaitDelay_LayoutItem.Visibility = Job.JobKind.In(eJobKind.WaitDelay, eJobKind.Restart) ? LayoutVisibility.Always : LayoutVisibility.Never;
                tseWaitDelay_LayoutItem.Text = Job.JobKind == eJobKind.Restart ? "Wait before restart" : "Wait delay";

                tseStopAfter_LayoutItem.Visibility = Job.JobKind == eJobKind.WaitTimeInterval ? LayoutVisibility.Always : LayoutVisibility.Never;

                txtProcessPath_LayoutItem.Visibility = Job.JobKind == eJobKind.CommandLine ? LayoutVisibility.Always : LayoutVisibility.Never;
                txtProcessArguments_LayoutItem.Visibility = Job.JobKind == eJobKind.CommandLine ? LayoutVisibility.Always : LayoutVisibility.Never;
                chkRequiredResources_LayoutItem.Visibility = Job.JobKind.In(eJobKind.CommandLine, eJobKind.WebRequest, eJobKind.WaitDelay, eJobKind.TEST_TaskWritingOnOutput, eJobKind.TEST_TaskWritingOnError, eJobKind.TEST_TaskInternalErrorThrowing) ? LayoutVisibility.Always : LayoutVisibility.Never;

                teUrlLink_LayoutItem.Visibility = Job.JobKind == eJobKind.WebRequest ? LayoutVisibility.Always : LayoutVisibility.Never;
                tseTimeOut_LayoutItem.Visibility = Job.JobKind == eJobKind.WebRequest ? LayoutVisibility.Always : LayoutVisibility.Never;
                txtWarningCode_LayoutItem.Visibility = Job.JobKind.NotIn(eJobKind.WaitTimeInterval, eJobKind.WaitDelay, eJobKind.Restart) ? LayoutVisibility.Always : LayoutVisibility.Never;
            }
            meDescription_LayoutItem.Visibility = LayoutVisibility.Always;

            lytCtlDefinition.EndUpdate();
        }
        
        #region Binding From Model => View

        void LoadJobDefinition()
        {
            lueJobKind.SelectedValueAsEnumSet(Job == null ? eJobKind.CommandLine : Job.JobKind);

            txtProcessPath.Text = Job?.ProcessPath;
            txtProcessArguments.Text = Job?.Arguments;
            chkRequiredResources.ItemsAvailablesSet(Job.OwnerGroup.ResourceManager.OriginalSet);
            chkRequiredResources.ItemsSelected = Job.ResourcesRequired;
            meDescription.Text = Job?.Description;
            tseExpectedDuration.EditValue = Job?.ExpectedDuration;
            chkEnabled.Checked = Job?.Enabled ?? false;
            chkConsiderAsSuccess.Checked = Job?.ConsiderAsSuccess ?? false;

            chkResetTasksRecursively.Checked = Job?.RestartRecursively ?? false;
            tseWaitUntil.EditValue = Job?.WaitUntil;
            tseWaitDelay.EditValue = Job?.WaitDelay;
            tseStopAfter.EditValue = Job?.StopAfter;

            teUrlLink.Text = Job?.UrlLink;
            tseTimeOut.TimeSpan = Job?.TimeOut ?? TimeSpan.Zero;
            txtWarningCode.EditValue = Job?.WarningCode;
        }

        void BindModel()
        {
            UnbindModel();
            tseStopAfter.EditValueChanged += tseStopAfter_EditValueChanged;
            txtWarningCode.EditValueChanged += txtWarningCode_EditValueChanged;
            chkResetTasksRecursively.CheckedChanged += chkResetTasksRecursively_CheckedChanged;
            chkConsiderAsSuccess.CheckedChanged += chkConsiderAsSuccess_CheckedChanged;
            chkEnabled.CheckedChanged += chkEnabled_CheckedChanged;
            tseTimeOut.EditValueChanged += tseTimeOut_EditValueChanged;
            tseExpectedDuration.EditValueChanged += tseExpectedDuration_EditValueChanged;
            teUrlLink.EditValueChanged += teUrlLink_EditValueChanged;
            tseWaitUntil.EditValueChanged += tseWaitUntil_EditValueChanged;
            tseWaitDelay.EditValueChanged += tseWaitDelay_EditValueChanged;
            lueJobKind.EditValueChanging += lueJobKind_EditValueChanging;
            meDescription.EditValueChanged += meDescription_EditValueChanged;
            txtProcessArguments.EditValueChanged += txtProcessArguments_EditValueChanged;
            txtProcessPath.EditValueChanged += txtProcessPath_EditValueChanged;

            Job.PropertyChanged += Job_PropertyChanged;
            Disposed += JobView_Disposed;
        }
        void UnbindModel()
        {
            tseStopAfter.EditValueChanged -= tseStopAfter_EditValueChanged;
            txtWarningCode.EditValueChanged -= txtWarningCode_EditValueChanged;
            chkResetTasksRecursively.CheckedChanged -= chkResetTasksRecursively_CheckedChanged;
            chkConsiderAsSuccess.CheckedChanged -= chkConsiderAsSuccess_CheckedChanged;
            chkEnabled.CheckedChanged -= chkEnabled_CheckedChanged;
            tseTimeOut.EditValueChanged -= tseTimeOut_EditValueChanged;
            tseExpectedDuration.EditValueChanged -= tseExpectedDuration_EditValueChanged;
            teUrlLink.EditValueChanged -= teUrlLink_EditValueChanged;
            tseWaitUntil.EditValueChanged -= tseWaitUntil_EditValueChanged;
            tseWaitDelay.EditValueChanged -= tseWaitDelay_EditValueChanged;
            lueJobKind.EditValueChanging -= lueJobKind_EditValueChanging;
            meDescription.EditValueChanged -= meDescription_EditValueChanged;
            txtProcessArguments.EditValueChanged -= txtProcessArguments_EditValueChanged;
            txtProcessPath.EditValueChanged -= txtProcessPath_EditValueChanged;

            if (Job == null)
                return;
            Job.PropertyChanged -= Job_PropertyChanged;
            Disposed -= JobView_Disposed;
        }
        private void JobView_Disposed(object sender, EventArgs e)
        {
            UnbindModel();
        }
        private void Job_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => Job_PropertyChanged(sender, e)));
                return;
            }
            LoadJobDefinition();
        }
        #endregion Binding From Model => View

        #region Binding From View => Model

        private void lueJobKind_EditValueChanging(object sender, ChangingEventArgs e)
        {
            try
            {
                Job.JobKind = lueJobKind.GetValueFromChangingEvent<eJobKind>(e.NewValue) ?? Job.JobKind;
                addToolTipPropertyOnAllControl.Hide(lueJobKind);
            }
            catch (Exception ex)
            {
                addToolTipPropertyOnAllControl.Show(ex.Message, lueJobKind, 0, lueJobKind.Height);
                e.Cancel = true;
            }
            DefineEnabilitiesAndVisibilities();
        }
        private void chkEnabled_CheckedChanged(object sender, EventArgs e)
        {
            Job.Enabled = chkEnabled.Checked;
            DefineEnabilitiesAndVisibilities();
        }

        private void chkConsiderAsSuccess_CheckedChanged(object sender, EventArgs e)
        {
            Job.ConsiderAsSuccess = chkConsiderAsSuccess.Checked;
        }
        private void meDescription_EditValueChanged(object sender, EventArgs e)
        {
            Job.Description = meDescription.Text;
        }
        private void tseExpectedDuration_EditValueChanged(object sender, EventArgs e)
        {
            Job.ExpectedDuration = tseExpectedDuration.EditValue == null ? (TimeSpan?)null : tseExpectedDuration.TimeSpan;
        }
        private void txtWarningCode_EditValueChanged(object sender, EventArgs e)
        {
            int? v = txtWarningCode.EditValue == null ? (int?)null : (int)Convert.ToInt64(txtWarningCode.EditValue);
            if (v == 0)
            {
                v = null;
                txtWarningCode.EditValue = null;
            }
            Job.WarningCode = v;
        }
        
        private void txtProcessPath_EditValueChanged(object sender, EventArgs e)
        {
            Job.ProcessPath = txtProcessPath.Text;
        }

        private void txtProcessArguments_EditValueChanged(object sender, EventArgs e)
        {
            Job.Arguments = txtProcessArguments.Text;
        }
        private void tseWaitDelay_EditValueChanged(object sender, EventArgs e)
        {
            Job.WaitDelay = tseWaitDelay.TimeSpan;
        }
        private void tseWaitUntil_EditValueChanged(object sender, EventArgs e)
        {
            Job.WaitUntil = tseWaitUntil.TimeSpan;
        }
        private void tseStopAfter_EditValueChanged(object sender, EventArgs e)
        {
            Job.StopAfter = tseStopAfter.TimeSpan;
        }
        
        private void chkResetTasksRecursively_CheckedChanged(object sender, EventArgs e)
        {
            Job.RestartRecursively = chkResetTasksRecursively.Checked;
        }
        private void teUrlLink_EditValueChanged(object sender, EventArgs e)
        {
            Job.UrlLink = teUrlLink.Text.Trim();
        }
        private void tseTimeOut_EditValueChanged(object sender, EventArgs e)
        {
            Job.TimeOut = tseTimeOut.TimeSpan;
        }

        #endregion Binding From View => Model

        new IAsyncResult BeginInvoke(Delegate method)
        {
            if (!Disposing && !IsDisposed)
                return Application.OpenForms[0].BeginInvoke(method);
            Action a = () => { };
            return a.BeginInvoke(null, null);
        }
        new bool InvokeRequired
        {
            get
            {
                return Application.OpenForms[0].InvokeRequired;
            }
        }
    }
}
