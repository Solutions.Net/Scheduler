using System;

using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;

using Automation.Model;


namespace Automation.UI.Winforms
{
    public partial class AliasSetForm : EnhancedXtraForm
    {
        public AliasSet Aliases { get; }

        [Obsolete("For designer only")]
        public AliasSetForm() : this(null) { }

        public AliasSetForm(AliasSet aliases)
        {
            Aliases = aliases;
            InitializeComponent();
            Text = "Aliases";
            _viewHelper = new GridViewForEditableSet<Alias, IAlias>();
        }
        readonly GridViewForEditableSet<Alias, IAlias> _viewHelper;

        void AliasSetForm_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            Configure_Aliases_Edition();
        }

        void Configure_Aliases_Edition()
        {
            _viewHelper.Install(gvAliases, Aliases);

            gcAliases.DataSource = Aliases.EditingSet;
            gvAliases.BestFitColumns();
        }

    }
}
