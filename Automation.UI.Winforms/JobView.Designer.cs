namespace Automation.UI.Winforms
{
    partial class JobView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.borderLineBottom = new System.Windows.Forms.Label();
            this.lblLineUnderTitle = new System.Windows.Forms.Label();
            this.borderLineTop = new System.Windows.Forms.Label();
            this.borderLineLeft = new System.Windows.Forms.Label();
            this.borderLineRight = new System.Windows.Forms.Label();
            this.txtTitle = new TechnicalTools.UI.Controls.LabelToTextBox();
            this.sizeGripResizer = new TechnicalTools.UI.Controls.SizeGripResizer();
            this.titleRightClickMenu = new System.Windows.Forms.ContextMenuStrip();
            this.mnuCopyJobName = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCopyJobDefinition = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDisable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDisableConsiderAsSuccess = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEnable = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCloneTask = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuRunAgain = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuResetExecution = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuForceRunNow = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuKillTask = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolTipPropertyOnAllControl = new System.Windows.Forms.ToolTip();
            this.mmoDescription = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.mnuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.titleRightClickMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mmoDescription.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // borderLineBottom
            // 
            this.borderLineBottom.BackColor = System.Drawing.Color.Teal;
            this.borderLineBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.borderLineBottom.Location = new System.Drawing.Point(1, 425);
            this.borderLineBottom.MaximumSize = new System.Drawing.Size(0, 1);
            this.borderLineBottom.MinimumSize = new System.Drawing.Size(0, 1);
            this.borderLineBottom.Name = "borderLineBottom";
            this.borderLineBottom.Size = new System.Drawing.Size(376, 1);
            this.borderLineBottom.TabIndex = 10;
            // 
            // lblLineUnderTitle
            // 
            this.lblLineUnderTitle.BackColor = System.Drawing.Color.Teal;
            this.lblLineUnderTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLineUnderTitle.Location = new System.Drawing.Point(1, 23);
            this.lblLineUnderTitle.MaximumSize = new System.Drawing.Size(0, 2);
            this.lblLineUnderTitle.MinimumSize = new System.Drawing.Size(0, 2);
            this.lblLineUnderTitle.Name = "lblLineUnderTitle";
            this.lblLineUnderTitle.Size = new System.Drawing.Size(376, 2);
            this.lblLineUnderTitle.TabIndex = 5;
            // 
            // borderLineTop
            // 
            this.borderLineTop.BackColor = System.Drawing.Color.Teal;
            this.borderLineTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.borderLineTop.Location = new System.Drawing.Point(0, 0);
            this.borderLineTop.MaximumSize = new System.Drawing.Size(0, 1);
            this.borderLineTop.MinimumSize = new System.Drawing.Size(0, 1);
            this.borderLineTop.Name = "borderLineTop";
            this.borderLineTop.Size = new System.Drawing.Size(378, 1);
            this.borderLineTop.TabIndex = 8;
            // 
            // borderLineLeft
            // 
            this.borderLineLeft.BackColor = System.Drawing.Color.Teal;
            this.borderLineLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.borderLineLeft.Location = new System.Drawing.Point(0, 1);
            this.borderLineLeft.MaximumSize = new System.Drawing.Size(1, 0);
            this.borderLineLeft.MinimumSize = new System.Drawing.Size(1, 0);
            this.borderLineLeft.Name = "borderLineLeft";
            this.borderLineLeft.Size = new System.Drawing.Size(1, 425);
            this.borderLineLeft.TabIndex = 6;
            // 
            // borderLineRight
            // 
            this.borderLineRight.BackColor = System.Drawing.Color.Teal;
            this.borderLineRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.borderLineRight.Location = new System.Drawing.Point(377, 1);
            this.borderLineRight.MaximumSize = new System.Drawing.Size(1, 0);
            this.borderLineRight.MinimumSize = new System.Drawing.Size(1, 0);
            this.borderLineRight.Name = "borderLineRight";
            this.borderLineRight.Size = new System.Drawing.Size(1, 425);
            this.borderLineRight.TabIndex = 9;
            // 
            // txtTitle
            // 
            this.txtTitle.BackColor = System.Drawing.Color.PaleTurquoise;
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtTitle.Location = new System.Drawing.Point(1, 1);
            this.txtTitle.MaximumSize = new System.Drawing.Size(10000, 500);
            this.txtTitle.MinimumSize = new System.Drawing.Size(0, 22);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.ReadOnly = false;
            this.txtTitle.Size = new System.Drawing.Size(376, 22);
            this.txtTitle.TabIndex = 3;
            this.txtTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtTitle_MouseDown);
            this.txtTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtTitle_MouseMove);
            this.txtTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txtTitle_MouseUp);
            this.txtTitle.DoubleClick += new System.EventHandler(this.txtTitle_DoubleClick);
            this.txtTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // sizeGripResizer
            // 
            this.sizeGripResizer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sizeGripResizer.Location = new System.Drawing.Point(354, 400);
            this.sizeGripResizer.Margin = new System.Windows.Forms.Padding(2);
            this.sizeGripResizer.MaximumSize = new System.Drawing.Size(18, 20);
            this.sizeGripResizer.MinimumSize = new System.Drawing.Size(18, 20);
            this.sizeGripResizer.Name = "sizeGripResizer";
            this.sizeGripResizer.Size = new System.Drawing.Size(18, 20);
            this.sizeGripResizer.TabIndex = 12;
            this.sizeGripResizer.Visible = false;
            // 
            // titleRightClickMenu
            // 
            this.titleRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCopyJobName,
            this.mnuCopyJobDefinition,
            this.mnuSeparator1,
            this.mnuDisable,
            this.mnuDisableConsiderAsSuccess,
            this.mnuEnable,
            this.mnuCloneTask,
            this.mnuSeparator2,
            this.mnuRunAgain,
            this.mnuResetExecution,
            this.mnuForceRunNow,
            this.mnuSeparator3,
            this.mnuKillTask});
            this.titleRightClickMenu.Name = "canvasRightClickMenu";
            this.titleRightClickMenu.Size = new System.Drawing.Size(282, 264);
            // 
            // mnuCopyJobName
            // 
            this.mnuCopyJobName.Name = "mnuCopyJobName";
            this.mnuCopyJobName.Size = new System.Drawing.Size(281, 22);
            this.mnuCopyJobName.Text = "Copy Job Name";
            this.mnuCopyJobName.Click += new System.EventHandler(this.mnuCopyJobName_Click);
            // 
            // mnuCopyJobDefinition
            // 
            this.mnuCopyJobDefinition.Name = "mnuCopyJobDefinition";
            this.mnuCopyJobDefinition.Size = new System.Drawing.Size(281, 22);
            this.mnuCopyJobDefinition.Text = "Copy Job Definition";
            this.mnuCopyJobDefinition.Click += new System.EventHandler(this.mnuCopyJobDefinition_Click);
            // 
            // mnuDisable
            // 
            this.mnuDisable.Name = "mnuDisable";
            this.mnuDisable.Size = new System.Drawing.Size(281, 22);
            this.mnuDisable.Text = "Disable (break workflow)";
            this.mnuDisable.Click += new System.EventHandler(this.mnuDisable_Click);
            // 
            // mnuDisableConsiderAsSuccess
            // 
            this.mnuDisableConsiderAsSuccess.Name = "mnuDisableConsiderAsSuccess";
            this.mnuDisableConsiderAsSuccess.Size = new System.Drawing.Size(281, 22);
            this.mnuDisableConsiderAsSuccess.Text = "Disable (but let workflow pass through)";
            this.mnuDisableConsiderAsSuccess.Click += new System.EventHandler(this.mnuDisableConsiderAsSuccess_Click);
            // 
            // mnuEnable
            // 
            this.mnuEnable.Name = "mnuEnable";
            this.mnuEnable.Size = new System.Drawing.Size(281, 22);
            this.mnuEnable.Text = "Enable";
            this.mnuEnable.Click += new System.EventHandler(this.mnuEnable_Click);
            // 
            // mnuCloneTask
            // 
            this.mnuCloneTask.Name = "mnuCloneTask";
            this.mnuCloneTask.Size = new System.Drawing.Size(281, 22);
            this.mnuCloneTask.Text = "Clone this task";
            this.mnuCloneTask.Click += new System.EventHandler(this.mnuCloneTask_Click);
            // 
            // mnuSeparator2
            // 
            this.mnuSeparator2.Name = "mnuSeparator2";
            this.mnuSeparator2.Size = new System.Drawing.Size(278, 6);
            // 
            // mnuRunAgain
            // 
            this.mnuRunAgain.Name = "mnuRunAgain";
            this.mnuRunAgain.Size = new System.Drawing.Size(281, 22);
            this.mnuRunAgain.Text = "Run again";
            this.mnuRunAgain.Click += new System.EventHandler(this.mnuRunAgain_Click);
            // 
            // mnuResetExecution
            // 
            this.mnuResetExecution.Name = "mnuResetExecution";
            this.mnuResetExecution.Size = new System.Drawing.Size(281, 22);
            this.mnuResetExecution.Text = "Reset Execution";
            this.mnuResetExecution.Click += new System.EventHandler(this.mnuResetExecution_Click);
            // 
            // mnuForceRunNow
            // 
            this.mnuForceRunNow.Name = "mnuForceRunNow";
            this.mnuForceRunNow.Size = new System.Drawing.Size(281, 22);
            this.mnuForceRunNow.Text = "Force Run Now";
            this.mnuForceRunNow.Click += new System.EventHandler(this.mnuForceRunNow_Click);
            // 
            // mnuSeparator3
            // 
            this.mnuSeparator3.Name = "mnuSeparator3";
            this.mnuSeparator3.Size = new System.Drawing.Size(278, 6);
            // 
            // mnuKillTask
            // 
            this.mnuKillTask.Name = "mnuKillTask";
            this.mnuKillTask.Size = new System.Drawing.Size(281, 22);
            this.mnuKillTask.Text = "Kill Task (not advised)";
            this.mnuKillTask.Click += new System.EventHandler(this.mnuKillTask_Click);
            // 
            // mmoDescription
            // 
            this.mmoDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mmoDescription.EditValue = "";
            this.mmoDescription.Location = new System.Drawing.Point(1, 25);
            this.mmoDescription.Name = "mmoDescription";
            this.mmoDescription.Properties.ReadOnly = true;
            this.mmoDescription.Size = new System.Drawing.Size(376, 400);
            this.mmoDescription.TabIndex = 13;
            // 
            // mnuSeparator1
            // 
            this.mnuSeparator1.Name = "mnuSeparator1";
            this.mnuSeparator1.Size = new System.Drawing.Size(278, 6);
            // 
            // JobView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sizeGripResizer);
            this.Controls.Add(this.mmoDescription);
            this.Controls.Add(this.borderLineBottom);
            this.Controls.Add(this.lblLineUnderTitle);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.borderLineLeft);
            this.Controls.Add(this.borderLineRight);
            this.Controls.Add(this.borderLineTop);
            this.Name = "JobView";
            this.Size = new System.Drawing.Size(378, 426);
            this.titleRightClickMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mmoDescription.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TechnicalTools.UI.Controls.LabelToTextBox txtTitle;
        private System.Windows.Forms.Label lblLineUnderTitle;
        private System.Windows.Forms.Label borderLineLeft;
        private System.Windows.Forms.Label borderLineTop;
        private System.Windows.Forms.Label borderLineRight;
        private System.Windows.Forms.Label borderLineBottom;
        private TechnicalTools.UI.Controls.SizeGripResizer sizeGripResizer;
        private System.Windows.Forms.ContextMenuStrip titleRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuResetExecution;
        private System.Windows.Forms.ToolStripMenuItem mnuKillTask;
        private System.Windows.Forms.ToolTip addToolTipPropertyOnAllControl;
        private System.Windows.Forms.ToolStripMenuItem mnuForceRunNow;
        private System.Windows.Forms.ToolStripMenuItem mnuDisable;
        private System.Windows.Forms.ToolStripMenuItem mnuEnable;
        private System.Windows.Forms.ToolStripMenuItem mnuCloneTask;
        private System.Windows.Forms.ToolStripMenuItem mnuRunAgain;
        private System.Windows.Forms.ToolStripSeparator mnuSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuCopyJobName;
        private System.Windows.Forms.ToolStripSeparator mnuSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuCopyJobDefinition;
        private System.Windows.Forms.ToolStripMenuItem mnuDisableConsiderAsSuccess;
        private TechnicalTools.UI.DX.Controls.MemoEdit mmoDescription;
        private System.Windows.Forms.ToolStripSeparator mnuSeparator1;
    }
}
