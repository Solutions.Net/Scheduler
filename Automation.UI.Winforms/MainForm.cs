﻿using System;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;


namespace Automation.UI.Winforms
{
    public partial class MainForm : EnhancedXtraForm
    {
        string _filenameToOpen;
        bool _runGlobalScheduler;
        bool _runDailyScheduler;

        public MainForm(string filenameToOpen, bool runGlobalScheduler, bool runDailyScheduler)
        {
            if (runGlobalScheduler && filenameToOpen == null)
                throw new Exception("Argument --open=\"filename\" needs to be specified in order to argument --runGlobalScheduler to be effective!");
            if (runDailyScheduler && filenameToOpen == null)
                throw new Exception("Argument --open=\"filename\" needs to be specified in order to argument --runDailyScheduler to be effective!");

            _filenameToOpen = filenameToOpen;
            _runGlobalScheduler = runGlobalScheduler;
            _runDailyScheduler = runDailyScheduler;
            InitializeComponent();
        }
        readonly DefaultExceptionHandler defaultExceptionHandler = new DefaultExceptionHandler();

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            EnhancedXtraForm.MainForm = this;
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (_filenameToOpen == null && DebugTools.IsForDevelopper &&
                DialogResult.Yes == XtraMessageBox.Show(this, "Load sample/demo schedule (yes) or continue with empty schedule (no) ?", Application.ProductName, MessageBoxButtons.YesNo))
            {
                _filenameToOpen = @"..\..\..\..\sample_schedule.xml";
            }
            BusyForm.DefaultExceptionHandling += BusyForm_DefaultExceptionHandling;
            if (!string.IsNullOrWhiteSpace(_filenameToOpen))
                BeginInvoke((Action)(() =>
                {
                    var opened = globalJobSchedulerView.Open(_filenameToOpen);
                    if (opened && _runGlobalScheduler)
                        BeginInvoke((Action)(() => globalJobSchedulerView.StartScheduler()));
                    if (opened && _runDailyScheduler)
                        BeginInvoke((Action)(() => globalJobSchedulerView.InstanciateADailySchedulerNow()));
                }));
        }

        private void BusyForm_DefaultExceptionHandling(string msg, Exception ex)
        {
            XtraMessageBox.Show(this, $"Error while \"{msg}\": " + Environment.NewLine + Environment.NewLine +
                                ex.ToString(), "Unexpected error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
