﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.CompilerServices;

using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;


namespace Automation.UI.Winforms
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            RuntimeHelpers.RunClassConstructor(typeof(ExceptionManager).TypeHandle);
            RuntimeHelpers.RunClassConstructor(typeof(AssertionManager).TypeHandle);

            LogManager.Default = new LogManager();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            BonusSkins.Register();
            SkinManager.EnableFormSkins();
            UserLookAndFeel.Default.SetSkinStyle("DevExpress Style");
            string filename = args.Where(a => a.ToLower().StartsWith("--open="))
                                  .Select(a => a.Substring("--open=".Length))
                                  .LastOrDefault();
            bool runGlobalScheduler = args.Any(a => a.ToLower() == "--runglobalscheduler")
                                  || args.Where(a => a.ToLower().StartsWith("--runglobalscheduler="))
                                         .DefaultIfEmpty("--runglobalscheduler=false")
                                         .Select(a => a.Substring("--runglobalscheduler=".Length))
                                         .Last().ToLower()
                                         .In("true", "1", "yes");
           
            bool runDailyScheduler = args.Any(a => a.ToLower() == "--rundailyscheduler")
                                  || args.Where(a => a.ToLower().StartsWith("--rundailyscheduler="))
                                         .DefaultIfEmpty("--rundailyscheduler=false")
                                         .Select(a => a.Substring("--rundailyscheduler=".Length))
                                         .Last().ToLower()
                                         .In("true", "1", "yes");

            try
            {
                Application.Run(new MainForm(filename, runGlobalScheduler, runDailyScheduler));
                return 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ExceptionManager.Instance.Format(ex));
                return 1;
            }
        }
    }
}
