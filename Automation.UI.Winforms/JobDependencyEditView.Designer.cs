﻿namespace Automation.UI.Winforms
{
    partial class JobDependencyEditView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.propertyGridControl = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.propertyDescriptionControl = new DevExpress.XtraVerticalGrid.PropertyDescriptionControl();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
            this.splitContainerControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // propertyGridControl
            // 
            this.propertyGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridControl.Location = new System.Drawing.Point(0, 0);
            this.propertyGridControl.MinimumSize = new System.Drawing.Size(100, 50);
            this.propertyGridControl.Name = "propertyGridControl";
            this.propertyGridControl.Size = new System.Drawing.Size(304, 449);
            this.propertyGridControl.TabIndex = 1;
            this.propertyGridControl.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.propertyGridControl_ShowingEditor);
            // 
            // splitContainerControl
            // 
            this.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl.Horizontal = false;
            this.splitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl.Name = "splitContainerControl";
            this.splitContainerControl.Panel1.Controls.Add(this.propertyGridControl);
            this.splitContainerControl.Panel1.Text = "Panel1";
            this.splitContainerControl.Panel2.Controls.Add(this.propertyDescriptionControl);
            this.splitContainerControl.Panel2.Text = "Panel2";
            this.splitContainerControl.Size = new System.Drawing.Size(304, 554);
            this.splitContainerControl.TabIndex = 2;
            this.splitContainerControl.Text = "splitContainerControl1";
            // 
            // propertyDescriptionControl
            // 
            this.propertyDescriptionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyDescriptionControl.Location = new System.Drawing.Point(0, 0);
            this.propertyDescriptionControl.Name = "propertyDescriptionControl";
            this.propertyDescriptionControl.PropertyGrid = this.propertyGridControl;
            this.propertyDescriptionControl.Size = new System.Drawing.Size(304, 100);
            this.propertyDescriptionControl.TabIndex = 2;
            this.propertyDescriptionControl.TabStop = false;
            // 
            // JobDependencyEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl);
            this.Name = "JobDependencyEditView";
            this.Size = new System.Drawing.Size(304, 554);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
            this.splitContainerControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl;
        private DevExpress.XtraVerticalGrid.PropertyDescriptionControl propertyDescriptionControl;
    }
}
