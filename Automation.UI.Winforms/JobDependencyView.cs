﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

using TechnicalTools.Algorithm.Geometry2D;

using Automation.Model;


namespace Automation.UI.Winforms
{
    public class JobDependencyView : ISelectableView, IDisposable
    {
        public JobDiagramView OwnerView { get; }

        public JobDependency JobDependency { get; }

        public Point  From { get; set; }
        public Point? To   { get; set; }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value)
                    return;
                _isSelected = value;
                SelectionChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        bool _isSelected;
        public event EventHandler SelectionChanged;

        public JobDependencyView(JobDependency jobDependency, JobDiagramView ownerView)
        {
            OwnerView = ownerView;
            JobDependency = jobDependency;

            OwnerView.JobViews[JobDependency.Source].LocationChanged += SourceOrTarget_LocationOrSizeChanged;
            OwnerView.JobViews[JobDependency.Source].SizeChanged += SourceOrTarget_LocationOrSizeChanged;
            if (JobDependency.Target != null)
            {
                OwnerView.JobViews[JobDependency.Target].LocationChanged += SourceOrTarget_LocationOrSizeChanged;
                OwnerView.JobViews[JobDependency.Target].SizeChanged += SourceOrTarget_LocationOrSizeChanged;
            }
            JobDependency.PropertyChanged += JobDependency_PropertyChanged;
            ProcessFromToPoints();
        }

        public void Dispose()
        {
            OwnerView.JobViews[JobDependency.Source].LocationChanged -= SourceOrTarget_LocationOrSizeChanged;
            OwnerView.JobViews[JobDependency.Source].SizeChanged -= SourceOrTarget_LocationOrSizeChanged;
            if (JobDependency.Target != null)
            {
                OwnerView.JobViews[JobDependency.Target].LocationChanged -= SourceOrTarget_LocationOrSizeChanged;
                OwnerView.JobViews[JobDependency.Target].SizeChanged -= SourceOrTarget_LocationOrSizeChanged;
            }
        }

        private void JobDependency_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (OwnerView.InvokeRequired)
            {
                OwnerView.BeginInvoke((Action)(() => JobDependency_PropertyChanged(sender, e)));
                return;
            }
            Invalidated?.Invoke(this, EventArgs.Empty);
        }

        private void SourceOrTarget_LocationOrSizeChanged(object sender, EventArgs e)
        {
            ProcessFromToPoints();
        }
        public void ProcessFromToPoints()
        {
            var fromView = OwnerView.JobViews[JobDependency.Source];
            From = fromView.Center; // default value in case we cannot find a value
            To = null; // we will not be able to draw this arrow, so...

            if (JobDependency.Target == null) // dependency is maybe building ...
                return;
            var toView = OwnerView.JobViews[JobDependency.Target];
            var line = new SegmentD(fromView.Center, toView.Center);

            var fromBounds = new RectangleD(fromView.Left, fromView.Top, fromView.Width, fromView.Height);
            var res = fromBounds.GetIntersectionsWith(line).OfType<PointD?>().FirstOrDefault();
            if (res != null)
                From = res.Value.ToWindowPoint();
            else // possible if one JobView is overlaying another one
                From = fromView.Center;

            var toBounds = new RectangleD(toView.Left, toView.Top, toView.Width, toView.Height);
            res = toBounds.GetIntersectionsWith(line).OfType<PointD?>().FirstOrDefault();
            if (res != null)
                To = res.Value.ToWindowPoint();
            else // possible if one JobView is overlaying another one
                To = toView.Center;
            Invalidated?.Invoke(this, EventArgs.Empty);
        }
        public event EventHandler Invalidated;

        internal void Draw(Graphics g)
        {
            if (!To.HasValue)
                return;
            var mode = g.SmoothingMode;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            var color = JobDependency.Source.JobKind == eJobKind.Restart 
                             ? !IsSelected && 
                               JobDependency.Target != null &&  // handle temporary case when dependency is being created by user
                               JobDependency.Target.ToTargetsDependencies.Any(dep => dep.Target == JobDependency.Source) 
                                ? Color.White 
                                : Color.Black
                      : JobDependency.SourceJobState == eSourceJobState.Success ? Color.Green
                      : JobDependency.SourceJobState == eSourceJobState.Error ? Color.Red
                      : Color.Orange;
            var colorSelected = JobDependency.Source.JobKind == eJobKind.Restart ? Color.FromArgb(255,188,188)
                              : JobDependency.SourceJobState == eSourceJobState.Success ? Color.GreenYellow
                              : JobDependency.SourceJobState == eSourceJobState.Error ? Color.DarkOrange
                              : Color.Yellow;
            using (Pen p = new Pen(color))
            // Dessine la même chose qu'en bas mais en plus gros afin d'entourer en jaune la fleche
            using (Pen pSelected = IsSelected ? new Pen(colorSelected, JobDependency.Source.JobKind == eJobKind.Restart ? 5 : 12) : null)
            {
                p.StartCap = LineCap.Square;
                
                double angle = Math.Atan2(To.Value.Y - From.Y, To.Value.X - From.X);

                // Dessine seulement la flèche d'arrivé
                var headShape = JobDependency.Source.JobKind == eJobKind.Restart ? LineCap.Round : LineCap.ArrowAnchor;
                int headWidth = JobDependency.Source.JobKind == eJobKind.Restart ? 5 : 10;
                var headDashStyle = DashStyle.Solid;
                int segmentWidth = JobDependency.Source.JobKind == eJobKind.Restart ? 2 : 4;
                var segmentShape = LineCap.Flat;
                var segmentDashStyle = JobDependency.Source.JobKind == eJobKind.Restart ? DashStyle.Dot : DashStyle.Solid;

                if (IsSelected)
                { 
                    if (JobDependency.Source.JobKind != eJobKind.Restart)
                    {
                        pSelected.EndCap = headShape;
                        pSelected.Width = headWidth + 2;
                        pSelected.DashStyle = headDashStyle;
                        g.DrawLine(pSelected, To.Value.X, To.Value.Y, To.Value.X + (float)Math.Cos(angle), To.Value.Y + (float)(Math.Sin(angle)));
                    }
                    pSelected.Width = segmentWidth + 2;
                    pSelected.EndCap = segmentShape;
                    pSelected.DashStyle = DashStyle.Solid; //segmentDashStyle;
                    g.DrawLine(pSelected, From, To.Value);
                }

                if (JobDependency.Source.JobKind != eJobKind.Restart)
                {
                    p.EndCap = headShape;
                    p.Width = headWidth;
                    p.DashStyle = headDashStyle;
                    g.DrawLine(p, To.Value.X, To.Value.Y, To.Value.X + (float)Math.Cos(angle), To.Value.Y + (float)(Math.Sin(angle)));
                }
                p.Width = segmentWidth;
                p.EndCap = segmentShape;
                p.DashStyle = segmentDashStyle;
                g.DrawLine(p, From, To.Value);
            }
            g.SmoothingMode = mode;
        }


    }
}
