﻿using System;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX;


namespace Automation.UI.Winforms
{
    static class ViewHelper
    {
        /// <summary>
        /// Because on Windows server 2008 SP1 CreateHandle method often fails (dont know why)
        /// </summary>
        public static void CreateHandleSafely(Action createHanle)
        {
            while (true)
            {
                try
                {
                    createHanle();
                    break;
                }
                catch (Exception ex)
                {
                    var answer = XtraMessageBox.Show(Application.OpenForms[0], "Cannot create handle because of following error. Try again ?" + Environment.NewLine +
                                                     Environment.NewLine +
                                                     ExceptionManager.Instance.Format(ex), "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    if (answer == DialogResult.No)
                        throw;
                }
            }
        }

        public static bool ShowBusyWhileDoingUIWork(Control ctl, string actionName, Action action)
        {
            // On Windows server, using ShowBusyWhileDoingUIWork will make application crash in ntdll (see https://www.devexpress.com/Support/Center/Question/Details/T549269/crash-in-ntdll-dll-ntyieldexecution-0-just-after-getinfobythread-is-called)
            // So we dont show any busy form...
            if (TechnicalTools.Tools.OsInfo.GetOSInfo() == TechnicalTools.Tools.OsInfo.eWindowsVersion.WindowsServer2008R2Standard)
            {
                try
                {
                    action();
                    return true;
                }
                catch
                {
                    DebugTools.Break();
                    return false;
                }
            }
            return ctl.ShowBusyWhileDoingUIWorkInPlace(actionName, pr => action());
        }

    }
}
