﻿namespace Automation.UI.Winforms
{
    partial class SharedResourcesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcSharedResource = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvSharedResource = new TechnicalTools.UI.DX.EnhancedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcSharedResource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSharedResource)).BeginInit();
            this.SuspendLayout();
            // 
            // gcSharedResource
            // 
            this.gcSharedResource.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcSharedResource.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcSharedResource.EmbeddedNavigator.TextStringFormat = "Resource {0} of {1} (visible) of {2} (total)";
            this.gcSharedResource.Location = new System.Drawing.Point(0, 0);
            this.gcSharedResource.MainView = this.gvSharedResource;
            this.gcSharedResource.Name = "gcSharedResource";
            this.gcSharedResource.Size = new System.Drawing.Size(757, 200);
            this.gcSharedResource.TabIndex = 0;
            this.gcSharedResource.UseEmbeddedNavigator = true;
            this.gcSharedResource.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSharedResource});
            // 
            // gvSharedResource
            // 
            this.gvSharedResource.GridControl = this.gcSharedResource;
            this.gvSharedResource.Name = "gvSharedResource";
            // 
            // SharedResourcesForm
            // 
            this.ClientSize = new System.Drawing.Size(757, 262);
            this.Controls.Add(this.gcSharedResource);
            this.Name = "SharedResourcesForm";
            this.Load += new System.EventHandler(this.SharedResourcesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcSharedResource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSharedResource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcSharedResource;
        private TechnicalTools.UI.DX.EnhancedGridView gvSharedResource;
    }
}