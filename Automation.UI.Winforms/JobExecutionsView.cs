﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using Automation.Model;


namespace Automation.UI.Winforms
{
    [DesignTimeVisible(true)]
    [ToolboxItem(true)]
    public partial class JobExecutionsView : EnhancedXtraUserControl
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Control        OwnerView     { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public JobDiagram     Diagram       { get { return _Diagram;                    } set { SetDiagram(value); } } JobDiagram _Diagram;
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<Job>      Filter        { get { return _FilteredJobs.Keys.ToList(); } set { SetFilter(value);  } } Dictionary<Job, Job> _FilteredJobs = new Dictionary<Job, Job>();

        readonly List<Job> _allJobListened = new List<Job>();
        readonly FixedBindingList<JobExecution> _allExecutions = new FixedBindingList<JobExecution>();
        readonly FixedBindingList<JobExecutionForDisplay> _displayedExecutions = new FixedBindingList<JobExecutionForDisplay>();

        class JobExecutionForDisplay : PropertyChangedUIDispatch<JobExecution>
        {
            public JobExecution Execution { get { return Item; } }

            public string        JobName             { get { return Execution.Job.JobName; } }
            public DateTime      StartDate           { get { return Execution.StartDate; } }
            public string        StandardOutput      { get { return Execution.StandardOutput; } }
            public string        StandardError       { get { return Execution.StandardError; } }
            public int?          ExitCode            { get { return Execution.ExitCode; } }
            public string        UnknownError        { get { return Execution.UnknownError; } }
            public TimeSpan?     Duration            { get { return Execution.Duration; } }
            public bool          ForcedExecution     { get { return Execution.ForcedExecution; } }
            public bool          Killed              { get { return Execution.Killed; } }
            
            public bool?         IsSuccess           { get { return Execution.IsSuccess; } }
            public DateTime?     EndDate             { get { return Execution.StartDate + Execution.Duration; } }

            public JobExecutionForDisplay(JobExecution exec)
                : base(exec)
            {
            }
        }

        public JobExecutionsView()
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;

            ViewHelper.CreateHandleSafely(() => CreateHandle()); // before any event subscribing
        }

        private void JobExecutionsView_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            colJobName.FieldName = nameof(JobExecutionForDisplay.JobName);
            colStartDate.FieldName = nameof(JobExecutionForDisplay.StartDate);
            gvExecutions.ColumnFormatChanger.DisplayDateAndTimeFor(colStartDate);
            colDuration.FieldName = nameof(JobExecutionForDisplay.Duration);
            //gvExecutions.ColumnFormatChanger.DisplayTimeOnlyFor(colDuration);
            colEndDate.FieldName = nameof(JobExecutionForDisplay.EndDate);
            gvExecutions.ColumnFormatChanger.DisplayDateAndTimeFor(colEndDate);
            colIsForced.FieldName = nameof(JobExecutionForDisplay.ForcedExecution);
            colIsKilled.FieldName = nameof(JobExecutionForDisplay.Killed);
            colIsSuccess.FieldName = nameof(JobExecutionForDisplay.IsSuccess);
            colExitCode.FieldName = nameof(JobExecutionForDisplay.ExitCode);
            colStdOutput.FieldName = nameof(JobExecutionForDisplay.StandardOutput);
            colErrOutput.FieldName = nameof(JobExecutionForDisplay.StandardError);
            colSchedulerInternalException.FieldName = nameof(JobExecutionForDisplay.UnknownError);

            gcExecutions.DataSource = _displayedExecutions;
            
            Disposed += JobExecutionView_Disposed;
        }

        void JobExecutionView_Disposed(object sender, EventArgs e)
        {
            Diagram = null;
            foreach (var item in _displayedExecutions)
                item.Dispose();
            _displayedExecutions.Clear();
        }

        void SetDiagram(JobDiagram newDiagram)
        {
            if (_Diagram == newDiagram)
                return;
            if (_Diagram != null && newDiagram != null)
                throw new Exception("Does not support changing diagram after first set. Create a new control.");
            if (_Diagram != null)
            {
                Diagram.UnsubscribeFromJobs(Jobs_ListChanged);
                _FilteredJobs.Clear();
                ResetJobs(new List<Job>());
            }
            _Diagram = newDiagram;
            if (Diagram != null)
            {
                var jobs = Diagram.GetJobsAndSubscribe(Jobs_ListChanged);
                ResetJobs(jobs);
            }
            DefineEnabilitiesAndVisibilities();
        }
        void SetFilter(IEnumerable<Job> jobs)
        {
            _FilteredJobs = jobs.ToDictionary(j => j, ReferenceEqualityComparer<Job>.Default);
            if (Diagram != null)
                ResetJobExecutions(null);
        }



        private void Jobs_ListChanged(object sender, IAfterChangeEventArgs<Job> e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => Jobs_ListChanged(sender, e)));
                return;
            }

            if (e.ListChangedType == ListChangedType.ItemAdded)
                OnDiagramJobAdded(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
                OnDiagramJobRemoved(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.Reset)
                ResetJobs(Diagram.GetJobs());
        }
        void ResetJobs(IReadOnlyCollection<Job> jobs)
        {
            foreach (var removedJob in _allJobListened.Except(jobs).ToList())
                OnDiagramJobRemoved(removedJob);
            foreach (var addedJob in jobs.Except(_allJobListened).ToList())
                OnDiagramJobAdded(addedJob);
        }
        void OnDiagramJobAdded(Job job)
        {
            _allJobListened.Add(job);
            job.PropertyChanged += Job_PropertyChanged;
            job.UnsubscribeFromExecutionHistory(JobExecutionHistory_ListChanged); // just for safety
            var executions = job.GetExecutionHistoryAndSubscribe(JobExecutionHistory_ListChanged, true);
            _allExecutions.AddRange(executions);
            if (_FilteredJobs.Count == 0 || _FilteredJobs.ContainsKey(job))
                _displayedExecutions.AddRange(executions.Select(exec => new JobExecutionForDisplay(exec)));
        }

        void OnDiagramJobRemoved(Job job)
        {
            _allJobListened.Remove(job);
            job.UnsubscribeFromExecutionHistory(JobExecutionHistory_ListChanged);
            job.PropertyChanged -= Job_PropertyChanged;
            BeginInvoke((Action)(() => // Postpone in case some event are in queue to trigger OnDiagramJobExecutionRemoved on this job
            {
                for (int i = _allExecutions.Count - 1; i >= 0; --i)
                    if (_allExecutions[i].Job == job)
                        OnDiagramJobExecutionRemoved(_allExecutions[i]);
            }));
        }

        private void Job_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => Job_PropertyChanged(sender, e)));
                return;
            }
            if (e.PropertyName == nameof(Job.Execution))
            {
                var job = sender as Job;
                var execution = job.Execution; // can be null because volatile
                if (execution != null) // Not a problem, execution object is supposed to be move to ExecutionHistory, which we listen too
                    OnDiagramJobExecutionAdded(execution);
            }
        }

        private void JobExecutionHistory_ListChanged(object sender, IAfterChangeEventArgs<JobExecution> e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => JobExecutionHistory_ListChanged(sender, e)));
                return;
            }

            if (e.ListChangedType == ListChangedType.ItemAdded)
                OnDiagramJobExecutionAdded(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
                OnDiagramJobExecutionRemoved(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.Reset)
                ResetJobExecutions(null);
        }
        // Important : ResetJobExecutions does not reset all execution of one job but all
        // Because we cannot know from which job sender (list) belongs 
        void ResetJobExecutions(IReadOnlyCollection<JobExecution> executions = null)
        {
            executions = executions ?? Diagram.GetJobs().SelectMany(j => j.GetExecutionHistory(true)).ToList();
            _displayedExecutions.WithListChangedEventsDisabled(() =>
            _allExecutions.WithListChangedEventsDisabled(() =>
            {
                // Remove all and add all again so it updates _displayedExecutions too
                foreach (var removedExec in _allExecutions.ToList())
                    OnDiagramJobExecutionRemoved(removedExec);
                foreach (var addedExec in executions.ToList())
                    OnDiagramJobExecutionAdded(addedExec);
            }));
        }
        void OnDiagramJobExecutionAdded(JobExecution execution)
        {
            // Can happens for Job.Execution which is placed in ExecutionHistory when executions is terminated
            if (_allExecutions.Contains(execution)) 
                return;
            _allExecutions.Add(execution);
            if (_FilteredJobs.Count == 0 || _FilteredJobs.ContainsKey(execution.Job))
            {
                var item = new JobExecutionForDisplay(execution);
                _displayedExecutions.Add(item);
                if (chkFocusNewExecution.Checked && !gvExecutions.IsEditing)
                    gvExecutions.FocusedBusinessObject(item);
            }
        }
        void OnDiagramJobExecutionRemoved(JobExecution execution)
        {
            for (int i = _displayedExecutions.Count - 1; i >= 0; --i)
                if (_displayedExecutions[i].Execution == execution)
                {
                    _displayedExecutions[i].Dispose();
                    _displayedExecutions.RemoveAt(i);
                    break;
                }
            _allExecutions.Remove(execution);
        }

        private void JobExecution_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (OwnerView.InvokeRequired)
            {
                OwnerView.BeginInvoke((Action)(() => JobExecution_PropertyChanged(sender, e)));
                return;
            }
        }

        void DefineEnabilitiesAndVisibilities()
        {
        }



        new IAsyncResult BeginInvoke(Delegate method)
        {
            if (!Disposing && !IsDisposed)
                return Application.OpenForms[0].BeginInvoke(method);
            Action a = () => { };
            return a.BeginInvoke(null, null);
        }
        new bool InvokeRequired
        {
            get
            {
                return Application.OpenForms[0].InvokeRequired;
            }
        }

        private void gvExecutions_RowItemDoubleClick(object sender, RowItemDoubleClickEventArgs e)
        {
            var execution = (gvExecutions.GetRow(e.RowHandle) as JobExecutionForDisplay)?.Execution;
            if (execution == null)
                return;
            Form view;
            if (_executionForms.TryGetValue(execution, out view))
            {
                view.TopMost = true;
                view.BeginInvoke((Action)(() => view.TopMost = false));
                return;
            }
            var frm = JobExecutionView.ShowInForm(execution, this);
            _executionForms.Add(execution, frm);
            frm.Disposed += (_, __) => _executionForms.Remove(execution);
        }
        readonly Dictionary<JobExecution, Form> _executionForms = new Dictionary<JobExecution, Form>();
    }
}
