﻿namespace Automation.UI.Winforms
{
    partial class JobDefinitionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lytCtlDefinition = new DevExpress.XtraLayout.LayoutControl();
            this.tseStopAfter = new DevExpress.XtraEditors.TimeSpanEdit();
            this.txtWarningCode = new DevExpress.XtraEditors.TextEdit();
            this.chkResetTasksRecursively = new DevExpress.XtraEditors.CheckEdit();
            this.chkConsiderAsSuccess = new DevExpress.XtraEditors.CheckEdit();
            this.chkEnabled = new DevExpress.XtraEditors.CheckEdit();
            this.tseTimeOut = new DevExpress.XtraEditors.TimeSpanEdit();
            this.tseExpectedDuration = new DevExpress.XtraEditors.TimeSpanEdit();
            this.teUrlLink = new DevExpress.XtraEditors.TextEdit();
            this.tseWaitUntil = new DevExpress.XtraEditors.TimeSpanEdit();
            this.tseWaitDelay = new DevExpress.XtraEditors.TimeSpanEdit();
            this.lueJobKind = new DevExpress.XtraEditors.LookUpEdit();
            this.meDescription = new DevExpress.XtraEditors.MemoEdit();
            this.txtProcessArguments = new DevExpress.XtraEditors.TextEdit();
            this.txtProcessPath = new DevExpress.XtraEditors.TextEdit();
            this.lytCtlDefinitionGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.txtProcessPath_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtProcessArguments_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.tseExpectedDuration_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.tseTimeOut_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.teUrlLink_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkConsiderAsSuccess_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.tseWaitUntil_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkResetTasksRecursively_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtWarningCode_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.tseStopAfter_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.tseWaitDelay_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.chkEnabled_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lueJobKind_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.meDescription_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.addToolTipPropertyOnAllControl = new System.Windows.Forms.ToolTip();
            this.chkRequiredResources = new TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit();
            this.chkRequiredResources_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlDefinition)).BeginInit();
            this.lytCtlDefinition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tseStopAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarningCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkResetTasksRecursively.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsiderAsSuccess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseTimeOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseExpectedDuration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUrlLink.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitUntil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitDelay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueJobKind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessArguments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlDefinitionGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessPath_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessArguments_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseExpectedDuration_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseTimeOut_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUrlLink_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsiderAsSuccess_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitUntil_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkResetTasksRecursively_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarningCode_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseStopAfter_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitDelay_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueJobKind_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDescription_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequiredResources_LayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // lytCtlDefinition
            // 
            this.lytCtlDefinition.Controls.Add(this.chkRequiredResources);
            this.lytCtlDefinition.Controls.Add(this.tseStopAfter);
            this.lytCtlDefinition.Controls.Add(this.txtWarningCode);
            this.lytCtlDefinition.Controls.Add(this.chkResetTasksRecursively);
            this.lytCtlDefinition.Controls.Add(this.chkConsiderAsSuccess);
            this.lytCtlDefinition.Controls.Add(this.chkEnabled);
            this.lytCtlDefinition.Controls.Add(this.tseTimeOut);
            this.lytCtlDefinition.Controls.Add(this.tseExpectedDuration);
            this.lytCtlDefinition.Controls.Add(this.teUrlLink);
            this.lytCtlDefinition.Controls.Add(this.tseWaitUntil);
            this.lytCtlDefinition.Controls.Add(this.tseWaitDelay);
            this.lytCtlDefinition.Controls.Add(this.lueJobKind);
            this.lytCtlDefinition.Controls.Add(this.meDescription);
            this.lytCtlDefinition.Controls.Add(this.txtProcessArguments);
            this.lytCtlDefinition.Controls.Add(this.txtProcessPath);
            this.lytCtlDefinition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytCtlDefinition.Location = new System.Drawing.Point(0, 0);
            this.lytCtlDefinition.Name = "lytCtlDefinition";
            this.lytCtlDefinition.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(697, 203, 677, 605);
            this.lytCtlDefinition.Root = this.lytCtlDefinitionGroup;
            this.lytCtlDefinition.Size = new System.Drawing.Size(404, 421);
            this.lytCtlDefinition.TabIndex = 11;
            this.lytCtlDefinition.Text = "layoutControl1";
            // 
            // tseStopAfter
            // 
            this.tseStopAfter.EditValue = System.TimeSpan.Parse("00:00:00");
            this.tseStopAfter.Location = new System.Drawing.Point(136, 107);
            this.tseStopAfter.Name = "tseStopAfter";
            this.tseStopAfter.Properties.AllowEditDays = false;
            this.tseStopAfter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tseStopAfter.Properties.Mask.EditMask = "HH:mm:ss";
            this.tseStopAfter.Size = new System.Drawing.Size(256, 20);
            this.tseStopAfter.StyleController = this.lytCtlDefinition;
            this.tseStopAfter.TabIndex = 21;
            // 
            // txtWarningCode
            // 
            this.txtWarningCode.Location = new System.Drawing.Point(136, 298);
            this.txtWarningCode.Name = "txtWarningCode";
            this.txtWarningCode.Properties.Mask.EditMask = "n0";
            this.txtWarningCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtWarningCode.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtWarningCode.Size = new System.Drawing.Size(256, 20);
            this.txtWarningCode.StyleController = this.lytCtlDefinition;
            this.txtWarningCode.TabIndex = 20;
            // 
            // chkResetTasksRecursively
            // 
            this.chkResetTasksRecursively.Location = new System.Drawing.Point(136, 131);
            this.chkResetTasksRecursively.Name = "chkResetTasksRecursively";
            this.chkResetTasksRecursively.Properties.Caption = "";
            this.chkResetTasksRecursively.Size = new System.Drawing.Size(256, 19);
            this.chkResetTasksRecursively.StyleController = this.lytCtlDefinition;
            this.chkResetTasksRecursively.TabIndex = 19;
            // 
            // chkConsiderAsSuccess
            // 
            this.chkConsiderAsSuccess.Location = new System.Drawing.Point(206, 36);
            this.chkConsiderAsSuccess.Name = "chkConsiderAsSuccess";
            this.chkConsiderAsSuccess.Properties.Caption = "Consider as successfully executed";
            this.chkConsiderAsSuccess.Size = new System.Drawing.Size(186, 19);
            this.chkConsiderAsSuccess.StyleController = this.lytCtlDefinition;
            this.chkConsiderAsSuccess.TabIndex = 18;
            // 
            // chkEnabled
            // 
            this.chkEnabled.Location = new System.Drawing.Point(136, 36);
            this.chkEnabled.Name = "chkEnabled";
            this.chkEnabled.Properties.Caption = "Enabled";
            this.chkEnabled.Size = new System.Drawing.Size(66, 19);
            this.chkEnabled.StyleController = this.lytCtlDefinition;
            this.chkEnabled.TabIndex = 17;
            // 
            // tseTimeOut
            // 
            this.tseTimeOut.EditValue = System.TimeSpan.Parse("00:00:00");
            this.tseTimeOut.Location = new System.Drawing.Point(136, 274);
            this.tseTimeOut.Name = "tseTimeOut";
            this.tseTimeOut.Properties.AllowEditDays = false;
            this.tseTimeOut.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tseTimeOut.Properties.Mask.EditMask = "HH:mm:ss";
            this.tseTimeOut.Size = new System.Drawing.Size(256, 20);
            this.tseTimeOut.StyleController = this.lytCtlDefinition;
            this.tseTimeOut.TabIndex = 16;
            // 
            // tseExpectedDuration
            // 
            this.tseExpectedDuration.EditValue = null;
            this.tseExpectedDuration.Location = new System.Drawing.Point(136, 250);
            this.tseExpectedDuration.Name = "tseExpectedDuration";
            this.tseExpectedDuration.Properties.AllowEditDays = false;
            this.tseExpectedDuration.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.tseExpectedDuration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tseExpectedDuration.Properties.Mask.EditMask = "HH:mm:ss";
            this.tseExpectedDuration.Size = new System.Drawing.Size(256, 20);
            this.tseExpectedDuration.StyleController = this.lytCtlDefinition;
            this.tseExpectedDuration.TabIndex = 15;
            // 
            // teUrlLink
            // 
            this.teUrlLink.Location = new System.Drawing.Point(136, 154);
            this.teUrlLink.Name = "teUrlLink";
            this.teUrlLink.Size = new System.Drawing.Size(256, 20);
            this.teUrlLink.StyleController = this.lytCtlDefinition;
            this.teUrlLink.TabIndex = 12;
            // 
            // tseWaitUntil
            // 
            this.tseWaitUntil.EditValue = System.TimeSpan.Parse("00:00:00");
            this.tseWaitUntil.Location = new System.Drawing.Point(136, 59);
            this.tseWaitUntil.Name = "tseWaitUntil";
            this.tseWaitUntil.Properties.AllowEditDays = false;
            this.tseWaitUntil.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tseWaitUntil.Properties.Mask.EditMask = "HH:mm:ss";
            this.tseWaitUntil.Size = new System.Drawing.Size(256, 20);
            this.tseWaitUntil.StyleController = this.lytCtlDefinition;
            this.tseWaitUntil.TabIndex = 11;
            // 
            // tseWaitDelay
            // 
            this.tseWaitDelay.EditValue = System.TimeSpan.Parse("00:00:00");
            this.tseWaitDelay.Location = new System.Drawing.Point(136, 83);
            this.tseWaitDelay.Name = "tseWaitDelay";
            this.tseWaitDelay.Properties.AllowEditDays = false;
            this.tseWaitDelay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tseWaitDelay.Properties.Mask.EditMask = "HH:mm:ss";
            this.tseWaitDelay.Size = new System.Drawing.Size(256, 20);
            this.tseWaitDelay.StyleController = this.lytCtlDefinition;
            this.tseWaitDelay.TabIndex = 10;
            // 
            // lueJobKind
            // 
            this.lueJobKind.Location = new System.Drawing.Point(136, 12);
            this.lueJobKind.Name = "lueJobKind";
            this.lueJobKind.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lueJobKind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueJobKind.Properties.NullText = "";
            this.lueJobKind.Size = new System.Drawing.Size(256, 20);
            this.lueJobKind.StyleController = this.lytCtlDefinition;
            this.lueJobKind.TabIndex = 8;
            // 
            // meDescription
            // 
            this.meDescription.Location = new System.Drawing.Point(136, 322);
            this.meDescription.Name = "meDescription";
            this.meDescription.Size = new System.Drawing.Size(256, 87);
            this.meDescription.StyleController = this.lytCtlDefinition;
            this.meDescription.TabIndex = 7;
            // 
            // txtProcessArguments
            // 
            this.txtProcessArguments.Location = new System.Drawing.Point(136, 202);
            this.txtProcessArguments.Name = "txtProcessArguments";
            this.txtProcessArguments.Size = new System.Drawing.Size(256, 20);
            this.txtProcessArguments.StyleController = this.lytCtlDefinition;
            this.txtProcessArguments.TabIndex = 5;
            // 
            // txtProcessPath
            // 
            this.txtProcessPath.Location = new System.Drawing.Point(136, 178);
            this.txtProcessPath.Name = "txtProcessPath";
            this.txtProcessPath.Size = new System.Drawing.Size(256, 20);
            this.txtProcessPath.StyleController = this.lytCtlDefinition;
            this.txtProcessPath.TabIndex = 4;
            // 
            // lytCtlDefinitionGroup
            // 
            this.lytCtlDefinitionGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lytCtlDefinitionGroup.GroupBordersVisible = false;
            this.lytCtlDefinitionGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.txtProcessPath_LayoutItem,
            this.txtProcessArguments_LayoutItem,
            this.tseExpectedDuration_LayoutItem,
            this.tseTimeOut_LayoutItem,
            this.teUrlLink_LayoutItem,
            this.chkConsiderAsSuccess_LayoutItem,
            this.tseWaitUntil_LayoutItem,
            this.chkResetTasksRecursively_LayoutItem,
            this.txtWarningCode_LayoutItem,
            this.tseStopAfter_LayoutItem,
            this.tseWaitDelay_LayoutItem,
            this.chkEnabled_LayoutItem,
            this.lueJobKind_LayoutItem,
            this.meDescription_LayoutItem,
            this.chkRequiredResources_LayoutItem});
            this.lytCtlDefinitionGroup.Location = new System.Drawing.Point(0, 0);
            this.lytCtlDefinitionGroup.Name = "Root";
            this.lytCtlDefinitionGroup.Size = new System.Drawing.Size(404, 421);
            this.lytCtlDefinitionGroup.TextVisible = false;
            // 
            // txtProcessPath_LayoutItem
            // 
            this.txtProcessPath_LayoutItem.Control = this.txtProcessPath;
            this.txtProcessPath_LayoutItem.Location = new System.Drawing.Point(0, 166);
            this.txtProcessPath_LayoutItem.Name = "txtProcessPath_LayoutItem";
            this.txtProcessPath_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.txtProcessPath_LayoutItem.Text = "Process Path  ";
            this.txtProcessPath_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // txtProcessArguments_LayoutItem
            // 
            this.txtProcessArguments_LayoutItem.Control = this.txtProcessArguments;
            this.txtProcessArguments_LayoutItem.Location = new System.Drawing.Point(0, 190);
            this.txtProcessArguments_LayoutItem.Name = "txtProcessArguments_LayoutItem";
            this.txtProcessArguments_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.txtProcessArguments_LayoutItem.Text = "Proc. Arguments";
            this.txtProcessArguments_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // tseExpectedDuration_LayoutItem
            // 
            this.tseExpectedDuration_LayoutItem.Control = this.tseExpectedDuration;
            this.tseExpectedDuration_LayoutItem.Location = new System.Drawing.Point(0, 238);
            this.tseExpectedDuration_LayoutItem.Name = "tseExpectedDuration_LayoutItem";
            this.tseExpectedDuration_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.tseExpectedDuration_LayoutItem.Text = "Expected duration           ";
            this.tseExpectedDuration_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // tseTimeOut_LayoutItem
            // 
            this.tseTimeOut_LayoutItem.Control = this.tseTimeOut;
            this.tseTimeOut_LayoutItem.Location = new System.Drawing.Point(0, 262);
            this.tseTimeOut_LayoutItem.Name = "tseTimeOut_LayoutItem";
            this.tseTimeOut_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.tseTimeOut_LayoutItem.Text = "TimeOut (in s.)";
            this.tseTimeOut_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // teUrlLink_LayoutItem
            // 
            this.teUrlLink_LayoutItem.Control = this.teUrlLink;
            this.teUrlLink_LayoutItem.Location = new System.Drawing.Point(0, 142);
            this.teUrlLink_LayoutItem.Name = "teUrlLink_LayoutItem";
            this.teUrlLink_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.teUrlLink_LayoutItem.Text = "Url Link";
            this.teUrlLink_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // chkConsiderAsSuccess_LayoutItem
            // 
            this.chkConsiderAsSuccess_LayoutItem.Control = this.chkConsiderAsSuccess;
            this.chkConsiderAsSuccess_LayoutItem.Location = new System.Drawing.Point(194, 24);
            this.chkConsiderAsSuccess_LayoutItem.Name = "chkConsiderAsSuccess_LayoutItem";
            this.chkConsiderAsSuccess_LayoutItem.Size = new System.Drawing.Size(190, 23);
            this.chkConsiderAsSuccess_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.chkConsiderAsSuccess_LayoutItem.TextVisible = false;
            // 
            // tseWaitUntil_LayoutItem
            // 
            this.tseWaitUntil_LayoutItem.Control = this.tseWaitUntil;
            this.tseWaitUntil_LayoutItem.Location = new System.Drawing.Point(0, 47);
            this.tseWaitUntil_LayoutItem.Name = "tseWaitUntil_LayoutItem";
            this.tseWaitUntil_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.tseWaitUntil_LayoutItem.Text = "Wait until";
            this.tseWaitUntil_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // chkResetTasksRecursively_LayoutItem
            // 
            this.chkResetTasksRecursively_LayoutItem.Control = this.chkResetTasksRecursively;
            this.chkResetTasksRecursively_LayoutItem.Location = new System.Drawing.Point(0, 119);
            this.chkResetTasksRecursively_LayoutItem.Name = "chkResetTasksRecursively_LayoutItem";
            this.chkResetTasksRecursively_LayoutItem.Size = new System.Drawing.Size(384, 23);
            this.chkResetTasksRecursively_LayoutItem.Text = "Reset task(s) recursively";
            this.chkResetTasksRecursively_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // txtWarningCode_LayoutItem
            // 
            this.txtWarningCode_LayoutItem.Control = this.txtWarningCode;
            this.txtWarningCode_LayoutItem.Location = new System.Drawing.Point(0, 286);
            this.txtWarningCode_LayoutItem.Name = "txtWarningCode_LayoutItem";
            this.txtWarningCode_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.txtWarningCode_LayoutItem.Text = "Warning Code";
            this.txtWarningCode_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // tseStopAfter_LayoutItem
            // 
            this.tseStopAfter_LayoutItem.Control = this.tseStopAfter;
            this.tseStopAfter_LayoutItem.Location = new System.Drawing.Point(0, 95);
            this.tseStopAfter_LayoutItem.Name = "tseStopAfter_LayoutItem";
            this.tseStopAfter_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.tseStopAfter_LayoutItem.Text = "Stop after";
            this.tseStopAfter_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // tseWaitDelay_LayoutItem
            // 
            this.tseWaitDelay_LayoutItem.Control = this.tseWaitDelay;
            this.tseWaitDelay_LayoutItem.Location = new System.Drawing.Point(0, 71);
            this.tseWaitDelay_LayoutItem.Name = "tseWaitDelay_LayoutItem";
            this.tseWaitDelay_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.tseWaitDelay_LayoutItem.Text = "Wait delay";
            this.tseWaitDelay_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // chkEnabled_LayoutItem
            // 
            this.chkEnabled_LayoutItem.Control = this.chkEnabled;
            this.chkEnabled_LayoutItem.Location = new System.Drawing.Point(0, 24);
            this.chkEnabled_LayoutItem.MaxSize = new System.Drawing.Size(194, 23);
            this.chkEnabled_LayoutItem.MinSize = new System.Drawing.Size(194, 23);
            this.chkEnabled_LayoutItem.Name = "chkEnabled_LayoutItem";
            this.chkEnabled_LayoutItem.Size = new System.Drawing.Size(194, 23);
            this.chkEnabled_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.chkEnabled_LayoutItem.Text = " ";
            this.chkEnabled_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // lueJobKind_LayoutItem
            // 
            this.lueJobKind_LayoutItem.Control = this.lueJobKind;
            this.lueJobKind_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.lueJobKind_LayoutItem.Name = "lueJobKind_LayoutItem";
            this.lueJobKind_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.lueJobKind_LayoutItem.Text = "Job Kind";
            this.lueJobKind_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // meDescription_LayoutItem
            // 
            this.meDescription_LayoutItem.Control = this.meDescription;
            this.meDescription_LayoutItem.Location = new System.Drawing.Point(0, 310);
            this.meDescription_LayoutItem.Name = "meDescription_LayoutItem";
            this.meDescription_LayoutItem.Size = new System.Drawing.Size(384, 91);
            this.meDescription_LayoutItem.Text = "Description";
            this.meDescription_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // chkRequiredResources
            // 
            this.chkRequiredResources.Location = new System.Drawing.Point(136, 226);
            this.chkRequiredResources.MaxSelectedItems = ((uint)(0u));
            this.chkRequiredResources.Name = "chkRequiredResources";
            this.chkRequiredResources.Size = new System.Drawing.Size(256, 20);
            this.chkRequiredResources.TabIndex = 22;
            // 
            // chkRequiredResources_LayoutItem
            // 
            this.chkRequiredResources_LayoutItem.Control = this.chkRequiredResources;
            this.chkRequiredResources_LayoutItem.Location = new System.Drawing.Point(0, 214);
            this.chkRequiredResources_LayoutItem.Name = "chkRequiredResources_LayoutItem";
            this.chkRequiredResources_LayoutItem.Size = new System.Drawing.Size(384, 24);
            this.chkRequiredResources_LayoutItem.Text = "Required Resources";
            this.chkRequiredResources_LayoutItem.TextSize = new System.Drawing.Size(121, 13);
            // 
            // JobDefinitionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytCtlDefinition);
            this.Name = "JobDefinitionView";
            this.Size = new System.Drawing.Size(404, 421);
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlDefinition)).EndInit();
            this.lytCtlDefinition.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tseStopAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarningCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkResetTasksRecursively.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsiderAsSuccess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseTimeOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseExpectedDuration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUrlLink.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitUntil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitDelay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueJobKind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessArguments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlDefinitionGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessPath_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcessArguments_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseExpectedDuration_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseTimeOut_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teUrlLink_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsiderAsSuccess_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitUntil_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkResetTasksRecursively_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarningCode_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseStopAfter_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseWaitDelay_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnabled_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueJobKind_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDescription_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRequiredResources_LayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl lytCtlDefinition;
        private DevExpress.XtraEditors.TextEdit txtProcessArguments;
        private DevExpress.XtraEditors.TextEdit txtProcessPath;
        private DevExpress.XtraLayout.LayoutControlGroup lytCtlDefinitionGroup;
        private DevExpress.XtraLayout.LayoutControlItem txtProcessPath_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem txtProcessArguments_LayoutItem;
        private DevExpress.XtraEditors.MemoEdit meDescription;
        private DevExpress.XtraLayout.LayoutControlItem meDescription_LayoutItem;
        private DevExpress.XtraEditors.TimeSpanEdit tseWaitUntil;
        private DevExpress.XtraEditors.TimeSpanEdit tseWaitDelay;
        private DevExpress.XtraEditors.LookUpEdit lueJobKind;
        private DevExpress.XtraLayout.LayoutControlItem lueJobKind_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem tseWaitDelay_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem tseWaitUntil_LayoutItem;
        private DevExpress.XtraEditors.TextEdit teUrlLink;
        private DevExpress.XtraLayout.LayoutControlItem teUrlLink_LayoutItem;
        private DevExpress.XtraEditors.TimeSpanEdit tseExpectedDuration;
        private DevExpress.XtraLayout.LayoutControlItem tseExpectedDuration_LayoutItem;
        private DevExpress.XtraEditors.TimeSpanEdit tseTimeOut;
        private DevExpress.XtraLayout.LayoutControlItem tseTimeOut_LayoutItem;
        private DevExpress.XtraEditors.CheckEdit chkConsiderAsSuccess;
        private DevExpress.XtraEditors.CheckEdit chkEnabled;
        private DevExpress.XtraLayout.LayoutControlItem chkEnabled_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem chkConsiderAsSuccess_LayoutItem;
        private System.Windows.Forms.ToolTip addToolTipPropertyOnAllControl;
        private DevExpress.XtraEditors.CheckEdit chkResetTasksRecursively;
        private DevExpress.XtraLayout.LayoutControlItem chkResetTasksRecursively_LayoutItem;
        private DevExpress.XtraEditors.TextEdit txtWarningCode;
        private DevExpress.XtraLayout.LayoutControlItem txtWarningCode_LayoutItem;
        private DevExpress.XtraEditors.TimeSpanEdit tseStopAfter;
        private DevExpress.XtraLayout.LayoutControlItem tseStopAfter_LayoutItem;
        private TechnicalTools.UI.DX.Controls.CheckedTreeListLookUpEdit chkRequiredResources;
        private DevExpress.XtraLayout.LayoutControlItem chkRequiredResources_LayoutItem;
    }
}
