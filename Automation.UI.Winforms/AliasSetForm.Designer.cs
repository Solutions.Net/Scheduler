﻿namespace Automation.UI.Winforms
{
    partial class AliasSetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcAliases = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvAliases = new TechnicalTools.UI.DX.EnhancedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcAliases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAliases)).BeginInit();
            this.SuspendLayout();
            // 
            // gcAliases
            // 
            this.gcAliases.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcAliases.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcAliases.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcAliases.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcAliases.EmbeddedNavigator.TextStringFormat = "Alias {0} of {1} (visible) of {2} (total)";
            this.gcAliases.Location = new System.Drawing.Point(0, 0);
            this.gcAliases.MainView = this.gvAliases;
            this.gcAliases.Name = "gcAliases";
            this.gcAliases.Size = new System.Drawing.Size(757, 200);
            this.gcAliases.TabIndex = 0;
            this.gcAliases.UseEmbeddedNavigator = true;
            this.gcAliases.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAliases});
            // 
            // gvAliases
            // 
            this.gvAliases.GridControl = this.gcAliases;
            this.gvAliases.Name = "gvAliases";
            // 
            // AliasSetForm
            // 
            this.ClientSize = new System.Drawing.Size(757, 262);
            this.Controls.Add(this.gcAliases);
            this.Name = "AliasSetForm";
            this.Load += new System.EventHandler(this.AliasSetForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcAliases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAliases)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TechnicalTools.UI.DX.EnhancedGridControl gcAliases;
        private TechnicalTools.UI.DX.EnhancedGridView gvAliases;
    }
}