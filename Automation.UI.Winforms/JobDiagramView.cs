﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using System.Reflection;
using System.Threading;

using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;

using TechnicalTools;
using TechnicalTools.UI;
using TechnicalTools.Model;
using TechnicalTools.Algorithm.Geometry2D;
using TechnicalTools.Tools;
using TechnicalTools.UI.Controls;
using TechnicalTools.UI.DX;

using Automation.Model;

using Debug = System.Diagnostics.Debug;


namespace Automation.UI.Winforms
{
    // A lire : https://www.codeproject.com/Articles/12870/Don-t-Flicker-Double-Buffer
    // Au cas ou : https://psycodedeveloper.wordpress.com/2013/08/15/improving-performance-in-windows-forms-by-temporarily-suspending-redrawing-a-window/
    [DesignTimeVisible(true)]
    [ToolboxItem(true)]
    //[DesignerCategory("TechnicalTools.UI.DX.Automation")]
    public partial class JobDiagramView : XtraUserControlBase, IViewLockable
    {
        // TODO : Migrer Scheduler dans JobDiagram
        public DailyJobScheduler  Scheduler    { get { return _Scheduler; } set { SetScheduler(value); } } DailyJobScheduler _Scheduler;
        public JobDiagram         Diagram      { get { return _Diagram;   } set { SetDiagram(value);   } } JobDiagram _Diagram;

        public               bool ReadOnly     { get { return _ReadOnly; } set { _ReadOnly = value; DefineEnabilitiesAndVisibilities(); } } bool _ReadOnly;
        public               bool IsReadOnly   { get { return ReadOnly || _Diagram == null || _Diagram.InstanciatedFrom != null; } }

        public               bool ShowEditMenu { get; set; }
        public event EventHandler EditMenuClick;


        public IReadOnlyDictionary<Job, JobView>                     JobViews      { get { return _jobViews; } }
        public IReadOnlyDictionary<JobDependency, JobDependencyView> JobDepViews   { get { return _jobDepViews; } }
        public IList<ISelectableView>                                SelectedViews { get { return _selectedViews; } }

        readonly Dictionary<Job, JobView>                     _jobViews = new Dictionary<Job, JobView>();
        readonly Dictionary<JobDependency, JobDependencyView> _jobDepViews = new Dictionary<JobDependency, JobDependencyView>();
        readonly FixedBindingList<ISelectableView>            _selectedViews = new FixedBindingList<ISelectableView>();

        StateIndicator.eState SchedulerState
        {
            get
            {
                return Scheduler       == null                      ? StateIndicator.eState.Gray
                     : Scheduler.State == ThreadState.Running       ? StateIndicator.eState.Blue
                     : Scheduler.State == ThreadState.StopRequested ? StateIndicator.eState.Yellow
                     : Scheduler.State == ThreadState.Stopped       ? StateIndicator.eState.RedOrange 
                     : throw new Exception("Not Handled!");
            }
        }

        public JobDiagramView()
        {
            InitializeComponent();
            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            Disposed += (_, __) => tmrRefreshJobExecutionInfo.Dispose();
            if (DesignTimeHelper.IsInDesignMode)
                return;

            _refreshUI = new PostponingExecuter(RefreshUIVisibilitiesRegardingSelection);
            _selectedViews.ListChanged += (_, __) => _refreshUI.SchedulePostponedExecution();

            splitContainerControl1.SplitterPosition = _oldPropertiesPanelWidth;

            txtDailyScheduleHistoryLength.MaskBox.ShortcutsEnabled = true; // Disable shortcut (for example press right click on control, then release it elsewhere => make the menu appear without this line of code)

            Load += AutomatedJobGroupView_Load;
            TopFormOrPanelClosing += JobDiagramView_TopFormOrPanelClosing;

            var mSetStyle = typeof(XtraScrollableControl).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
            //mSetStyle.Invoke(canvas, new object[] { ControlStyles.UserPaint, true }); 
            //mSetStyle.Invoke(canvas, new object[] { ControlStyles.AllPaintingInWmPaint, true });
            //mSetStyle.Invoke(canvas, new object[] { ControlStyles.OptimizedDoubleBuffer, true });

            ViewHelper.CreateHandleSafely(() => CreateHandle()); // before an yevent subscribing

            Text = "Automated Job Group Definition";
            txtFocusStealer.SendToBack();
            txtFocusStealer.SetBounds(btnSave.Location.X, btnSave.Location.Y, 10, 10);
        }

        private void JobDiagramView_TopFormOrPanelClosing(XtraUserControlBase sender, TopFormOrPanelCancelEventArgs e)
        {
            if (Scheduler?.State == ThreadState.Running)
            {
                XtraMessageBox.Show(this, "Please stop the scheduler before closing this view!");
                e.Cancel = true;
            }
            else if (Scheduler?.State == ThreadState.StopRequested)
            {
                XtraMessageBox.Show(this, "Please wait for global scheduler to stop!");
                e.Cancel = true;
            }
        }

        readonly PostponingExecuter _refreshUI;

        public XElement SaveLayout(XElement nLayout = null)
        {
            nLayout = nLayout ?? new XElement("JobDiagramView");

            var nJobViews = new XElement("JobViews");
            foreach (var jobView in JobViews.OrderBy(kvp => kvp.Key.JobName))
            {
                var nJobView = new XElement("JobView", new XAttribute("Job", jobView.Key.JobName));
                jobView.Value.SaveLayout(nJobView);
                nJobViews.Add(nJobView);
            }
            nLayout.Add(nJobViews);

            //var nJobDepViews = new XElement("JobDepViews");
            //foreach (var jobDepView in JobDepViews)
            //{
            //    var nJobDepView = new XElement("JobDepView", ...);
            //    nJobDepViews.Add(nJobDepView);
            //}
            //nLayout.Add(nJobDepViews);

            return nLayout;
        }
        public void RestoreLayout(XElement nLayouts)
        {
            var nJobDiagramView = nLayouts.Element("JobDiagramView") ?? (nLayouts.Name == "JobDiagramView" ? nLayouts : null);
            if (nJobDiagramView == null)
                return;

            var nJobViews = nJobDiagramView.Element("JobViews");
            foreach (var nJobView in nJobViews.Elements("JobView"))
            {
                var jobName = nJobView.Attribute("Job").Value;
                var job = Diagram.GetJobs().FirstOrDefault(j => j.JobName == jobName);
                if (job != null)
                {
                    var view = _jobViews[job];
                    view.RestoreLayout(nJobView);
                }
            }
        }

        public string GetDiagramErrors()
        {
            if (string.IsNullOrWhiteSpace(Diagram.Name))
                return "Name must be filled";
            return null;
        }

        private void AutomatedJobGroupView_Load(object sender, EventArgs e)
        {
            SuspendLayout();
            RefreshUIVisibilitiesRegardingSelection();
            ResumeLayout();

            DoubleBuffered = true;
            // set instance non-public property with name "DoubleBuffered" to true
            CanvasDoubleBuffered = true;
            canvas.Invalidate();

            Disposed += JobDiagramView_Disposed;
        }
        // Empeche le clignottement  des fleches
        // Mais  en contrepartie quand les JobView sont deplacé, ils laissent une trainée...
        bool CanvasDoubleBuffered
        {
            get { return (bool)_p.GetValue(canvas, null); }
            set { _p.SetValue(canvas, value); }
        }
        static readonly PropertyInfo _p = typeof(Control).GetProperty("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic);

        private void JobDiagramView_Disposed(object sender, EventArgs e)
        {
            UnbindModelEvents();
            // Dispose all ui element in reverse order of creation
            foreach (var jobDepView in _jobDepViews.Values)
                jobDepView.Dispose();
            _jobDepViews.Clear();
            foreach (var jobView in _jobViews.Values)
                jobView.Dispose();
            _jobViews.Clear();
        }

        public void DefineEnabilitiesAndVisibilities()
        {
            var hasScheduler = Scheduler != null;
            stateIndicator_LayoutItem.Visibility = hasScheduler ? LayoutVisibility.Always : LayoutVisibility.Never;
            btnLoad_LayoutItem.Visibility = !hasScheduler ? LayoutVisibility.Always : LayoutVisibility.Never;
            emptySpaceItem4.Visibility = !hasScheduler ? LayoutVisibility.Always : LayoutVisibility.Never;
            btnSave_LayoutItem.Visibility = !hasScheduler ? LayoutVisibility.Always : LayoutVisibility.Never;

            tseInstanciationTime.Enabled = Diagram != null;
            txtDiagramName.Enabled = Diagram != null;

            txtDiagramName.ReadOnly = IsReadOnly;
            tseInstanciationTime.ReadOnly = IsReadOnly;
            txtLogPath.ReadOnly = IsReadOnly;
            txtDailyScheduleHistoryLength.ReadOnly = IsReadOnly;
            btnEditProcessShortcuts.Enabled = !IsReadOnly;
            btnEditSharedResources.Enabled = !IsReadOnly;

            btnLoad.Enabled = true;
            btnSave.Enabled = Diagram != null && !IsReadOnly && string.IsNullOrWhiteSpace(GetDiagramErrors());

            tmrRefreshJobExecutionInfo.Enabled = _Scheduler != null;

            if (Scheduler == null && !_userWantKeepExecutionPanelCollapsed)
                lytGrpExecutions.Expanded = false;
        }

        void SetScheduler(DailyJobScheduler scheduler)
        {
            if (_Scheduler != null)
                throw new Exception("Does not support changing scheduler after first set. Create a new control.");
            _Scheduler = scheduler;
            if (_Scheduler != null)
                SetDiagram(scheduler.DiagramInstance);
            stateIndicator.State = SchedulerState;
            RefreshEnabilitiesAndVisibilities();
        }
        void SetDiagram(JobDiagram newValue)
        {
            if (_Diagram != null)
                throw new Exception("Does not support changing diagram after first set. Create a new control.");
            if (_Diagram != null)
                UnbindModelEvents();
            bool firstLoad = _Diagram == null;
            _Diagram = newValue;
            this.SuspendDrawing();
            this.SuspendLayout();
            if (Diagram != null)
                BindModelEvents();
            DefineEnabilitiesAndVisibilities();
            if (firstLoad && _Diagram != null)
                Reload();
            this.ResumeLayout();
            this.ResumeDrawing();
        }
        void BindModelEvents()
        {
            UnbindModelEvents();
            SuspendLayout();
            var jobs = Diagram.GetJobsAndSubscribe(Jobs_ListChanged);
            ResetJobs(jobs);
            var jobDependencies = Diagram.GetJobDependenciesAndSubscribe(JobDependencies_ListChanged);
            ResetJobDependencies(jobDependencies);
            ResumeLayout();
        }
        void UnbindModelEvents()
        {
            Diagram.UnsubscribeFromJobs(Jobs_ListChanged);
            Diagram.UnsubscribeFromJobDependencies(JobDependencies_ListChanged);
            ResetJobDependencies(EmptyJobDependencyList); // must be before jobs
            ResetJobs(EmptyJobList);
        }
        static readonly List<Job> EmptyJobList = new List<Job>();
        static readonly List<JobDependency> EmptyJobDependencyList = new List<JobDependency>();

        public void Reload()
        {
            txtDiagramName.Text = Diagram.Name;
            tseInstanciationTime.EditValue = Diagram.DailyTimeToInstanciate;
            txtLogPath.Text = Diagram.LogPath;
            txtDailyScheduleHistoryLength.Text = Diagram.DailyScheduleHistoryLength.ToString();
            jobExecutionsView.Diagram = Diagram;
        }


        private void stateIndicator_MouseUp(object sender, MouseEventArgs e)
        {
            mnuStart.Enabled = Scheduler?.State.In(ThreadState.StopRequested, ThreadState.Stopped) ?? false;
            mnuStopAfterCurrentExecutingJobs.Enabled = Scheduler?.State == ThreadState.Running;
            mnuStateIndicator.Show(this, e.Location);
        }
        private void mnuStart_Click(object sender, EventArgs e)
        {
            Scheduler.Run();
        }
        private void mnuStopAfterCurrentExecutingJobs_Click(object sender, EventArgs e)
        {
            Scheduler.StopStartingNewJob();
        }


        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (Diagram != null)
                ViewHelper.ShowBusyWhileDoingUIWork(this, "Loading", () => { RestoreLayout(Diagram.Load()); Reload(); });
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Diagram != null)
                ViewHelper.ShowBusyWhileDoingUIWork(this, "Saving", () =>
                {
                    var dt = DateTime.UtcNow;
                    Diagram.Save(SaveLayout());
                    // wait at least one second to provide a visual feedback to user
                    System.Threading.Thread.Sleep(Math.Max(0, 1000 - (int)(DateTime.UtcNow - dt).TotalMilliseconds));
                });
        }
        private void txtDiagramName_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                Diagram.Name = txtDiagramName.Text.Trim();
                addToolTipPropertyOnAllControl.Hide(txtDiagramName);
            }
            catch (Exception ex)
            {
                addToolTipPropertyOnAllControl.Show(ex.Message, txtDiagramName, 0, txtDiagramName.Height);
                e.Cancel = true;
            }
            DefineEnabilitiesAndVisibilities();
        }

        private void tseInstanciationTime_EditValueChanged(object sender, EventArgs e)
        {
            Diagram.DailyTimeToInstanciate = tseInstanciationTime.TimeSpan;
        }
        private void txtLogPath_EditValueChanged(object sender, EventArgs e)
        {
            Diagram.LogPath = txtLogPath.Text.Trim();
        }
        private void txtAutoCloseDays_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) &&
                (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9))
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
        private void txtAutoCloseDays_EditValueChanged(object sender, EventArgs e)
        {
            Diagram.DailyScheduleHistoryLength = string.IsNullOrWhiteSpace(txtDailyScheduleHistoryLength.Text) ? (int?)null : int.Parse(txtDailyScheduleHistoryLength.Text.Trim());
        }

        private void Jobs_ListChanged(object sender, IAfterChangeEventArgs<Job> e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => Jobs_ListChanged(sender, e)));
                return;
            }

            if (e.ListChangedType == ListChangedType.ItemAdded)
                OnDiagramJobAdded(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
                OnDiagramJobRemoved(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.Reset)
                ResetJobs(Diagram.GetJobs());
        }
        void ResetJobs(IReadOnlyCollection<Job> jobs)
        {
            foreach (var removedJob in _jobViews.Keys.Except(jobs).ToList())
                OnDiagramJobRemoved(removedJob);
            foreach (var addedJob in jobs.Except(_jobViews.Keys).ToList())
                OnDiagramJobAdded(addedJob);
        }
        void OnDiagramJobAdded(Job job)
        {
            var view = new JobView(job, this);
            view.Location = new Point((canvas.Width - view.Size.Width) / 2, (canvas.Height - view.Size.Height) / 2);
            view.LocationChanged += JobView_LocationChanged;
            view.SelectionChanging += JobView_SelectionChanging; // Use "Changing" and not "Changed" because it allows us to solve a focus problem https://bytes.com/topic/access/answers/652934-another-setfocus-question-setfocus-subform
            view.DependencyBegin += View_DependencyBegin;
            view.DependencyTargetMouseMove += View_DependencyTargetMouseMove;
            view.DependencyEnd += View_DependencyEnd;
            view.TitleDoubleClick += View_TitleDoubleClick;

            _jobViews.Add(job, view);
            view.Parent = canvas;
            view.Visible = true;
            view.BringToFront();
        }
        void OnDiagramJobRemoved(Job job)
        {
            var view = _jobViews[job];
            view.LocationChanged -= JobView_LocationChanged;
            view.SelectionChanging -= JobView_SelectionChanging;
            view.DependencyBegin -= View_DependencyBegin;
            view.DependencyTargetMouseMove -= View_DependencyTargetMouseMove;
            view.DependencyEnd -= View_DependencyEnd;
            view.TitleDoubleClick -= View_TitleDoubleClick;

            view.Parent = null;
            view.Dispose();
            _jobViews.Remove(job);
            _selectedViews.Remove(view);
        }

        private void JobDependencies_ListChanged(object sender, IAfterChangeEventArgs<JobDependency> e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => JobDependencies_ListChanged(sender, e)));
                return;
            }
            if (e.ListChangedType == ListChangedType.ItemAdded)
                OnDiagramJobDependencyAdded(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
                OnDiagramJobDependencyRemoved(e.ItemAddedOrRemoved);
            else if (e.ListChangedType == ListChangedType.Reset)
                ResetJobDependencies(Diagram.GetJobDependencies());
        }
        void ResetJobDependencies(IReadOnlyCollection<JobDependency> deps)
        {
            foreach (var removedJobDep in _jobDepViews.Keys.Except(deps).ToList())
                OnDiagramJobDependencyRemoved(removedJobDep);
            foreach (var addedJobDep in deps.Except(_jobDepViews.Keys).ToList())
                OnDiagramJobDependencyAdded(addedJobDep);
        }
        void OnDiagramJobDependencyAdded(JobDependency jobDep)
        {
            var view = new JobDependencyView(jobDep, this);
            view.SelectionChanged += JobDepView_SelectionChanged;
            view.Invalidated += JobDepView_Invalidated;
            _jobDepViews.Add(jobDep, view);
            canvas.Invalidate();
        }
        void OnDiagramJobDependencyRemoved(JobDependency jobDep)
        {
            var view = _jobDepViews[jobDep];
            view.SelectionChanged -= JobDepView_SelectionChanged;
            view.Invalidated -= JobDepView_Invalidated;

            view.Dispose();
            _jobDepViews.Remove(jobDep);
            _selectedViews.Remove(view);
            canvas.Invalidate();
        }

        private void JobView_LocationChanged(object sender, EventArgs e)
        {
            var jobView = sender as JobView;

            if (jobView.Location.X < 0 || jobView.Location.Y < 0)
            {
                if (!jobView.IsSelected || SelectedViews.OfType<JobView>().All(v => !v.IsMoving)) // user is not dragging the job, he is using the srollbar
                {
                    //canvas.Invalidate();
                    //Invalidate();
                    return;
                }


                canvas.SuspendDrawing();
                var move = new Point(Math.Max(0, -jobView.Location.X), Math.Max(0, -jobView.Location.Y));
                foreach (var view in _jobViews.Values)
                    view.Location = view.Location.Add(move);
                canvas.ResumeDrawing();
            }

            if (jobView.IsSelected && // Le deplacement d'une vue appartenant a un groupe d'element selectionné, fait deplacer l'ensemble
                !_movingSelection) // stop recursion
            {
                _movingSelection = true;
                var move = new Point(jobView.Left - jobView.PreviousLocation.X, jobView.Top - jobView.PreviousLocation.Y);
                foreach (var view in _selectedViews.OfType<JobView>().ToList())
                    if (view != jobView)
                        view.Location = view.Location.Add(move);

                _movingSelection = false;
            }
        }
        bool _movingSelection;

        private void JobView_SelectionChanging(JobView selectedView, bool willBeSelected)
        {
            //var selectedView = sender as JobView;
            if (!willBeSelected)
                _selectedViews.Remove(selectedView);
            else
            {
                if (!System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) &&
                    !System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl))
                {
                    foreach (var view in _selectedViews.ToList())
                        view.IsSelected = false;
                }
                _selectedViews.Add(selectedView);
            }
        }
        private void JobDepView_SelectionChanged(object sender, EventArgs e)
        {
            var selectedView = sender as JobDependencyView;
            if (!selectedView.IsSelected)
                _selectedViews.Remove(selectedView);
            else
            {
                if (!System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) &&
                    !System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl))
                {
                    foreach (var view in _selectedViews.ToList())
                        view.IsSelected = false;
                }
                _selectedViews.Add(selectedView);
            }
            canvas.Invalidate();
        }
        private void JobDepView_Invalidated(object sender, EventArgs e)
        {
            canvas.Invalidate();
        }
        void RefreshUIVisibilitiesRegardingSelection()
        {
            if (_selectedViews.Count != 1)
            {
                if (!_userWantKeepPropertiesPanelCollapsed)
                    _oldPropertiesPanelWidth = splitContainerControl1.SplitterPosition;
                splitContainerControl1.IsSplitterFixed = true;
                splitContainerControl1.SplitterPosition = 0;

                jobDependencyEditView.Unbind();
                jobDefinitionView.Unbind();
                mustRestoreSelection = true;
            }
            else
            {
                if (_selectedViews[0] is JobView)
                {
                    var jobView = _selectedViews[0] as JobView;
                    jobDefinitionView.Bind(jobView.Job, this);
                    lytGrpElementEditor.Text = "Properties of: " + jobView.Job.JobName;
                    jobDependencyDefinitionView_LayoutItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    jobDefinitionView_LayoutItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
                else if (_selectedViews[0] is JobDependencyView)
                {
                    var jobDependencyView = _selectedViews[0] as JobDependencyView;
                    jobDependencyEditView.Bind(jobDependencyView.JobDependency, this);
                    jobDependencyDefinitionView_LayoutItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    jobDefinitionView_LayoutItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
                else
                    Debug.Assert(false, "no other cases!");

                if (mustRestoreSelection)
                    splitContainerControl1.SplitterPosition = _userWantKeepPropertiesPanelCollapsed ? 20 : _oldPropertiesPanelWidth;
                splitContainerControl1.IsSplitterFixed = _userWantKeepPropertiesPanelCollapsed;
                mustRestoreSelection = false;
            }
            if (Scheduler != null)
            {
                var jobs = _selectedViews.OfType<JobView>().Select(jv => jv.Job).ToList();
                jobExecutionsView.Filter = jobs;
            }
            RemoveFocus();
        }
        int _oldPropertiesPanelWidth = 380;
        bool _userWantKeepPropertiesPanelCollapsed;
        bool mustRestoreSelection = true;

        private void layoutControl1_GroupExpandChanging(object sender, DevExpress.XtraLayout.Utils.LayoutGroupCancelEventArgs e)
        {
            e.Cancel = true; // Cancel event related to latoutcontrol because we handle meaning of "collapse" ourselve

            if (!_userWantKeepPropertiesPanelCollapsed) // User want to collapse
            {
                e.Group.TextLocation = DevExpress.Utils.Locations.Left;
                e.Group.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
                lytGrpElementEditor.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Normal;
                _userWantKeepPropertiesPanelCollapsed = true;
                splitContainerControl1.IsSplitterFixed = true;
                splitContainerControl1.SplitterPosition = 20;
            }
            else  // User want to collapse
            {
                e.Group.TextLocation = DevExpress.Utils.Locations.Top;
                e.Group.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.BeforeText;
                lytGrpElementEditor.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
                _userWantKeepPropertiesPanelCollapsed = false;
                splitContainerControl1.IsSplitterFixed = false;
                splitContainerControl1.SplitterPosition = _oldPropertiesPanelWidth;
            }
        }

        private void layoutControl3_GroupExpandChanging(object sender, DevExpress.XtraLayout.Utils.LayoutGroupCancelEventArgs e)
        {
            e.Cancel = true; // Cancel event related to latoutcontrol because we handle meaning of "collap
            if (!_userWantKeepExecutionPanelCollapsed) // User want to collapse
            {
                lytGrpExecutions.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Normal;
                _userWantKeepExecutionPanelCollapsed = true;
                splitContainerControl2.IsSplitterFixed = true;
                splitContainerControl2.SplitterPosition = 20;
            }
            else  // User want to collapse
            {
                _userWantKeepExecutionPanelCollapsed = false;
                lytGrpExecutions.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
                splitContainerControl2.IsSplitterFixed = false;
                splitContainerControl2.SplitterPosition = _oldExecutionPanelWidth;
            }
        }
        int _oldExecutionPanelWidth = 450;
        bool _userWantKeepExecutionPanelCollapsed;


        protected internal void RemoveFocus()
        {
            // var ac = ActiveControl;
            // var f = Focused;
            // var acf = ac.Focused;
            // var ctl = this.FindFocusedControl();
            // if (ctl is TextBoxMaskBox)
            //     ctl = ctl.Parent;
            // var lc = ctl as DevExpress.XtraLayout.LayoutControl;
            // var acff = lc.FindFocusedControl();
            //// lc.Items
            // if (Focused) // Le layout pique le Foucsed = true et donc le Focused de ce test est false .. bizarre
            // {            
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (txtFocusStealer.CanFocus) // ie: must be Visible and Enabled
            {
                txtFocusStealer.Focus();
                Debug.Assert(txtFocusStealer.Focused);
            }
            //}
        }
        private void txtFocusStealer_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && _selectedViews.Count > 0)
            {
                if (IsReadOnly)
                    return;
                var answer = XtraMessageBox.Show(this, "Delete all these elements?", "Please confirm", MessageBoxButtons.OKCancel);
                if (answer == DialogResult.Cancel)
                    return;
                var views = _selectedViews.ToList();
                foreach (var view in views)
                    view.IsSelected = false;

                foreach (var view in views)
                {
                    // Note : we can remove in any order we want, Diagram class handle all
                    var job = (view as JobView)?.Job;
                    if (job != null)
                    {
                        Diagram.RemoveJob(job);
                        continue;
                    }
                    var dependency = (view as JobDependencyView)?.JobDependency;
                    if (dependency != null)
                    {
                        Diagram.RemoveJobDependency(dependency);
                        continue;
                    }
                    Debug.Assert(false, "Not handled !");
                }
            }
            else if (e.Modifiers.HasFlag(Keys.Control) && e.KeyCode == Keys.S)
            {
                btnSave.PerformClick();
            }
        }

        #region Building Dependency
        JobDependencyView _dependencyBuildingView;
        private void View_DependencyBegin(object sender, EventArgs e)
        {
            if (IsReadOnly)
                return;
            var jobSource = sender as JobView;
            _dependencyBuildingView = new JobDependencyView(new JobDependency(Diagram, jobSource.Job, null) { SourceJobState = eSourceJobState.Success }, this);
        }
        private void View_DependencyTargetMouseMove(object sender, MouseEventArgs e)
        {
            if (IsReadOnly)
                return;
            UpdateDependencyBuildingDrawAndReturnTarget(sender as JobView, e);
        }
        JobView UpdateDependencyBuildingDrawAndReturnTarget(JobView jobViewSource, MouseEventArgs e)
        {
            // this test is true when Messagebox appear in View_DependencyEnd
            if (_dependencyBuildingView == null)
                return null;
            var p = canvas.PointToClient(jobViewSource.PointToScreen(e.Location));
            var nearestJobViewfromMouse = _jobViews.Values.Where(jv => jv != jobViewSource && jv.Bounds.Contains(p))
                                                    .OrderBy(jv => jv.Center.SquareDistance(p))
                                                    .FirstOrDefault();
            _dependencyBuildingView.To = nearestJobViewfromMouse == null
                                       ? p
                                       : nearestJobViewfromMouse.Center;
            canvas.Invalidate();
            return nearestJobViewfromMouse;
        }
        private void View_DependencyEnd(object sender, MouseEventArgs e)
        {
            if (IsReadOnly)
                return;

            var aimedJobViewTarget = UpdateDependencyBuildingDrawAndReturnTarget(sender as JobView, e);
            if (aimedJobViewTarget != null)
            {
                var jobDep = new JobDependency(Diagram, _dependencyBuildingView.JobDependency.Source, aimedJobViewTarget.Job)
                { SourceJobState = _dependencyBuildingView.JobDependency.SourceJobState };
                var jobDepExisting = Diagram.JobDependenciesBySourceJob.TryGetValueClass(jobDep.Source)?.FirstOrDefault(jd => jd.Target == jobDep.Target);
                if (jobDepExisting != null)
                {
                    XtraMessageBox.Show("This dependency already exist!");
                    _jobDepViews[jobDepExisting].IsSelected = true;
                }
                // TODO : Faire en sorte que ShowBusyWhileDoingUIWorkInPlace postpone un message a l'utilisateur !
                else
                {
                    if (ViewHelper.ShowBusyWhileDoingUIWork(this, "Adding dependency", () => Diagram.AddJobDependency(jobDep)))
                        _jobDepViews[jobDep].IsSelected = true;
                    else
                        canvas.Invalidate();
                }
            }
            _dependencyBuildingView = null;
        }

        private void View_TitleDoubleClick(object sender, EventArgs e)
        {
            if (Scheduler != null)
            {
                var view = sender as JobView;
                var execution = view.Job.GetExecutionHistory(true).LastOrDefault();
                if (execution != null)
                    JobExecutionView.ShowInForm(execution, this);
            }
        }


        #endregion Building Dependency

        void canvas_Paint(object sender, PaintEventArgs e)
        {
            if (Diagram == null)
                return;
            foreach (var depDef in Diagram.GetJobDependencies())
                if (depDef.Source.JobKind != eJobKind.Restart)
                    _jobDepViews[depDef].Draw(e.Graphics);
            foreach (var depDef in Diagram.GetJobDependencies())
                if (depDef.Source.JobKind == eJobKind.Restart)
                    _jobDepViews[depDef].Draw(e.Graphics);
            if (_dependencyBuildingView != null)
                _dependencyBuildingView.Draw(e.Graphics);
        }

        private void canvas_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDownLocation = e.Location;
            _hasMoved = false;
            CanvasDoubleBuffered = false;
        }
        Point? mouseDownLocation;
        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            
            if (mouseDownLocation.HasValue && e.Button.HasFlag(MouseButtons.Right))
            {
                var move = e.Location.Substract(mouseDownLocation.Value);
                if (_hasMoved || Math.Abs(move.X) >= 3 || Math.Abs(move.Y) >= 3)
                {
                    if (!_hasMoved)
                    {
                        //CanvasDoubleBuffered = false;
                    }
                    // _movingSelection = true;
                    //  canvas.SuspendLayout();
                    //  canvas.SuspendDrawing();
                   

                    foreach (var view in _jobViews.Values)
                        view.Location = view.Location.Add(move);
                    //move.Offset(canvas.AutoScrollPosition.X, canvas.AutoScrollPosition.Y);
                    //canvas.AutoScrollPosition = move;
                    //canvas.AutoScrollMinSize = new Size(-10000, -10000);
                    mouseDownLocation = e.Location;
                    // canvas.ResumeDrawing();
                  //   canvas.ResumeLayout();
                    _hasMoved = true;
                    _movingSelection = false;
                }
            }
        }
        bool _hasMoved;
        private void canvas_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDownLocation = null;
            CanvasDoubleBuffered = true;
            if (_hasMoved)
            {
                //CanvasDoubleBuffered = true;
                return;
            }
            if (e.Button == MouseButtons.Right) // Show Context menu to create new job
            {
                mnuNewJob.Visible = Scheduler == null;
                mnuNewJob.Enabled = !IsReadOnly;
                mnuEditSchedule.Visible = ShowEditMenu;
                mnuEditSchedule.Enabled = IsReadOnly;
                mnuLineUnderCreateNewJob.Visible = mnuNewJob.Visible;
                mnuEnableAllTasks.Visible = Scheduler == null;
                mnuEnableAllTasks.Enabled = !IsReadOnly;
                mnuDisableAllTasks.Visible = Scheduler == null;
                mnuDisableAllTasks.Enabled = !IsReadOnly;
                mnuUnderEnableDisableAllTasks.Visible = mnuEnableAllTasks.Visible;
                mnuLineUnderCreateNewJob.Visible = mnuNewJob.Visible;
                canvasRightClickMenu.Show(sender as Control, e.Location);
            }
            else if ((Control.ModifierKeys & Keys.Control) == Keys.None) // Deselect all item and select the dependency targeted by mouse
            {
                foreach (var view in _selectedViews.ToList())
                    view.IsSelected = false;
                var nearestDependencyView = _jobDepViews.Values.OrderBy(_jobDepView => new PointD(e.Location).SquareDistance(_jobDepView.From, _jobDepView.To.Value))
                                                               .FirstOrDefault();
                if (nearestDependencyView != null && new PointD(e.Location).SquareDistance(nearestDependencyView.From, nearestDependencyView.To.Value) <= 100)
                {
                    nearestDependencyView.IsSelected = true;
                    canvas.Invalidate();
                }
                else
                    RemoveFocus();
            }
        }

        private void mnuNewJob_Click(object sender, EventArgs e)
        {
            canvas.SuspendDrawing();
            var job = Diagram.CreateNewJob();
            var jobView = _jobViews[job];
            jobView.Location = canvas.PointToClient(canvasRightClickMenu.PointToScreen(Point.Empty));
            //jobView.BringToFront();
            jobView.IsSelected = true;
            canvas.ResumeDrawing();
        }

        private void mnuEditSchedule_Click(object sender, EventArgs e)
        {
            EditMenuClick?.Invoke(this, EventArgs.Empty);
        }

        private void tmrRefreshJobExecutionInfo_Tick(object sender, EventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            stateIndicator.State = SchedulerState;
        }

        private void btnEditSharedResources_Click(object sender, EventArgs e)
        {
            var frm = new SharedResourcesForm(Diagram.ResourceManager);
            frm.ShowDialog(this);
        }

        private void btnEditProcessShortcuts_Click(object sender, EventArgs e)
        {
            var frm = new AliasSetForm(Diagram.Aliases);
            frm.ShowDialog(this);
        }

        private void mnuEnableAllTasks_Click(object sender, EventArgs e)
        {
            foreach (var jobView in JobViews.Values)
                jobView.Job.Enabled = true;
        }
        private void mnuDisableAllTasks_Click(object sender, EventArgs e)
        {
            foreach (var jobView in JobViews.Values)
                jobView.Job.Enabled = false;
        }

        private void mnuReduceAllTasks_Click(object sender, EventArgs e)
        {
            foreach (var jobView in JobViews.Values)
                jobView.BestFitSize();
        }
        private void canvas_DoubleClick(object sender, EventArgs e)
        {
            mnuReduceAllTasks.PerformClick();
        }
        new IAsyncResult BeginInvoke(Delegate method)
        {
            if (!Disposing && !IsDisposed)
                return Application.OpenForms[0].BeginInvoke(method);
            Action a = () => { };
            return a.BeginInvoke(null, null);
        }
        new bool InvokeRequired
        {
            get
            {
                return Application.OpenForms.Count == 0 ? false : Application.OpenForms[0].InvokeRequired;
            }
        }
    }
}
 