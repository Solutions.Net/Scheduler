using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

using DevExpress.XtraEditors;

using TechnicalTools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using Automation.Model;


namespace Automation.UI.Winforms
{
    [DebuggerDisplay("View For JobName={Job.JobName,nq}, JobKind={Job.JobKind,nq}, ProcessPath= {Job.ProcessPath,nq}, Arguments={Job.Arguments,nq}")]
    public partial class JobView : EnhancedXtraUserControl, ISelectableView
    {
        public Job Job { get; }

        public JobDiagramView OwnerView { get; }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value)
                    return;
                SelectionChanging?.Invoke(this, value);
                _isSelected = value;
                SelectionChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        bool _isSelected;
        public event Action<JobView, bool> SelectionChanging;
        public event EventHandler SelectionChanged;

        public IEnumerable<JobDependencyView> DependenciesStarting { get { return (OwnerView.Diagram.JobDependenciesBySourceJob.TryGetValueClass(Job) ?? Enumerable.Empty<JobDependency>()).Select(jobDep => OwnerView.JobDepViews[jobDep]); } }
        public IEnumerable<JobDependencyView> DependenciesTargeting { get { return (OwnerView.Diagram.JobDependenciesByTargetJob.TryGetValueClass(Job) ?? Enumerable.Empty<JobDependency>()).Select(jobDep => OwnerView.JobDepViews[jobDep]); } }

        readonly Dictionary<JobExecution, DevExpress.XtraTab.XtraTabPage> _JobExecutionTabPages = new Dictionary<JobExecution, DevExpress.XtraTab.XtraTabPage>();

        public Point Center { get { return new Point(Left + Width / 2, Top + Height / 2); } }
        public int TitleHeight { get { return txtTitle.Height; } }

        [Obsolete("For designer only", true)]
        protected JobView()
            : this(null, null)
        {
        }

        public JobView(Job job, JobDiagramView ownerView)
        {
            Job = job;
            OwnerView = ownerView;

            InitializeComponent();
            txtTitle.IconVisibleChanged += TxtTitle_IconVisibleChanged; // A faire avant LoadJobDefinition

            if (DesignTimeHelper.IsInDesignMode)
                return;
            MinimumSize = new Size(sizeGripResizer.Width + 50, TitleHeight);

            ViewHelper.CreateHandleSafely(() => CreateHandle()); // before an event subscribing

            txtTitle.Warning.ToolTip = "This task has successfully finished but there are warnings you should read!";// Job.Execution?.ExitCode == Job.WarningCode;

            Text = "Job Definition";
            DoubleBuffered = true;
            Height = 155;

            LoadJobDefinition();
            LoadExecutions();

            SelectionChanged += JobView_SelectionChanged;
            JobView_SelectionChanged(this, EventArgs.Empty);
        }

        private void Job_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => Job_PropertyChanged(sender, e)));
                return;
            }
            LoadJobDefinition();
            RefreshJobConfigurationChecks();
            UpdateTitleColor();
        }

        // Prevent flickering when resizing (see https://stackoverflow.com/a/2613272/294998)
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
        //        return cp;
        //    }
        //}

        void DefineEnabilitiesAndVisibilities()
        {

            if (txtTitle.ReadOnly != OwnerView.IsReadOnly)
                txtTitle.ResetDoubleClickSimulationState();
            txtTitle.ReadOnly = OwnerView.IsReadOnly;

            sizeGripResizer.Visible = IsSelected;
        }



        public Point PreviousLocation { get; private set; }
        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
        {
            if (specified.HasFlag(BoundsSpecified.X) && x != Left ||
                specified.HasFlag(BoundsSpecified.Y) && y != Top)
                PreviousLocation = Location;
            base.SetBoundsCore(x, y, width, height, specified);
        }

        private void JobView_SelectionChanged(object sender, EventArgs e)
        {
            DefineEnabilitiesAndVisibilities();
            UpdateTitleColor();
            if (IsSelected)
                BringToFront();
        }
        void UpdateTitleColor()
        {
            var execution = Job.Execution;

            var colors = execution == null           ? IsSelected ? Job.Enabled ? Tuple.Create(Color.PaleTurquoise, Color.Black) : Tuple.Create(Color.LightSteelBlue, Color.White)
                                                                  : Job.Enabled ? (Job.GetExecutionHistory(false).FirstOrDefault()?.ResetedByRestartJob ?? false
                                                                                  ? Tuple.Create(Color.Turquoise,   Color.Black) 
                                                                                  : Tuple.Create(Color.DodgerBlue,  Color.Black)) : Tuple.Create(Color.LightSlateGray, Color.White)
                       : execution.IsSuccess == null ? IsSelected               ? Tuple.Create(Color.LawnGreen,     Color.Black)  : Tuple.Create(Color.LightGreen,     Color.Black)
                       : execution.IsSuccess.Value   ? IsSelected               ? Tuple.Create(Color.LimeGreen,     Color.Black)  : Tuple.Create(Color.ForestGreen,    Color.White)
                                                     : IsSelected               ? Tuple.Create(Color.Orange,        Color.Black)  : Tuple.Create(Color.OrangeRed,      Color.Black);
            txtTitle.BackColor = colors.Item1;
            txtTitle.ForeColor = colors.Item2;
        }

       

        void LoadJobDefinition()
        {
            UnbindModel();

            txtTitle.Text = Job.JobName;

            txtTitle.Warning.Visible = Job.Execution?.ExitCode != null && Job.Execution.ExitCode == Job.WarningCode;
            mmoDescription.Text = Job.Description?.Replace("\r\n", "\n").Replace("\n", "\r\n");

            BindModel();
            DefineEnabilitiesAndVisibilities();
        }
        private void RefreshJobConfigurationChecks()
        {
            if (Job.Execution == null) // Mode design
            {
                var error = Job.CheckConfiguration();
                txtTitle.Error.ToolTip = (error ?? "").Trim();
                txtTitle.Error.Visible = !string.IsNullOrEmpty(txtTitle.Error.ToolTip);
            }
        }
        void LoadExecutions()
        {
            //foreach (var jobExec in Job.GetExecutionHistory(false).AsEnumerable().Reverse().Take(Job.OwnerGroup.MaxExecutionContextLength ?? Job.GetExecutionHistory(false).Count))
            //    OnNewExecutionContextInHistory(jobExec);

            //if (Job.Execution != null)
            //    Job_PropertyChanged(this, new PropertyChangedEventArgs(nameof(Job.Execution)));
        }
        
        void BindModel()
        {
            UnbindModel();
            Job.PropertyChanged += Job_PropertyChanged;
            //Job.GetExecutionHistoryAndSubscribe(ExecutionHistory_AfterListChange, false);
            Disposed += JobView_Disposed;
        }

        void UnbindModel()
        {
            if (Job == null)
                return;
            Job.PropertyChanged -= Job_PropertyChanged;
            //Job.UnsubscribeFromExecutionHistory(ExecutionHistory_AfterListChange);
            Disposed -= JobView_Disposed;
        }

        private void JobView_Disposed(object sender, EventArgs e)
        {
            UnbindModel();
        }

        DevExpress.XtraTab.XtraTabPage CreateTabPage(JobExecution jobExec)
        {
            var jobExecView = new JobExecutionView(jobExec, this);
            var tab = new DevExpress.XtraTab.XtraTabPage();
            tab.Controls.Add(jobExecView);
            jobExecView.Dock = DockStyle.Fill;
            tab.Name = "tabExecution";
            tab.Text = "Execution";
            return tab;
        }

        void TxtTitle_IconVisibleChanged(object sender, EventArgs e)
        {
            var icon = sender as TechnicalTools.UI.Controls.LabelToTextBox.IIcon;
            // at this point we know for sure that icon.Visible value has been toggled
            var defaultIconSize = txtTitle.GetCurrentDefaultIconSize();
            var iconWidth = txtTitle.GetIconWidthForDisplay(icon, defaultIconSize);
            if (icon.Visible)  // icon is now Visible
            {
                var rect = Bounds;
                rect.Inflate(iconWidth / 2, 0);
                Bounds = rect;
            }
            else
            {
                var rect = Bounds;
                rect.Inflate(-iconWidth / 2, 0);
                Bounds = rect;
            }
        }

        private void txtTitle_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Debug.Assert(txtTitle.CausesValidation);
            try
            {
                Job.JobName = txtTitle.Text;
                addToolTipPropertyOnAllControl.Hide(txtTitle);
            }
            catch (Exception ex)
            {
                addToolTipPropertyOnAllControl.Show(ex.Message, txtTitle, 0, txtTitle.Height);
                e.Cancel = true;
            }
        }




        Point? _mouseLocation;
        public bool IsMoving { get; private set; }
        private void txtTitle_MouseDown(object sender, MouseEventArgs e)
        {
            _mouseLocation = e.Location;
            _hasMoved = false;
            _hasBeginDependency = false;
        }


        private void txtTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseLocation.HasValue)
            {
                if (IsSelected)
                {
                    var p = Location;
                    p.Offset(e.Location.X - _mouseLocation.Value.X, e.Location.Y - _mouseLocation.Value.Y);
                    Location = p;
                    // current mouse coordinate should be aligned with mouseLocation again.
                    // but if owner did some change location (or fixed it), the mouse is not aligned so it will raise anew event infinitely
                    // we have to update mouseLocation.
                    _mouseLocation = _mouseLocation.Value.Add(p.Substract(Location)); // p.Substract(Location) is usually equals to (0,0)
                    _hasMoved = true;
                    IsMoving = true;
                }
                // User has to move a lot before we consider this as a a real move
                // this allows user to select JobView easily even if mouse move from one or two pixel
                else if (_mouseLocation.Value.SquareDistance(e.Location) > 10)
                { // draw dependency 
                    if (!_hasBeginDependency)
                    {
                        _hasBeginDependency = true;
                        DependencyBegin?.Invoke(this, EventArgs.Empty);
                    }
                    else
                    {
                        var p = txtTitle.PointToScreen(e.Location); // change referential...
                        p = PointToClient(p); // of mouse position
                        DependencyTargetMouseMove?.Invoke(this, new MouseEventArgs(e.Button, e.Clicks, p.X, p.Y, e.Delta));
                    }
                    _hasMoved = true;
                }

            }
        }
        bool _hasMoved;
        bool _hasBeginDependency;

        private void txtTitle_MouseUp(object sender, MouseEventArgs e)
        {
            IsMoving = false;
            if (_hasBeginDependency)
            {
                DependencyEnd?.Invoke(this, e);
                _hasBeginDependency = false;
            }
            else if (e.Button.HasFlag(MouseButtons.Left))
            {
                if (_mouseLocation.HasValue && !_hasMoved)
                {
                    if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                        System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl) ||
                        !IsSelected)
                        IsSelected = !IsSelected;
                }
                _hasMoved = false;
            }
            else
            {
                var isInScheduler = OwnerView.Scheduler != null;
                var execution = Job.Execution; // fix value to avoid race condition;
                var isJobExecuting = isInScheduler && execution != null && execution.ExitCode == null;

                mnuEnable.Enabled = !Job.Enabled;
                mnuDisable.Enabled = Job.Enabled && !isJobExecuting;
                mnuCloneTask.Enabled = !isInScheduler;

                mnuRunAgain.Enabled = isInScheduler && Job.Execution?.ExitCode != null;
                mnuResetExecution.Enabled = mnuRunAgain.Enabled;
                mnuForceRunNow.Enabled = isInScheduler && Job.Execution == null;

                mnuKillTask.Enabled = isJobExecuting;

                titleRightClickMenu.Show(sender as Control, e.Location);
            }
            _mouseLocation = null;
        }

        public event EventHandler      DependencyBegin;
        public event MouseEventHandler DependencyTargetMouseMove;
        public event MouseEventHandler DependencyEnd;

        public event EventHandler TitleDoubleClick;
        private void txtTitle_DoubleClick(object sender, EventArgs e)
        {
            TitleDoubleClick?.Invoke(this, e);
        }


        public void BestFitSize()
        {
            using (Graphics g = CreateGraphics())
            {
                SizeF size = g.MeasureString(txtTitle.Text, txtTitle.Font);
                var iconsWidth = txtTitle.Icons.Where(icon => icon.Visible).Select(icon => (icon.Size ?? txtTitle.GetCurrentDefaultIconSize()).Width).DefaultIfEmpty(0).Sum();
                Size = new Size((int)Math.Ceiling(size.Width) + borderLineLeft.Width + borderLineRight.Width + iconsWidth + 1, TitleHeight);
            }
        }

        #region Menu

        private void mnuCopyJobName_Click(object sender, EventArgs e)
        {
            CopyToClipBoardUI(Job.JobName);
        }
        private void mnuCopyJobDefinition_Click(object sender, EventArgs e)
        {
            CopyToClipBoardUI(Job.ToXml().ToString());
        }
        void CopyToClipBoardUI(string str)
        {
            try
            {
                Clipboard.SetText(str);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occured, please retry!" +
                                    Environment.NewLine + Environment.NewLine +
                                    ex.ToString());
            }
        }
        private void mnuDisable_Click(object sender, EventArgs e)
        {
            Job.ConsiderAsSuccess = false;
            Job.Enabled = false;
        }
        private void mnuDisableConsiderAsSuccess_Click(object sender, EventArgs e)
        {
            Job.ConsiderAsSuccess = true;
            Job.Enabled = false;
        }
        private void mnuEnable_Click(object sender, EventArgs e)
        {
            Job.Enabled = true;
        }
        private void mnuCloneTask_Click(object sender, EventArgs e)
        {
            var clonedJob = Job.OwnerGroup.CloneJob(Job);
            var clonedJobView = OwnerView.JobViews[clonedJob];
            clonedJobView.RestoreLayout(SaveLayout());
            clonedJobView.IsSelected = true;
        }


        private void mnuRunAgain_Click(object sender, EventArgs e)
        {
            mnuResetExecution.PerformClick();
            mnuForceRunNow.PerformClick();
        }
        private void mnuResetExecution_Click(object sender, EventArgs e)
        {
            OwnerView.Scheduler?.ResetJobExecution(Job);
        }
        private void mnuForceRunNow_Click(object sender, EventArgs e)
        {
            OwnerView.Scheduler?.ForceRun(Job);
        }

        private void mnuKillTask_Click(object sender, EventArgs e)
        {
            OwnerView.Scheduler?.KillExecution(Job);
        }

        #endregion Menu

        public XElement SaveLayout(XElement nJobView = null)
        {
            nJobView = nJobView ?? new XElement("JobView");
            nJobView.Add(new XElement("Location", Location.X + ", " + Location.Y));
            nJobView.Add(new XElement("Size", Size.Width + ", " + Size.Height));
            return nJobView;
        }

        public void RestoreLayout(XElement nJobView)
        {
            var x = int.Parse(nJobView.Element("Location").Value.Split(',')[0].Trim());
            var y = int.Parse(nJobView.Element("Location").Value.Split(',')[1].Trim());
            Location = new Point(x, y);
            var width = int.Parse(nJobView.Element("Size").Value.Split(',')[0].Trim());
            var height = int.Parse(nJobView.Element("Size").Value.Split(',')[1].Trim());
            Size = new Size(width, height); // Restore the size meant to display title without any icon
        }

        new IAsyncResult BeginInvoke(Delegate method)
        {
            if (!Disposing && !IsDisposed)
                return Application.OpenForms[0].BeginInvoke(method);
            Action a = () => { };
            return a.BeginInvoke(null, null);
        }
        new bool InvokeRequired
        {
            get
            {
                return Application.OpenForms[0].InvokeRequired;
            }
        }
    }
}
