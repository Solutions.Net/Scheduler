﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;

using TechnicalTools;
using TechnicalTools.UI;
using TechnicalTools.UI.Controls;
using TechnicalTools.UI.DX;

using Automation.Model;


namespace Automation.UI.Winforms
{
    // Au cas ou : https://psycodedeveloper.wordpress.com/2013/08/15/improving-performance-in-windows-forms-by-temporarily-suspending-redrawing-a-window/
    [DesignTimeVisible(true)]
    [ToolboxItem(true)]
    //[DesignerCategory("TechnicalTools.UI.DX.Automation")]
    public partial class GlobalJobSchedulerView : XtraUserControlBase
    {
        public GlobalJobScheduler GlobalJobScheduler { get { return _GlobalJobScheduler; } set { SetGlobalJobScheduler(value); } }
        GlobalJobScheduler _GlobalJobScheduler;

        JobDiagramView       _referenceDiagramView;
        JobDiagramView       _editingReferenceDiagram;
        DockPanel            _editingReferenceDiagramPanel;
        readonly Dictionary<DailyJobScheduler, JobDiagramView> _DailyJobSchedulerViews = new Dictionary<DailyJobScheduler, JobDiagramView>();

        public GlobalJobSchedulerView()
        {
            InitializeComponent();
            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            Disposed += (_, __) => tmrRefreshJobExecutionInfo.Dispose();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Load += AutomatedJobGroupView_Load;
            TopFormOrPanelClosing += GlobalJobSchedulerView_TopFormOrPanelClosing;
            Text = "Master Job Scheduler";
        }

        private void GlobalJobSchedulerView_TopFormOrPanelClosing(XtraUserControlBase sender, TopFormOrPanelCancelEventArgs e)
        {
            if (GlobalJobScheduler?.State == System.Threading.ThreadState.Running)
            {
                XtraMessageBox.Show(this, "Please stop the global scheduler before closing application!");
                e.Cancel = true;
            }
            else if (GlobalJobScheduler?.State == System.Threading.ThreadState.StopRequested)
            {
                XtraMessageBox.Show(this, "Please wait for global scheduler to stop!");
                e.Cancel = true;
            }
        }

        public void StopAll()
        {
            GlobalJobScheduler.Stop();
            GlobalJobScheduler.Join();
        }

        private void AutomatedJobGroupView_Load(object sender, EventArgs e)
        {
            Disposed += (_, __) => UnbindModelEvents();
        }

        void DefineEnabilitiesAndVisibilities()
        {
            //btnLoad.Enabled = true;
            //btnSave.Enabled = Diagram != null && !IsReadOnly && !string.IsNullOrWhiteSpace(Diagram.Name);
        }

        void SetGlobalJobScheduler(GlobalJobScheduler newValue)
        {
            if (_GlobalJobScheduler != null)
                throw new Exception("Does not support changing global job scheduler after first set. Create a new control.");
            if (_GlobalJobScheduler != null)
            {
                _referenceDiagramView.TopFormOrPanelClosing -= ReferenceDiagram_TopFormOrPanelClosing;
                UnbindModelEvents();
                DestroyAllViews();
                _referenceDiagramView = null;
            }
            _GlobalJobScheduler = newValue;
            if (_GlobalJobScheduler != null)
            {
                BindModelEvents();
                _referenceDiagramView = CreateReferenceDiagramView();
                _referenceDiagramView.TopFormOrPanelClosing += ReferenceDiagram_TopFormOrPanelClosing;
                var dockPanel = DiagramsContainer.DockAsDefault(_referenceDiagramView);
                CreateAllViews();
            }
            DefineEnabilitiesAndVisibilities();
        }
        void ReferenceDiagram_TopFormOrPanelClosing(XtraUserControlBase sender, TopFormOrPanelCancelEventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            e.Cancel = true;
            XtraMessageBox.Show(this, "This view cannot be closed!");
        }
        void BindModelEvents()
        {
            UnbindModelEvents();
            _GlobalJobScheduler.SchedulerCreated += GlobalJobScheduler_SchedulerCreated;
            _GlobalJobScheduler.SchedulerRemoved += GlobalJobScheduler_SchedulerRemoved;
        }
        void UnbindModelEvents()
        {
            if (_GlobalJobScheduler == null)
                return;
            _GlobalJobScheduler.SchedulerCreated -= GlobalJobScheduler_SchedulerCreated;
            _GlobalJobScheduler.SchedulerRemoved -= GlobalJobScheduler_SchedulerRemoved;
        }
        private JobDiagramView CreateReferenceDiagramView()
        {
            var view = new JobDiagramView();
            view.Diagram = GlobalJobScheduler.ReferenceDiagram;
            view.ReadOnly = true;
            view.Text = "Schedule Reference";
            view.ShowEditMenu = true;
            view.EditMenuClick += ReferenceDiagramView_EditMenuClick;
            view.Disposed += (_, __) => view.EditMenuClick -= ReferenceDiagramView_EditMenuClick;
            if (GlobalJobScheduler.Layouts != null)
                view.RestoreLayout(GlobalJobScheduler.Layouts);
            return view;
        }
        private void ReferenceDiagramView_EditMenuClick(object sender, EventArgs e)
        {
            if (_editingReferenceDiagram != null)
            {
                _editingReferenceDiagramPanel.Show();
                return;
            }
            ViewHelper.ShowBusyWhileDoingUIWork(this, "Cloning", () =>
            {
                _editingReferenceDiagram = CreateReferenceDiagramViewForEditing(_referenceDiagramView.Diagram.Clone());
                _editingReferenceDiagramPanel = DiagramsContainer.DockAsDefault(_editingReferenceDiagram);
                _editingReferenceDiagramPanel.ClosingPanel += DockPanel_ClosingPanel;
            });
        }

        private void DockPanel_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            System.Diagnostics.Debug.Assert(_editingReferenceDiagramPanel == sender);

            var view = _editingReferenceDiagram;
            var errors = view.GetDiagramErrors();
            if (!string.IsNullOrWhiteSpace(errors))
            {
                e.Cancel = true;
                MessageBox.Show(this, "Some error(s) must be fixed before saving:" + Environment.NewLine + errors,
                                "Diagram is not consistent", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            var userAnswer = MessageBox.Show(this, "Commit / Inject changes into current global scheduler ?", "Job Scheduler", MessageBoxButtons.YesNoCancel, MessageBoxIcon.None, MessageBoxDefaultButton.Button3);
            if (userAnswer == DialogResult.Cancel)
            {
                e.Cancel = true;
                return;
            }
            var dockPanel = sender as DockPanel;
            bool commitIsSuccess = true;
            if (userAnswer == DialogResult.Yes)
            {
                commitIsSuccess = ViewHelper.ShowBusyWhileDoingUIWork(this, "Committing", () =>
                {
                    // Save to file
                    var nLayout = view.SaveLayout();
                    view.Diagram.Save(nLayout);
                    // Inject in current scheduler
                    GlobalJobScheduler.Load(view.Diagram, nLayout);
                    _referenceDiagramView.RestoreLayout(nLayout);
                    _referenceDiagramView.Reload();
                });
            }
            if (commitIsSuccess)
            {
                _editingReferenceDiagram = null;
                _editingReferenceDiagramPanel = null;
            }
            else
                e.Cancel = true;
        }

        private JobDiagramView CreateReferenceDiagramViewForEditing(JobDiagram diagram)
        {
            var view = new JobDiagramView();
            view.Diagram = diagram;
            view.Text = "Schedule Reference (editing...)";

            if (GlobalJobScheduler.Layouts != null)
                view.RestoreLayout(_referenceDiagramView.SaveLayout());
            return view;
        }

       

        private void CreateAllViews()
        {
            foreach (var scheduler in GlobalJobScheduler.Schedulers.Values)
                GlobalJobScheduler_SchedulerCreated(GlobalJobScheduler, scheduler);
        }
        private void DestroyAllViews()
        {
            
        }
        private void GlobalJobScheduler_SchedulerCreated(object sender, DailyJobScheduler newScheduler)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => GlobalJobScheduler_SchedulerCreated(sender, newScheduler)));
                return;
            }
            if (_DailyJobSchedulerViews.ContainsKey(newScheduler))
                return;
            var view = new JobDiagramView();
            view.Scheduler = newScheduler;
            view.Text = "Plan of " + newScheduler.StartDate.ToString("dd/MM/yyyy") 
                      + (newScheduler.StartDate.TimeOfDay == TimeSpan.Zero ? "" : " " + newScheduler.StartDate.TimeOfDay.ToString("hh'h'mm'm'ss's'"));
            view.RestoreLayout(GlobalJobScheduler.Layouts);
            _DailyJobSchedulerViews.Add(newScheduler, view);
            // When disposed event is raised by user (when closing the view) we delegate the remove to the model.
            // If the disposed is because the model
            view.Disposed += (_, __) => _GlobalJobScheduler.RemoveScheduler(newScheduler); 
            DiagramsContainer.DockAsDefault(view);
            view.Focus();
        }
        // When the model say a scheduler does not exist anymore.
        private void GlobalJobScheduler_SchedulerRemoved(object sender, DailyJobScheduler scheduler)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => GlobalJobScheduler_SchedulerRemoved(sender, scheduler)));
                return;
            }
            JobDiagramView view;
            if (_DailyJobSchedulerViews.TryGetValue(scheduler, out view))
            {
                var parent = view.AllParentControls()
                                 .OfType<ContainerControl>()
                                 .FirstOrDefault(p => p is DockPanel
                                                   || p is Form && p != EnhancedXtraForm.MainForm);
                if (parent is DockPanel)
                    (parent as DockPanel).Close(); // Will call view.Dispose
                else if (parent is Form)
                    (parent as Form).Close(); // Will call view.Dispose
                _DailyJobSchedulerViews.Remove(scheduler);
            }
        }

        private void stateIndicator_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            mnuStart.Enabled = GlobalJobScheduler.State == ThreadState.Stopped 
                            || GlobalJobScheduler.State == ThreadState.StopRequested;
            mnuStopAfterCurrentSchedulers.Enabled = GlobalJobScheduler.State == ThreadState.Running;
            mnuInstanciateSchedulerNow.Enabled = GlobalJobScheduler.State == ThreadState.Running
                                              || GlobalJobScheduler.State == ThreadState.StopRequested;
            mnuStateIndicator.Show(this, e.Location);
        }
        private void mnuStart_Click(object sender, EventArgs e)
        {
            StartScheduler();
        }
        public void StartScheduler()
        {
            _GlobalJobScheduler.Start();
        }
        private void mnuInstanciateSchedulerNow_Click(object sender, EventArgs e)
        {
            InstanciateADailySchedulerNow();
        }
        public void InstanciateADailySchedulerNow()
        {
            _GlobalJobScheduler.InstanciateNow();
        }
        private void mnuStopAfterCurrentSchedulers_Click(object sender, EventArgs e)
        {
            _GlobalJobScheduler.Stop();
        }


        private void tmrRefreshJobExecutionInfo_Tick(object sender, EventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            stateIndicator.State = JobSchedulerState;

            var time = GlobalJobScheduler?.ReferenceDiagram?.DailyTimeToInstanciate // ?. prevent concurrential access issue
                    ?? TimeSpan.Zero;
            var ts = stateIndicator.State == StateIndicator.eState.Blue
                   ? new TimeSpan((DateTime.Now.Date.AddDays(1) + time - DateTime.Now).Ticks % TimeSpan.TicksPerDay)
                   : (TimeSpan?)null;
            lblNextInstanciationRemainingTime.Text = ts == null 
                                                   ? "-" 
                                                   : ts.Value.ToReadableString(); 
        }

        StateIndicator.eState JobSchedulerState
        {
            get
            {
                return GlobalJobScheduler       == null                      ? StateIndicator.eState.Gray
                     : GlobalJobScheduler.State == ThreadState.Running       ? StateIndicator.eState.Blue
                     : GlobalJobScheduler.State == ThreadState.StopRequested ? StateIndicator.eState.Yellow
                     : GlobalJobScheduler.State == ThreadState.Stopped       ? StateIndicator.eState.RedOrange
                     : throw new Exception("Not Handled!");
            }
        }

        private void mnuOpen_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Importing Scheduler Definition";
            openFileDialog.DefaultExt = "xml";
            openFileDialog.Filter = "XML data (*.xml)|*.xml|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            if (DialogResult.OK != openFileDialog.ShowDialog(this))
                return;
            Open(openFileDialog.FileName);
        }
        public bool Open(string filename)
        {
            bool result = false;
            if (GlobalJobScheduler != null && GlobalJobScheduler?.State != System.Threading.ThreadState.Stopped)
                MessageBox.Show("Please stop the scheduler before loading a new one.");
             else
                ViewHelper.ShowBusyWhileDoingUIWork(this, "Loading \"" + filename + "\"...", () =>
                {
                    var diag = new JobDiagramXml(filename);
                    var nViews = diag.Load();
                    GlobalJobScheduler = new GlobalJobScheduler(diag, nViews);
                    mnuOpen.Enabled = false; // setting GlobalJobScheduler wont work a second time.
                    result = true;
                });
            return result;
        }

        new IAsyncResult BeginInvoke(Delegate method)
        {
            if (!Disposing && !IsDisposed)
                return Application.OpenForms[0].BeginInvoke(method);
            Action a = () => { };
            return a.BeginInvoke(null, null);
        }
        new bool InvokeRequired
        {
            get
            {
                return Application.OpenForms[0].InvokeRequired;
            }
        }
    }

}
 