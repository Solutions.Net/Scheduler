﻿namespace Automation.UI.Winforms
{
    partial class JobExecutionsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gcExecutions = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvExecutions = new TechnicalTools.UI.DX.EnhancedGridView();
            this.colJobName = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colStartDate = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colDuration = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colEndDate = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colIsForced = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colIsKilled = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colIsSuccess = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExitCode = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colStdOutput = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colErrOutput = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colSchedulerInternalException = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkFocusNewExecution = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcExecutions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExecutions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFocusNewExecution.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcExecutions
            // 
            this.gcExecutions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcExecutions.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcExecutions.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcExecutions.EmbeddedNavigator.TextStringFormat = "Execution {0} of {1} (visible) of {2} (total)";
            this.gcExecutions.Location = new System.Drawing.Point(0, 40);
            this.gcExecutions.MainView = this.gvExecutions;
            this.gcExecutions.Name = "gcExecutions";
            this.gcExecutions.Size = new System.Drawing.Size(874, 380);
            this.gcExecutions.TabIndex = 0;
            this.gcExecutions.UseEmbeddedNavigator = true;
            this.gcExecutions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvExecutions});
            // 
            // gvExecutions
            // 
            this.gvExecutions.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobName,
            this.colStartDate,
            this.colDuration,
            this.colEndDate,
            this.colIsForced,
            this.colIsKilled,
            this.colIsSuccess,
            this.colExitCode,
            this.colStdOutput,
            this.colErrOutput,
            this.colSchedulerInternalException});
            this.gvExecutions.GridControl = this.gcExecutions;
            this.gvExecutions.Name = "gvExecutions";
            this.gvExecutions.OptionsBehavior.Editable = false;
            this.gvExecutions.OptionsBehavior.ReadOnly = true;
            this.gvExecutions.OptionsSelection.MultiSelect = true;
            this.gvExecutions.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvExecutions.RowItemDoubleClick += new System.EventHandler<TechnicalTools.UI.DX.RowItemDoubleClickEventArgs>(this.gvExecutions_RowItemDoubleClick);
            // 
            // colJobName
            // 
            this.colJobName.Caption = "Job Name";
            this.colJobName.Name = "colJobName";
            this.colJobName.Visible = true;
            this.colJobName.VisibleIndex = 0;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 1;
            // 
            // colDuration
            // 
            this.colDuration.Caption = "Duration";
            this.colDuration.Name = "colDuration";
            this.colDuration.Visible = true;
            this.colDuration.VisibleIndex = 2;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.Name = "colEndDate";
            // 
            // colIsForced
            // 
            this.colIsForced.Caption = "Is Forced";
            this.colIsForced.Name = "colIsForced";
            // 
            // colIsKilled
            // 
            this.colIsKilled.Caption = "Killed By User";
            this.colIsKilled.Name = "colIsKilled";
            // 
            // colIsSuccess
            // 
            this.colIsSuccess.Caption = "Is Success";
            this.colIsSuccess.Name = "colIsSuccess";
            this.colIsSuccess.Visible = true;
            this.colIsSuccess.VisibleIndex = 4;
            // 
            // colExitCode
            // 
            this.colExitCode.Caption = "Exit Code";
            this.colExitCode.Name = "colExitCode";
            this.colExitCode.Visible = true;
            this.colExitCode.VisibleIndex = 3;
            // 
            // colStdOutput
            // 
            this.colStdOutput.Caption = "Std Output";
            this.colStdOutput.Name = "colStdOutput";
            this.colStdOutput.Visible = true;
            this.colStdOutput.VisibleIndex = 5;
            // 
            // colErrOutput
            // 
            this.colErrOutput.Caption = "Err Output";
            this.colErrOutput.Name = "colErrOutput";
            this.colErrOutput.Visible = true;
            this.colErrOutput.VisibleIndex = 6;
            // 
            // colSchedulerInternalException
            // 
            this.colSchedulerInternalException.Caption = "Scheduler\'s Internal Exception";
            this.colSchedulerInternalException.Name = "colSchedulerInternalException";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkFocusNewExecution);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(874, 40);
            this.panelControl1.TabIndex = 1;
            // 
            // chkFocusNewExecution
            // 
            this.chkFocusNewExecution.EditValue = true;
            this.chkFocusNewExecution.Location = new System.Drawing.Point(5, 5);
            this.chkFocusNewExecution.Name = "chkFocusNewExecution";
            this.chkFocusNewExecution.Properties.Caption = "Focus new executions";
            this.chkFocusNewExecution.Size = new System.Drawing.Size(144, 19);
            this.chkFocusNewExecution.TabIndex = 0;
            // 
            // JobExecutionsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcExecutions);
            this.Controls.Add(this.panelControl1);
            this.Name = "JobExecutionsView";
            this.Size = new System.Drawing.Size(874, 420);
            this.Load += new System.EventHandler(this.JobExecutionsView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcExecutions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExecutions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkFocusNewExecution.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TechnicalTools.UI.DX.EnhancedGridControl gcExecutions;
        private TechnicalTools.UI.DX.EnhancedGridView gvExecutions;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit chkFocusNewExecution;
        private TechnicalTools.UI.DX.EnhancedGridColumn colJobName;
        private TechnicalTools.UI.DX.EnhancedGridColumn colStartDate;
        private TechnicalTools.UI.DX.EnhancedGridColumn colDuration;
        private TechnicalTools.UI.DX.EnhancedGridColumn colEndDate;
        private TechnicalTools.UI.DX.EnhancedGridColumn colIsForced;
        private TechnicalTools.UI.DX.EnhancedGridColumn colIsKilled;
        private TechnicalTools.UI.DX.EnhancedGridColumn colIsSuccess;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExitCode;
        private TechnicalTools.UI.DX.EnhancedGridColumn colStdOutput;
        private TechnicalTools.UI.DX.EnhancedGridColumn colErrOutput;
        private TechnicalTools.UI.DX.EnhancedGridColumn colSchedulerInternalException;
    }
}
