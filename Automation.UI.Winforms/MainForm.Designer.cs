﻿namespace Automation.UI.Winforms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.globalJobSchedulerView = new Automation.UI.Winforms.GlobalJobSchedulerView();
            this.SuspendLayout();
            // 
            // globalJobSchedulerView
            // 
            this.globalJobSchedulerView.AutoScroll = true;
            this.globalJobSchedulerView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.globalJobSchedulerView.GlobalJobScheduler = null;
            this.globalJobSchedulerView.Location = new System.Drawing.Point(0, 0);
            this.globalJobSchedulerView.Name = "globalJobSchedulerView";
            this.globalJobSchedulerView.Size = new System.Drawing.Size(1370, 1001);
            this.globalJobSchedulerView.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 1001);
            this.Controls.Add(this.globalJobSchedulerView);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Scheduler";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private Automation.UI.Winforms.GlobalJobSchedulerView globalJobSchedulerView;
    }
}