﻿using System;


namespace Automation.UI.Winforms
{
    public interface IViewLockable
    {
        bool ReadOnly   { get; set; }
        bool IsReadOnly { get; }
    }
}
