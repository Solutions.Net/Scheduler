﻿namespace Automation.UI.Winforms
{
    partial class JobExecutionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lytCtlExecution = new DevExpress.XtraLayout.LayoutControl();
            this.lblDuration = new DevExpress.XtraEditors.LabelControl();
            this.lblExitCode = new DevExpress.XtraEditors.LabelControl();
            this.meStdError = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.meStdOutput = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.lblState = new DevExpress.XtraEditors.LabelControl();
            this.lblStartingTime = new DevExpress.XtraEditors.LabelControl();
            this.lytCtlExecutionGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblStartingTime_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.meStdOutput_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.meStdError_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblExitCode_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterStdOutputError = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.timerRefresh = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlExecution)).BeginInit();
            this.lytCtlExecution.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meStdError.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStdOutput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlExecutionGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStartingTime_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStdOutput_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStdError_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExitCode_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterStdOutputError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // lytCtlExecution
            // 
            this.lytCtlExecution.Controls.Add(this.lblDuration);
            this.lytCtlExecution.Controls.Add(this.lblExitCode);
            this.lytCtlExecution.Controls.Add(this.meStdError);
            this.lytCtlExecution.Controls.Add(this.meStdOutput);
            this.lytCtlExecution.Controls.Add(this.lblState);
            this.lytCtlExecution.Controls.Add(this.lblStartingTime);
            this.lytCtlExecution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lytCtlExecution.Location = new System.Drawing.Point(0, 0);
            this.lytCtlExecution.Name = "lytCtlExecution";
            this.lytCtlExecution.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(717, 191, 250, 350);
            this.lytCtlExecution.Root = this.lytCtlExecutionGroup;
            this.lytCtlExecution.Size = new System.Drawing.Size(378, 426);
            this.lytCtlExecution.TabIndex = 1;
            this.lytCtlExecution.Text = "layoutControl2";
            // 
            // lblDuration
            // 
            this.lblDuration.Location = new System.Drawing.Point(78, 46);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(288, 13);
            this.lblDuration.StyleController = this.lytCtlExecution;
            this.lblDuration.TabIndex = 8;
            // 
            // lblExitCode
            // 
            this.lblExitCode.Location = new System.Drawing.Point(78, 63);
            this.lblExitCode.Name = "lblExitCode";
            this.lblExitCode.Size = new System.Drawing.Size(288, 13);
            this.lblExitCode.StyleController = this.lytCtlExecution;
            this.lblExitCode.TabIndex = 7;
            // 
            // meStdError
            // 
            this.meStdError.Location = new System.Drawing.Point(78, 248);
            this.meStdError.Name = "meStdError";
            this.meStdError.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.meStdError.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.meStdError.Properties.ReadOnly = true;
            this.meStdError.Size = new System.Drawing.Size(288, 166);
            this.meStdError.StyleController = this.lytCtlExecution;
            this.meStdError.TabIndex = 6;
            // 
            // meStdOutput
            // 
            this.meStdOutput.Location = new System.Drawing.Point(78, 80);
            this.meStdOutput.Name = "meStdOutput";
            this.meStdOutput.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.meStdOutput.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.meStdOutput.Properties.ReadOnly = true;
            this.meStdOutput.Size = new System.Drawing.Size(288, 159);
            this.meStdOutput.StyleController = this.lytCtlExecution;
            this.meStdOutput.TabIndex = 5;
            // 
            // lblState
            // 
            this.lblState.Location = new System.Drawing.Point(78, 12);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(288, 13);
            this.lblState.StyleController = this.lytCtlExecution;
            this.lblState.TabIndex = 4;
            // 
            // lblStartingTime
            // 
            this.lblStartingTime.Location = new System.Drawing.Point(78, 29);
            this.lblStartingTime.Name = "lblStartingTime";
            this.lblStartingTime.Size = new System.Drawing.Size(288, 13);
            this.lblStartingTime.StyleController = this.lytCtlExecution;
            this.lblStartingTime.TabIndex = 0;
            // 
            // lytCtlExecutionGroup
            // 
            this.lytCtlExecutionGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lytCtlExecutionGroup.GroupBordersVisible = false;
            this.lytCtlExecutionGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblStartingTime_LayoutItem,
            this.layoutControlItem1,
            this.meStdOutput_LayoutItem,
            this.meStdError_LayoutItem,
            this.lblExitCode_LayoutItem,
            this.splitterStdOutputError,
            this.layoutControlItem2});
            this.lytCtlExecutionGroup.Location = new System.Drawing.Point(0, 0);
            this.lytCtlExecutionGroup.Name = "Root";
            this.lytCtlExecutionGroup.Size = new System.Drawing.Size(378, 426);
            this.lytCtlExecutionGroup.TextVisible = false;
            // 
            // lblStartingTime_LayoutItem
            // 
            this.lblStartingTime_LayoutItem.Control = this.lblStartingTime;
            this.lblStartingTime_LayoutItem.Location = new System.Drawing.Point(0, 17);
            this.lblStartingTime_LayoutItem.Name = "lblStartingTime_LayoutItem";
            this.lblStartingTime_LayoutItem.Size = new System.Drawing.Size(358, 17);
            this.lblStartingTime_LayoutItem.Text = "Starting Time";
            this.lblStartingTime_LayoutItem.TextSize = new System.Drawing.Size(63, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblState;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(358, 17);
            this.layoutControlItem1.Text = "State";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(63, 13);
            // 
            // meStdOutput_LayoutItem
            // 
            this.meStdOutput_LayoutItem.Control = this.meStdOutput;
            this.meStdOutput_LayoutItem.Location = new System.Drawing.Point(0, 68);
            this.meStdOutput_LayoutItem.Name = "meStdOutput_LayoutItem";
            this.meStdOutput_LayoutItem.Size = new System.Drawing.Size(358, 163);
            this.meStdOutput_LayoutItem.Text = "Std Output";
            this.meStdOutput_LayoutItem.TextSize = new System.Drawing.Size(63, 13);
            // 
            // meStdError_LayoutItem
            // 
            this.meStdError_LayoutItem.Control = this.meStdError;
            this.meStdError_LayoutItem.Location = new System.Drawing.Point(0, 236);
            this.meStdError_LayoutItem.Name = "meStdError_LayoutItem";
            this.meStdError_LayoutItem.Size = new System.Drawing.Size(358, 170);
            this.meStdError_LayoutItem.Text = "Std Error";
            this.meStdError_LayoutItem.TextSize = new System.Drawing.Size(63, 13);
            // 
            // lblExitCode_LayoutItem
            // 
            this.lblExitCode_LayoutItem.Control = this.lblExitCode;
            this.lblExitCode_LayoutItem.Location = new System.Drawing.Point(0, 51);
            this.lblExitCode_LayoutItem.Name = "lblExitCode_LayoutItem";
            this.lblExitCode_LayoutItem.Size = new System.Drawing.Size(358, 17);
            this.lblExitCode_LayoutItem.Text = "Exit Code";
            this.lblExitCode_LayoutItem.TextSize = new System.Drawing.Size(63, 13);
            // 
            // splitterStdOutputError
            // 
            this.splitterStdOutputError.AllowHotTrack = true;
            this.splitterStdOutputError.Location = new System.Drawing.Point(0, 231);
            this.splitterStdOutputError.Name = "splitterStdOutputError";
            this.splitterStdOutputError.Size = new System.Drawing.Size(358, 5);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lblDuration;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(358, 17);
            this.layoutControlItem2.Text = "Duration";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(63, 13);
            // 
            // timerRefresh
            // 
            this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
            // 
            // JobExecutionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytCtlExecution);
            this.Name = "JobExecutionView";
            this.Size = new System.Drawing.Size(378, 426);
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlExecution)).EndInit();
            this.lytCtlExecution.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.meStdError.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStdOutput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytCtlExecutionGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStartingTime_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStdOutput_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meStdError_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExitCode_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterStdOutputError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl lytCtlExecution;
        private DevExpress.XtraEditors.LabelControl lblStartingTime;
        private DevExpress.XtraLayout.LayoutControlGroup lytCtlExecutionGroup;
        private DevExpress.XtraEditors.LabelControl lblExitCode;
        private TechnicalTools.UI.DX.Controls.MemoEdit meStdError;
        private TechnicalTools.UI.DX.Controls.MemoEdit meStdOutput;
        private DevExpress.XtraEditors.LabelControl lblState;
        private DevExpress.XtraLayout.LayoutControlItem lblStartingTime_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem meStdOutput_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem meStdError_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem lblExitCode_LayoutItem;
        private DevExpress.XtraLayout.SplitterItem splitterStdOutputError;
        private DevExpress.XtraEditors.LabelControl lblDuration;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.Timer timerRefresh;
    }
}
