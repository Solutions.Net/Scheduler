﻿using System;
using System.Diagnostics;
using TechnicalTools.UI.DX.Forms.Errors;
using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.UI.DX;


namespace Automation.UI.Winforms
{
    // From Project Application.UI (will be replaced later)
    public class DefaultExceptionHandler
    {
        public DefaultExceptionHandler()
        {
            UnexpectedExceptionManager.NewUnhandledException += (_, e) => HandleException(UnknownAction, e.Exception);
        }
        public const string UnknownAction = "Unknown action or background task";


        public void HandleException(string actionName, Exception ex)
        {
            TechnicalErrorPopup.ProvideMaximumErrorData = true;
            string text = "Error during " + (actionName == UnknownAction ? UnknownAction : $"action \"{actionName}\"");

            while ((ex as AggregateException)?.InnerExceptions.Count == 1)
                ex = ex.InnerException;

            // Prevent the same exception to be handle twice
            // This can happens when exception has come here through two different execution flow
            // (ex: BusyForm in UI project catch, handle it and rethrow it
            //      If the method that called BusyForm did it using await keyword, the exception is recatch by ExceptionManager (Assembly events...)
            if (ex.Data.Contains("DefaultExceptionHandler.Handled"))
                return;
            ex.Data["DefaultExceptionHandler.Handled"] = true;

            if (ex is SilentBusinessException)
                return;

            var mainForm = EnhancedXtraForm.MainForm;
            Debug.Assert(mainForm != null, nameof(mainForm) + " != null");
            if (mainForm?.InvokeRequired ?? false)
            {
                // TODO : should do a try catch to test if mainForm has been Disposed !
                mainForm.BeginInvoke((Action)(() => { HandleException(actionName, ex); }));
                return;
            }
            if (ex.IsFileAlreadyOpened())
            {
                mainForm.BeginInvoke((Action)(() =>
                {
                    string prettyMsg = "Cannot open the file, it is already open by another program!";
                    var frm = new TechnicalErrorPopup(text + Environment.NewLine + prettyMsg);
                    frm.Show(mainForm);
                }));
            }
            else if (ex is BusinessException)
            {
                mainForm.BeginInvoke((Action)(() =>
                {
                    var frm = new BusinessErrorPopup(ex, false);
                    frm.Show(mainForm);
                }));
            }
            else if (ex is UserUnderstandableException)
            {
                mainForm.BeginInvoke((Action)(() =>
                {
                    var frm = new UserUnderstandableErrorPopup(ex, false);
                    frm.Show(mainForm);
                }));
            }
            else 
            {
                mainForm.BeginInvoke((Action)(() =>
                {
                    var frm = ex is ICriticalException ? new CriticalErrorPopup(ex) : new TechnicalErrorPopup(ex);
                    // Ne mettre a true QUE si on detecte deux fois la strictement même erreur dans un laps de temps tres court
                    // Cela est souvent dû a des bug dans un des handlers d'evenement de la couche graphique (par exemple Paint ou CustomColumnDisplayText)
                    frm.ForceStopPopupButtonVisible = false;  // DebugTools.IsDebug && DebugTools.IsForDevelopper || 

                    // When there is some probleme in unbound columns in GridView, 
                    // the user constantly has a message each time gridview tries to refresh pixels...
                    // So the user is blocked...
                    // TODO : find a way to prevent the user to be blocked and kill application
                    // Potential solutions:
                    // 1) DX GridView seems to throw on CalcRowHeight and CalcRowCellDrawInfoCore.
                    //    When trying to ignore the exception and return an arbitrary value for heigh, 
                    //    the gridview still seems  completely broken (scrollbar does not works etc)
                    // 2) Another Way is to tell user gridview is maybe broken, but it can choose to ignore the issue (the messagewon't appear)
                    //    it seems the gridview is still broken too, but at least user cna close the window withouit closing application
                    // 3) Send a ticket to DX Support asking a way to handle this (no support currently :'()
                    // Solution chosen : we dont change anything and let user kill application (we hope for a new licence that will allow us to chose solution 3)
                    //if (ex.StackTrace.Contains(".CalcRowHeight("))
                    //{

                    //}
                    //else if (ex.StackTrace.Contains(".CalcRowCellDrawInfoCore("))
                    //{

                    //}
                    //else
                    frm.Show(mainForm);
                    //var th = new Thread(() => ExceptionManager.EnsureNoExceptionButIgnoreIt(() => AssemblyConfig.ExceptionReporter.Show("Unexpected error :-( !", ex)));
                    //th.SetApartmentState(ApartmentState.STA); // Sinon certain controle graphique plante (exemple http://stackoverflow.com/questions/135803/dragdrop-registration-did-not-succeed)
                    //th.Start();
                }));
            }
        }
    }
}
