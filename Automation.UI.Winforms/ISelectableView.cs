﻿using System;


namespace Automation.UI.Winforms
{
    public interface ISelectableView
    {
        bool IsSelected { get; set; }

        event EventHandler SelectionChanged;
    }
}
