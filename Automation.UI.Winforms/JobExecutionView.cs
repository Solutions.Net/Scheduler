﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using Automation.Model;


namespace Automation.UI.Winforms
{
    [ToolboxItem(true)]
    public partial class JobExecutionView : EnhancedXtraUserControl
    {
        public JobExecution Execution { get; }

        public Control OwnerView  { get; }

        [Obsolete("For designer only", true)]
        protected JobExecutionView()
            : this(null, null)
        {
        }
        public static EnhancedXtraForm ShowInForm(JobExecution execution, Control ownerView)
        {
            var frm = new EnhancedXtraForm();
            var ctl = new JobExecutionView(execution, ownerView);
            ctl.Dock = DockStyle.Fill;
            frm.Controls.Add(ctl);
            frm.StartPosition = FormStartPosition.CenterParent;
            var ownerForm = ownerView.FindForm() ?? EnhancedXtraForm.MainForm;
            var r = ownerForm.Bounds;
            Func<int, int> reduce = v => v * 80 / 100;
            frm.SetBounds(r.X + (r.Width - reduce(r.Width))/2,
                          r.Y + (r.Width - reduce(r.Height)) / 2,
                          reduce(r.Width), reduce(r.Height));
            frm.Show(ownerForm);
            ownerView.Disposed += (_, __) => frm.Close();
            return frm;
        }
        public JobExecutionView(JobExecution execution, Control ownerView)
        {
            Execution = execution;
            OwnerView = ownerView;

            InitializeComponent();
            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            Disposed += (_, __) => timerRefresh.Dispose();

            if (DesignTimeHelper.IsInDesignMode)
                return;
            meStdOutput.DisableSelectAllOnFocus = true;
            meStdError.DisableSelectAllOnFocus = true;
            ViewHelper.CreateHandleSafely(() => CreateHandle()); // before any event subscribing

            // Make resizing / drawing etc a LOT faster
            meStdOutput.Properties.WordWrap = false;
            meStdError.Properties.WordWrap = false;

            lytCtlExecution.Appearance.DisabledLayoutItem.ForeColor = Color.Black;
            //DoubleBuffered = true;

            Disposed += JobExecutionView_Disposed;

            LoadJobExecutionState();
            timerRefresh.Enabled = Execution.ExitCode == null;
        }


        private void JobExecutionView_Disposed(object sender, EventArgs e)
        {
            UnbindModel();
        }
     
        private void JobExecution_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (OwnerView.InvokeRequired)
            {
                OwnerView.BeginInvoke((Action)(() => JobExecution_PropertyChanged(sender, e)));
                return;
            }
            LoadJobExecutionState();
            timerRefresh.Enabled = Execution.ExitCode == null;
        }

        void DefineEnabilitiesAndVisibilities()
        {
        }

        void BindModel()
        {
            UnbindModel();
            Execution.PropertyChanged += JobExecution_PropertyChanged;
        }

        void UnbindModel()
        {
            Execution.PropertyChanged -= JobExecution_PropertyChanged;
        }

        private void LoadJobExecutionState()
        {
            UnbindModel();

            lblState.Text = Execution == null ? (Execution.Job.OwnerGroup.InstanciatedFrom == null ? "" : "*** SCHEDULED ***")
                          : Execution.IsSuccess == null ? "*** EXECUTING ***"
                          : Execution.IsSuccess.Value ? "*** SUCCESS ***"
                          : "*** FAILED ***";
            lblStartingTime.Text = Execution?.StartDate == null ? "" : Execution?.StartDate.ToString("yyyy/MM/dd HH':'mm':'ss");
            lblDuration.Text = Execution?.Duration != null ? Execution.Duration.Value.ToString("hh':'mm':'ss")
                             : Execution?.StartDate == null ? "" : (DateTime.Now - Execution.StartDate).ToString("hh':'mm':'ss");
            lblExitCode.Text = Execution?.ExitCode?.ToString();
            UpdateMemoEdit(meStdOutput, Execution?.StandardOutput);
            UpdateMemoEdit(meStdError, Execution?.StandardError);
            RefreshDuration();
            BindModel();
            DefineEnabilitiesAndVisibilities();
        }
        void UpdateMemoEdit(TechnicalTools.UI.DX.Controls.MemoEdit mmo, string newText)
        {
            Debug.Assert(mmo.DisableSelectAllOnFocus, "Without this, it can't work really good!");
            var selStart = mmo.SelectionStart;
            var selLength = mmo.SelectionLength;
            var oldText = mmo.Text;
            var restoreCursorPositionOrSelection = selStart + selLength == oldText.Length;
            mmo.Text = newText;
            if (restoreCursorPositionOrSelection)
            {
                mmo.SelectionStart = selLength == 0 ? newText.Length : selStart;
                mmo.SelectionLength = selLength == 0 ? newText.Length : selLength + (newText.Length - oldText.Length);
                mmo.ScrollToCaret();
            }
        }
        void RefreshDuration()
        {
            lblDuration.Text = Execution?.Duration != null ? Execution.Duration.Value.ToString("hh':'mm':'ss")
                             : Execution?.StartDate == null ? "" : (DateTime.Now - Execution.StartDate).ToString("hh':'mm':'ss");
        }
        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            RefreshDuration();
        }
        new IAsyncResult BeginInvoke(Delegate method)
        {
            if (!Disposing && !IsDisposed)
                return Application.OpenForms[0].BeginInvoke(method);
            Action a = () => { };
            return a.BeginInvoke(null, null);
        }
        new bool InvokeRequired
        {
            get
            {
                return Application.OpenForms[0].InvokeRequired;
            }
        }
    }
}
