﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;


using DevExpress.XtraVerticalGrid.Rows;

using TechnicalTools;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;

using Automation.Model;


namespace Automation.UI.Winforms
{
    [DebuggerDisplay("View For JobName={Job.JobName,nq}, JobKind={Job.JobKind,nq}, ProcessPath= {Job.ProcessPath,nq}, Arguments={Job.Arguments,nq}")]
    [ToolboxItem(true)]
    public partial class JobDependencyEditView : EnhancedXtraUserControl
    {
        public JobDependency JobDependency { get; private set; }
        public IViewLockable OwnerView     { get; private set; }

        public   bool ReadOnly             { get { return _ReadOnly; } set { _ReadOnly = value; DefineEnabilitiesAndVisibilities(); } } bool _ReadOnly;
        public   bool IsReadOnly           { get { return ReadOnly 
                                                       || OwnerView == null || OwnerView.IsReadOnly
                                                       || JobDependency == null || JobDependency.Source.JobKind == eJobKind.Restart; } }

        
        public JobDependencyEditView()
        {
            InitializeComponent();

            if (DesignTimeHelper.IsInDesignMode)
                return;

            ViewHelper.CreateHandleSafely(() => CreateHandle()); // before an event subscribing
            propertyGridControl.OptionsView.ShowRootCategories = false;
        }
        public void Bind(JobDependency jobDependency, IViewLockable ownerView)
        {
            propertyGridControl.BeginUpdate();
            UnbindModel();
            JobDependency = jobDependency;
            OwnerView = ownerView;
            LoadJobDependencyDefinition();
            BindModel();
            DefineEnabilitiesAndVisibilities();
            propertyGridControl.EndUpdate();
        }
        public void Unbind()
        {
            propertyGridControl.BeginUpdate();
            UnbindModel();
            JobDependency = null;
            OwnerView = null;
            //LoadJobDependencyDefinition();
            DefineEnabilitiesAndVisibilities();
            propertyGridControl.EndUpdate();
        }

        void DefineEnabilitiesAndVisibilities()
        {
        }
        private void propertyGridControl_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = IsReadOnly;
        }

        #region Binding From Model => View

        void LoadJobDependencyDefinition()
        {

        }

        void BindModel()
        {
            UnbindModel();
            propertyGridControl.SelectedObject = JobDependency;
            propertyGridControl.Rows.Clear();
            if (JobDependency != null)
            {
                EditorRow rowSourceJobState = new EditorRow(nameof(JobDependency.SourceJobState));
                rowSourceJobState.Properties.Caption = "Source Job State";
                propertyGridControl.Rows.Add(rowSourceJobState);
            }
            JobDependency.PropertyChanged += Job_PropertyChanged;
            Disposed += JobView_Disposed;
        }
        void UnbindModel()
        {
            if (JobDependency == null)
                return;
            JobDependency.PropertyChanged -= Job_PropertyChanged;
            propertyGridControl.SelectedObject = null;
            Disposed -= JobView_Disposed;
        }
        private void JobView_Disposed(object sender, EventArgs e)
        {
            UnbindModel();
        }
        private void Job_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)(() => Job_PropertyChanged(sender, e)));
                return;
            }
            LoadJobDependencyDefinition();
        }
        #endregion Binding From Model => View

        #region Binding From View => Model

        // Nothing

        #endregion Binding From View => Model

        new IAsyncResult BeginInvoke(Delegate method)
        {
            if (!Disposing && !IsDisposed)
                return Application.OpenForms[0].BeginInvoke(method);
            Action a = () => { };
            return a.BeginInvoke(null, null);
        }
        new bool InvokeRequired
        {
            get
            {
                return Application.OpenForms[0].InvokeRequired;
            }
        }


    }
}
