﻿namespace Automation.UI.Winforms
{
    partial class GlobalJobSchedulerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainLayout = new DevExpress.XtraLayout.LayoutControl();
            this.lblNextInstanciationRemainingTime = new System.Windows.Forms.Label();
            this.stateIndicator = new TechnicalTools.UI.Controls.StateIndicator();
            this.DiagramsContainer = new TechnicalTools.UI.DX.XtraUserControlComposite();
            this.mainLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.DiagramsContainer_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.stateIndicator_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tmrRefreshJobExecutionInfo = new System.Windows.Forms.Timer(this.components);
            this.toolTipControler = new System.Windows.Forms.ToolTip(this.components);
            this.mnuStateIndicator = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuStart = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuInstanciateSchedulerNow = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStopAfterCurrentSchedulers = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpen = new System.Windows.Forms.MenuStrip();
            this.openSchedulerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.mainLayout)).BeginInit();
            this.mainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiagramsContainer_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicator_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.mnuStateIndicator.SuspendLayout();
            this.mnuOpen.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainLayout
            // 
            this.mainLayout.Controls.Add(this.lblNextInstanciationRemainingTime);
            this.mainLayout.Controls.Add(this.stateIndicator);
            this.mainLayout.Controls.Add(this.DiagramsContainer);
            this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayout.Location = new System.Drawing.Point(0, 24);
            this.mainLayout.Name = "mainLayout";
            this.mainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(838, 256, 250, 350);
            this.mainLayout.Root = this.mainLayoutGroup;
            this.mainLayout.Size = new System.Drawing.Size(784, 461);
            this.mainLayout.TabIndex = 1;
            this.mainLayout.Text = "layoutControl1";
            // 
            // lblNextInstanciationRemainingTime
            // 
            this.lblNextInstanciationRemainingTime.Location = new System.Drawing.Point(240, 12);
            this.lblNextInstanciationRemainingTime.Name = "lblNextInstanciationRemainingTime";
            this.lblNextInstanciationRemainingTime.Size = new System.Drawing.Size(532, 32);
            this.lblNextInstanciationRemainingTime.TabIndex = 7;
            this.lblNextInstanciationRemainingTime.Text = "-";
            this.lblNextInstanciationRemainingTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stateIndicator
            // 
            this.stateIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.stateIndicator.Location = new System.Drawing.Point(12, 12);
            this.stateIndicator.MaximumSize = new System.Drawing.Size(32, 32);
            this.stateIndicator.MinimumSize = new System.Drawing.Size(32, 32);
            this.stateIndicator.Name = "stateIndicator";
            this.stateIndicator.Size = new System.Drawing.Size(32, 32);
            this.stateIndicator.State = TechnicalTools.UI.Controls.StateIndicator.eState.Gray;
            this.stateIndicator.TabIndex = 6;
            this.stateIndicator.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stateIndicator_MouseUp);
            // 
            // DiagramsContainer
            // 
            this.DiagramsContainer.Location = new System.Drawing.Point(12, 48);
            this.DiagramsContainer.Name = "DiagramsContainer";
            this.DiagramsContainer.Size = new System.Drawing.Size(760, 401);
            this.DiagramsContainer.TabIndex = 0;
            // 
            // mainLayoutGroup
            // 
            this.mainLayoutGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.mainLayoutGroup.GroupBordersVisible = false;
            this.mainLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.DiagramsContainer_LayoutItem,
            this.stateIndicator_LayoutItem,
            this.layoutControlItem1});
            this.mainLayoutGroup.Location = new System.Drawing.Point(0, 0);
            this.mainLayoutGroup.Name = "Root";
            this.mainLayoutGroup.Size = new System.Drawing.Size(784, 461);
            this.mainLayoutGroup.TextVisible = false;
            // 
            // DiagramsContainer_LayoutItem
            // 
            this.DiagramsContainer_LayoutItem.Control = this.DiagramsContainer;
            this.DiagramsContainer_LayoutItem.Location = new System.Drawing.Point(0, 36);
            this.DiagramsContainer_LayoutItem.Name = "DiagramsContainer_LayoutItem";
            this.DiagramsContainer_LayoutItem.Size = new System.Drawing.Size(764, 405);
            this.DiagramsContainer_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.DiagramsContainer_LayoutItem.TextVisible = false;
            // 
            // stateIndicator_LayoutItem
            // 
            this.stateIndicator_LayoutItem.Control = this.stateIndicator;
            this.stateIndicator_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.stateIndicator_LayoutItem.Name = "stateIndicator_LayoutItem";
            this.stateIndicator_LayoutItem.Size = new System.Drawing.Size(36, 36);
            this.stateIndicator_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.stateIndicator_LayoutItem.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lblNextInstanciationRemainingTime;
            this.layoutControlItem1.Location = new System.Drawing.Point(36, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(728, 36);
            this.layoutControlItem1.Text = " Next Instanciation of daily scheduler in";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(189, 13);
            // 
            // tmrRefreshJobExecutionInfo
            // 
            this.tmrRefreshJobExecutionInfo.Enabled = true;
            this.tmrRefreshJobExecutionInfo.Interval = 3000;
            this.tmrRefreshJobExecutionInfo.Tick += new System.EventHandler(this.tmrRefreshJobExecutionInfo_Tick);
            // 
            // mnuStateIndicator
            // 
            this.mnuStateIndicator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuStart,
            this.mnuInstanciateSchedulerNow,
            this.mnuStopAfterCurrentSchedulers});
            this.mnuStateIndicator.Name = "mnuStateIndicator";
            this.mnuStateIndicator.Size = new System.Drawing.Size(226, 92);
            // 
            // mnuStart
            // 
            this.mnuStart.Name = "mnuStart";
            this.mnuStart.Size = new System.Drawing.Size(225, 22);
            this.mnuStart.Text = "Start";
            this.mnuStart.Click += new System.EventHandler(this.mnuStart_Click);
            // 
            // mnuInstanciateSchedulerNow
            // 
            this.mnuInstanciateSchedulerNow.Name = "mnuInstanciateSchedulerNow";
            this.mnuInstanciateSchedulerNow.Size = new System.Drawing.Size(225, 22);
            this.mnuInstanciateSchedulerNow.Text = "Instanciate a scheduler Now";
            this.mnuInstanciateSchedulerNow.Click += new System.EventHandler(this.mnuInstanciateSchedulerNow_Click);
            // 
            // mnuStopAfterCurrentSchedulers
            // 
            this.mnuStopAfterCurrentSchedulers.Name = "mnuStopAfterCurrentSchedulers";
            this.mnuStopAfterCurrentSchedulers.Size = new System.Drawing.Size(225, 22);
            this.mnuStopAfterCurrentSchedulers.Text = "Stop after current schedulers";
            this.mnuStopAfterCurrentSchedulers.Click += new System.EventHandler(this.mnuStopAfterCurrentSchedulers_Click);
            // 
            // mnuOpen
            // 
            this.mnuOpen.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openSchedulerToolStripMenuItem});
            this.mnuOpen.Location = new System.Drawing.Point(0, 0);
            this.mnuOpen.Name = "mnuOpen";
            this.mnuOpen.Size = new System.Drawing.Size(784, 24);
            this.mnuOpen.TabIndex = 2;
            this.mnuOpen.Text = "menuStrip1";
            // 
            // openSchedulerToolStripMenuItem
            // 
            this.openSchedulerToolStripMenuItem.Name = "openSchedulerToolStripMenuItem";
            this.openSchedulerToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.openSchedulerToolStripMenuItem.Text = "Open scheduler";
            this.openSchedulerToolStripMenuItem.Click += new System.EventHandler(this.mnuOpen_Click);
            // 
            // GlobalJobSchedulerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.mainLayout);
            this.Controls.Add(this.mnuOpen);
            this.Name = "GlobalJobSchedulerView";
            this.Size = new System.Drawing.Size(784, 485);
            ((System.ComponentModel.ISupportInitialize)(this.mainLayout)).EndInit();
            this.mainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiagramsContainer_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicator_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.mnuStateIndicator.ResumeLayout(false);
            this.mnuOpen.ResumeLayout(false);
            this.mnuOpen.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl mainLayout;
        private DevExpress.XtraLayout.LayoutControlGroup mainLayoutGroup;
        private System.Windows.Forms.Timer tmrRefreshJobExecutionInfo;
        private TechnicalTools.UI.Controls.StateIndicator stateIndicator;
        private DevExpress.XtraLayout.LayoutControlItem stateIndicator_LayoutItem;
        private System.Windows.Forms.ToolTip toolTipControler;
        private System.Windows.Forms.ContextMenuStrip mnuStateIndicator;
        private System.Windows.Forms.ToolStripMenuItem mnuStart;
        private System.Windows.Forms.ToolStripMenuItem mnuStopAfterCurrentSchedulers;
        private System.Windows.Forms.ToolStripMenuItem mnuInstanciateSchedulerNow;
        private System.Windows.Forms.Label lblNextInstanciationRemainingTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.MenuStrip mnuOpen;
        private System.Windows.Forms.ToolStripMenuItem openSchedulerToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private TechnicalTools.UI.DX.XtraUserControlComposite DiagramsContainer;
        private DevExpress.XtraLayout.LayoutControlItem DiagramsContainer_LayoutItem;
    }
}
