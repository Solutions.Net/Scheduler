﻿namespace Automation.UI.Winforms
{
    partial class JobDiagramView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnEditSharedResources = new DevExpress.XtraEditors.SimpleButton();
            this.txtDailyScheduleHistoryLength = new DevExpress.XtraEditors.TextEdit();
            this.stateIndicator = new TechnicalTools.UI.Controls.StateIndicator();
            this.btnEditProcessShortcuts = new DevExpress.XtraEditors.SimpleButton();
            this.txtLogPath = new DevExpress.XtraEditors.TextEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.txtDiagramName = new System.Windows.Forms.TextBox();
            this.tseInstanciationTime = new DevExpress.XtraEditors.TimeSpanEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnLoad_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnSave_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.stateIndicator_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtDailyScheduleHistoryLength_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.txtFocusStealer = new System.Windows.Forms.TextBox();
            this.canvas = new DevExpress.XtraEditors.XtraScrollableControl();
            this.canvasRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuNewJob = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuEditSchedule = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLineUnderCreateNewJob = new System.Windows.Forms.ToolStripSeparator();
            this.mnuEnableAllTasks = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDisableAllTasks = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUnderEnableDisableAllTasks = new System.Windows.Forms.ToolStripSeparator();
            this.mnuReduceAllTasks = new System.Windows.Forms.ToolStripMenuItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.jobDependencyEditView = new Automation.UI.Winforms.JobDependencyEditView();
            this.jobDefinitionView = new Automation.UI.Winforms.JobDefinitionView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lytGrpElementEditor = new DevExpress.XtraLayout.LayoutControlGroup();
            this.jobDefinitionView_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.jobDependencyDefinitionView_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.tmrRefreshJobExecutionInfo = new System.Windows.Forms.Timer(this.components);
            this.addToolTipPropertyOnAllControl = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.jobExecutionsView = new Automation.UI.Winforms.JobExecutionsView();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lytGrpExecutions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.mnuStateIndicator = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuStart = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStopAfterCurrentExecutingJobs = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDailyScheduleHistoryLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseInstanciationTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoad_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicator_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDailyScheduleHistoryLength_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.canvasRightClickMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytGrpElementEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobDefinitionView_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobDependencyDefinitionView_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytGrpExecutions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.mnuStateIndicator.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(86, 60);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(93, 22);
            this.btnSave.StyleController = this.layoutControl2;
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btnEditSharedResources);
            this.layoutControl2.Controls.Add(this.txtDailyScheduleHistoryLength);
            this.layoutControl2.Controls.Add(this.stateIndicator);
            this.layoutControl2.Controls.Add(this.btnEditProcessShortcuts);
            this.layoutControl2.Controls.Add(this.txtLogPath);
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Controls.Add(this.btnSave);
            this.layoutControl2.Controls.Add(this.txtDiagramName);
            this.layoutControl2.Controls.Add(this.tseInstanciationTime);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 2);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1049, 171, 383, 560);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1053, 94);
            this.layoutControl2.TabIndex = 7;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnEditSharedResources
            // 
            this.btnEditSharedResources.Location = new System.Drawing.Point(415, 60);
            this.btnEditSharedResources.Name = "btnEditSharedResources";
            this.btnEditSharedResources.Size = new System.Drawing.Size(311, 22);
            this.btnEditSharedResources.StyleController = this.layoutControl2;
            this.btnEditSharedResources.TabIndex = 15;
            this.btnEditSharedResources.Text = "Edit Shared Resources";
            this.btnEditSharedResources.Click += new System.EventHandler(this.btnEditSharedResources_Click);
            // 
            // txtDailyScheduleHistoryLength
            // 
            this.txtDailyScheduleHistoryLength.Location = new System.Drawing.Point(361, 60);
            this.txtDailyScheduleHistoryLength.Name = "txtDailyScheduleHistoryLength";
            this.txtDailyScheduleHistoryLength.Size = new System.Drawing.Size(50, 20);
            this.txtDailyScheduleHistoryLength.StyleController = this.layoutControl2;
            this.txtDailyScheduleHistoryLength.TabIndex = 14;
            this.txtDailyScheduleHistoryLength.EditValueChanged += new System.EventHandler(this.txtAutoCloseDays_EditValueChanged);
            this.txtDailyScheduleHistoryLength.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAutoCloseDays_KeyDown);
            // 
            // stateIndicator
            // 
            this.stateIndicator.BackColor = System.Drawing.Color.Silver;
            this.stateIndicator.Location = new System.Drawing.Point(12, 12);
            this.stateIndicator.MinimumSize = new System.Drawing.Size(32, 32);
            this.stateIndicator.Name = "stateIndicator";
            this.stateIndicator.Size = new System.Drawing.Size(70, 70);
            this.stateIndicator.State = TechnicalTools.UI.Controls.StateIndicator.eState.Gray;
            this.stateIndicator.StateColor = System.Drawing.Color.Gray;
            this.stateIndicator.TabIndex = 13;
            this.stateIndicator.UseColorMode = true;
            this.stateIndicator.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stateIndicator_MouseUp);
            // 
            // btnEditProcessShortcuts
            // 
            this.btnEditProcessShortcuts.Location = new System.Drawing.Point(730, 60);
            this.btnEditProcessShortcuts.Name = "btnEditProcessShortcuts";
            this.btnEditProcessShortcuts.Size = new System.Drawing.Size(311, 22);
            this.btnEditProcessShortcuts.StyleController = this.layoutControl2;
            this.btnEditProcessShortcuts.TabIndex = 12;
            this.btnEditProcessShortcuts.Text = "Edit Process Shortcuts";
            this.btnEditProcessShortcuts.Click += new System.EventHandler(this.btnEditProcessShortcuts_Click);
            // 
            // txtLogPath
            // 
            this.txtLogPath.Location = new System.Drawing.Point(268, 36);
            this.txtLogPath.Name = "txtLogPath";
            this.txtLogPath.Size = new System.Drawing.Size(773, 20);
            this.txtLogPath.StyleController = this.layoutControl2;
            this.txtLogPath.TabIndex = 11;
            this.txtLogPath.EditValueChanged += new System.EventHandler(this.txtLogPath_EditValueChanged);
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.Location = new System.Drawing.Point(86, 12);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(93, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            this.btnLoad.TabIndex = 4;
            this.btnLoad.Text = "(Re)load";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // txtDiagramName
            // 
            this.txtDiagramName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiagramName.Location = new System.Drawing.Point(268, 12);
            this.txtDiagramName.Name = "txtDiagramName";
            this.txtDiagramName.Size = new System.Drawing.Size(550, 20);
            this.txtDiagramName.TabIndex = 5;
            this.txtDiagramName.Validating += new System.ComponentModel.CancelEventHandler(this.txtDiagramName_Validating);
            // 
            // tseInstanciationTime
            // 
            this.tseInstanciationTime.EditValue = System.TimeSpan.Parse("00:00:00");
            this.tseInstanciationTime.Location = new System.Drawing.Point(988, 12);
            this.tseInstanciationTime.Name = "tseInstanciationTime";
            this.tseInstanciationTime.Properties.AllowEditDays = false;
            this.tseInstanciationTime.Properties.Mask.EditMask = "HH:mm:ss";
            this.tseInstanciationTime.Size = new System.Drawing.Size(53, 20);
            this.tseInstanciationTime.StyleController = this.layoutControl2;
            this.tseInstanciationTime.TabIndex = 2;
            this.tseInstanciationTime.EditValueChanged += new System.EventHandler(this.tseInstanciationTime_EditValueChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.btnLoad_LayoutItem,
            this.emptySpaceItem1,
            this.btnSave_LayoutItem,
            this.emptySpaceItem4,
            this.layoutControlItem9,
            this.stateIndicator_LayoutItem,
            this.layoutControlItem5,
            this.layoutControlItem8,
            this.txtDailyScheduleHistoryLength_LayoutItem,
            this.layoutControlItem4});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1053, 94);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtDiagramName;
            this.layoutControlItem3.Location = new System.Drawing.Point(184, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(626, 24);
            this.layoutControlItem3.Text = "Diagram Name";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(69, 13);
            // 
            // btnLoad_LayoutItem
            // 
            this.btnLoad_LayoutItem.Control = this.btnLoad;
            this.btnLoad_LayoutItem.Location = new System.Drawing.Point(74, 0);
            this.btnLoad_LayoutItem.Name = "btnLoad_LayoutItem";
            this.btnLoad_LayoutItem.Size = new System.Drawing.Size(97, 26);
            this.btnLoad_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnLoad_LayoutItem.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(171, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(13, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(13, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(13, 74);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnSave_LayoutItem
            // 
            this.btnSave_LayoutItem.Control = this.btnSave;
            this.btnSave_LayoutItem.Location = new System.Drawing.Point(74, 48);
            this.btnSave_LayoutItem.MaxSize = new System.Drawing.Size(97, 26);
            this.btnSave_LayoutItem.MinSize = new System.Drawing.Size(97, 26);
            this.btnSave_LayoutItem.Name = "btnSave_LayoutItem";
            this.btnSave_LayoutItem.Size = new System.Drawing.Size(97, 26);
            this.btnSave_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnSave_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnSave_LayoutItem.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(74, 26);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(97, 22);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnEditProcessShortcuts;
            this.layoutControlItem9.Location = new System.Drawing.Point(718, 48);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(315, 26);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // stateIndicator_LayoutItem
            // 
            this.stateIndicator_LayoutItem.Control = this.stateIndicator;
            this.stateIndicator_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.stateIndicator_LayoutItem.MaxSize = new System.Drawing.Size(74, 260);
            this.stateIndicator_LayoutItem.MinSize = new System.Drawing.Size(74, 36);
            this.stateIndicator_LayoutItem.Name = "stateIndicator_LayoutItem";
            this.stateIndicator_LayoutItem.Size = new System.Drawing.Size(74, 74);
            this.stateIndicator_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.stateIndicator_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.stateIndicator_LayoutItem.TextVisible = false;
            this.stateIndicator_LayoutItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.tseInstanciationTime;
            this.layoutControlItem5.Location = new System.Drawing.Point(810, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(223, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(223, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Run this job diagram each day at ";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(161, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtLogPath;
            this.layoutControlItem8.Location = new System.Drawing.Point(184, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(849, 24);
            this.layoutControlItem8.Text = " Log Path";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(69, 13);
            // 
            // txtDailyScheduleHistoryLength_LayoutItem
            // 
            this.txtDailyScheduleHistoryLength_LayoutItem.Control = this.txtDailyScheduleHistoryLength;
            this.txtDailyScheduleHistoryLength_LayoutItem.Location = new System.Drawing.Point(184, 48);
            this.txtDailyScheduleHistoryLength_LayoutItem.MaxSize = new System.Drawing.Size(219, 24);
            this.txtDailyScheduleHistoryLength_LayoutItem.MinSize = new System.Drawing.Size(219, 24);
            this.txtDailyScheduleHistoryLength_LayoutItem.Name = "txtDailyScheduleHistoryLength_LayoutItem";
            this.txtDailyScheduleHistoryLength_LayoutItem.Size = new System.Drawing.Size(219, 26);
            this.txtDailyScheduleHistoryLength_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.txtDailyScheduleHistoryLength_LayoutItem.Text = "Max Daily schedule history length";
            this.txtDailyScheduleHistoryLength_LayoutItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.txtDailyScheduleHistoryLength_LayoutItem.TextSize = new System.Drawing.Size(160, 13);
            this.txtDailyScheduleHistoryLength_LayoutItem.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnEditSharedResources;
            this.layoutControlItem4.Location = new System.Drawing.Point(403, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(315, 26);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // panelTop
            // 
            this.panelTop.Appearance.BackColor = System.Drawing.Color.Silver;
            this.panelTop.Appearance.Options.UseBackColor = true;
            this.panelTop.Controls.Add(this.layoutControl2);
            this.panelTop.Controls.Add(this.txtFocusStealer);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1057, 98);
            this.panelTop.TabIndex = 1;
            // 
            // txtFocusStealer
            // 
            this.txtFocusStealer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFocusStealer.Location = new System.Drawing.Point(943, 7);
            this.txtFocusStealer.Name = "txtFocusStealer";
            this.txtFocusStealer.Size = new System.Drawing.Size(109, 21);
            this.txtFocusStealer.TabIndex = 1;
            this.txtFocusStealer.Text = "Hack to control focus";
            this.txtFocusStealer.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFocusStealer_KeyUp);
            // 
            // canvas
            // 
            this.canvas.Appearance.BackColor = System.Drawing.Color.White;
            this.canvas.Appearance.Options.UseBackColor = true;
            this.canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvas.Location = new System.Drawing.Point(0, 0);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(385, 369);
            this.canvas.TabIndex = 2;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            this.canvas.DoubleClick += new System.EventHandler(this.canvas_DoubleClick);
            this.canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
            this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
            this.canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
            // 
            // canvasRightClickMenu
            // 
            this.canvasRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewJob,
            this.mnuEditSchedule,
            this.mnuLineUnderCreateNewJob,
            this.mnuEnableAllTasks,
            this.mnuDisableAllTasks,
            this.mnuUnderEnableDisableAllTasks,
            this.mnuReduceAllTasks});
            this.canvasRightClickMenu.Name = "canvasRightClickMenu";
            this.canvasRightClickMenu.Size = new System.Drawing.Size(233, 126);
            // 
            // mnuNewJob
            // 
            this.mnuNewJob.Name = "mnuNewJob";
            this.mnuNewJob.Size = new System.Drawing.Size(232, 22);
            this.mnuNewJob.Text = "Create new Job";
            this.mnuNewJob.Click += new System.EventHandler(this.mnuNewJob_Click);
            // 
            // mnuEditSchedule
            // 
            this.mnuEditSchedule.Name = "mnuEditSchedule";
            this.mnuEditSchedule.Size = new System.Drawing.Size(232, 22);
            this.mnuEditSchedule.Text = "Edit schedule";
            this.mnuEditSchedule.Click += new System.EventHandler(this.mnuEditSchedule_Click);
            // 
            // mnuLineUnderCreateNewJob
            // 
            this.mnuLineUnderCreateNewJob.Name = "mnuLineUnderCreateNewJob";
            this.mnuLineUnderCreateNewJob.Size = new System.Drawing.Size(229, 6);
            // 
            // mnuEnableAllTasks
            // 
            this.mnuEnableAllTasks.Name = "mnuEnableAllTasks";
            this.mnuEnableAllTasks.Size = new System.Drawing.Size(232, 22);
            this.mnuEnableAllTasks.Text = "Enable all tasks";
            this.mnuEnableAllTasks.Click += new System.EventHandler(this.mnuEnableAllTasks_Click);
            // 
            // mnuDisableAllTasks
            // 
            this.mnuDisableAllTasks.Name = "mnuDisableAllTasks";
            this.mnuDisableAllTasks.Size = new System.Drawing.Size(232, 22);
            this.mnuDisableAllTasks.Text = "Disable all tasks";
            this.mnuDisableAllTasks.Click += new System.EventHandler(this.mnuDisableAllTasks_Click);
            // 
            // mnuUnderEnableDisableAllTasks
            // 
            this.mnuUnderEnableDisableAllTasks.Name = "mnuUnderEnableDisableAllTasks";
            this.mnuUnderEnableDisableAllTasks.Size = new System.Drawing.Size(229, 6);
            // 
            // mnuReduceAllTasks
            // 
            this.mnuReduceAllTasks.Name = "mnuReduceAllTasks";
            this.mnuReduceAllTasks.Size = new System.Drawing.Size(232, 22);
            this.mnuReduceAllTasks.Text = "Reduce all tasks (double click)";
            this.mnuReduceAllTasks.Click += new System.EventHandler(this.mnuReduceAllTasks_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.jobDependencyEditView);
            this.layoutControl1.Controls.Add(this.jobDefinitionView);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1136, 551, 471, 517);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(662, 369);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            this.layoutControl1.GroupExpandChanging += new DevExpress.XtraLayout.Utils.LayoutGroupCancelEventHandler(this.layoutControl1_GroupExpandChanging);
            // 
            // jobDependencyEditView
            // 
            this.jobDependencyEditView.Location = new System.Drawing.Point(497, 35);
            this.jobDependencyEditView.Name = "jobDependencyEditView";
            this.jobDependencyEditView.ReadOnly = false;
            this.jobDependencyEditView.Size = new System.Drawing.Size(134, 352);
            this.jobDependencyEditView.TabIndex = 5;
            // 
            // jobDefinitionView
            // 
            this.jobDefinitionView.Location = new System.Drawing.Point(14, 35);
            this.jobDefinitionView.Name = "jobDefinitionView";
            this.jobDefinitionView.ReadOnly = false;
            this.jobDefinitionView.Size = new System.Drawing.Size(479, 352);
            this.jobDefinitionView.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lytGrpElementEditor});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(645, 401);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lytGrpElementEditor
            // 
            this.lytGrpElementEditor.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.lytGrpElementEditor.ExpandButtonVisible = true;
            this.lytGrpElementEditor.ExpandOnDoubleClick = true;
            this.lytGrpElementEditor.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.BeforeText;
            this.lytGrpElementEditor.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.jobDefinitionView_LayoutItem,
            this.jobDependencyDefinitionView_LayoutItem});
            this.lytGrpElementEditor.Location = new System.Drawing.Point(0, 0);
            this.lytGrpElementEditor.Name = "lytGrpElementEditor";
            this.lytGrpElementEditor.Size = new System.Drawing.Size(645, 401);
            this.lytGrpElementEditor.Text = "Properties";
            // 
            // jobDefinitionView_LayoutItem
            // 
            this.jobDefinitionView_LayoutItem.Control = this.jobDefinitionView;
            this.jobDefinitionView_LayoutItem.Location = new System.Drawing.Point(0, 0);
            this.jobDefinitionView_LayoutItem.Name = "jobDefinitionView_LayoutItem";
            this.jobDefinitionView_LayoutItem.Size = new System.Drawing.Size(483, 356);
            this.jobDefinitionView_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.jobDefinitionView_LayoutItem.TextVisible = false;
            // 
            // jobDependencyDefinitionView_LayoutItem
            // 
            this.jobDependencyDefinitionView_LayoutItem.Control = this.jobDependencyEditView;
            this.jobDependencyDefinitionView_LayoutItem.Location = new System.Drawing.Point(483, 0);
            this.jobDependencyDefinitionView_LayoutItem.Name = "jobDependencyDefinitionView_LayoutItem";
            this.jobDependencyDefinitionView_LayoutItem.Size = new System.Drawing.Size(138, 356);
            this.jobDependencyDefinitionView_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.jobDependencyDefinitionView_LayoutItem.TextVisible = false;
            // 
            // tmrRefreshJobExecutionInfo
            // 
            this.tmrRefreshJobExecutionInfo.Interval = 3000;
            this.tmrRefreshJobExecutionInfo.Tick += new System.EventHandler(this.tmrRefreshJobExecutionInfo_Tick);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.canvas);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1057, 369);
            this.splitContainerControl1.SplitterPosition = 662;
            this.splitContainerControl1.TabIndex = 16;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 98);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.layoutControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1057, 567);
            this.splitContainerControl2.SplitterPosition = 188;
            this.splitContainerControl2.TabIndex = 17;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.jobExecutionsView);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup5;
            this.layoutControl3.Size = new System.Drawing.Size(1057, 188);
            this.layoutControl3.TabIndex = 1;
            this.layoutControl3.Text = "layoutControl3";
            this.layoutControl3.GroupExpandChanging += new DevExpress.XtraLayout.Utils.LayoutGroupCancelEventHandler(this.layoutControl3_GroupExpandChanging);
            // 
            // jobExecutionsView
            // 
            this.jobExecutionsView.Location = new System.Drawing.Point(14, 35);
            this.jobExecutionsView.Name = "jobExecutionsView";
            this.jobExecutionsView.Size = new System.Drawing.Size(1029, 139);
            this.jobExecutionsView.TabIndex = 0;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lytGrpExecutions});
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(1057, 188);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // lytGrpExecutions
            // 
            this.lytGrpExecutions.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.lytGrpExecutions.ExpandButtonVisible = true;
            this.lytGrpExecutions.ExpandOnDoubleClick = true;
            this.lytGrpExecutions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.lytGrpExecutions.Location = new System.Drawing.Point(0, 0);
            this.lytGrpExecutions.Name = "lytGrpExecutions";
            this.lytGrpExecutions.Size = new System.Drawing.Size(1057, 188);
            this.lytGrpExecutions.Text = "Executions";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.jobExecutionsView;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1033, 143);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1057, 100);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1057, 100);
            this.layoutControlGroup4.Text = "Executions";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1049, 75);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            // 
            // mnuStateIndicator
            // 
            this.mnuStateIndicator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuStart,
            this.mnuStopAfterCurrentExecutingJobs});
            this.mnuStateIndicator.Name = "mnuStateIndicator";
            this.mnuStateIndicator.Size = new System.Drawing.Size(241, 48);
            // 
            // mnuStart
            // 
            this.mnuStart.Name = "mnuStart";
            this.mnuStart.Size = new System.Drawing.Size(240, 22);
            this.mnuStart.Text = "Start";
            this.mnuStart.Click += new System.EventHandler(this.mnuStart_Click);
            // 
            // mnuStopAfterCurrentExecutingJobs
            // 
            this.mnuStopAfterCurrentExecutingJobs.Name = "mnuStopAfterCurrentExecutingJobs";
            this.mnuStopAfterCurrentExecutingJobs.Size = new System.Drawing.Size(240, 22);
            this.mnuStopAfterCurrentExecutingJobs.Text = "Stop after current excuting jobs";
            this.mnuStopAfterCurrentExecutingJobs.Click += new System.EventHandler(this.mnuStopAfterCurrentExecutingJobs_Click);
            // 
            // JobDiagramView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.splitContainerControl2);
            this.Controls.Add(this.panelTop);
            this.Name = "JobDiagramView";
            this.Size = new System.Drawing.Size(1057, 665);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDailyScheduleHistoryLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tseInstanciationTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoad_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicator_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDailyScheduleHistoryLength_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.canvasRightClickMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytGrpElementEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobDefinitionView_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobDependencyDefinitionView_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytGrpExecutions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.mnuStateIndicator.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.XtraScrollableControl canvas;
        private System.Windows.Forms.ContextMenuStrip canvasRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuNewJob;
        private System.Windows.Forms.TextBox txtFocusStealer;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup lytGrpElementEditor;
        private DevExpress.XtraEditors.TimeSpanEdit tseInstanciationTime;
        private System.Windows.Forms.Timer tmrRefreshJobExecutionInfo;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.TextBox txtDiagramName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem btnLoad_LayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem btnSave_LayoutItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit txtLogPath;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton btnEditProcessShortcuts;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.ToolStripSeparator mnuLineUnderCreateNewJob;
        private System.Windows.Forms.ToolStripMenuItem mnuEnableAllTasks;
        private System.Windows.Forms.ToolStripMenuItem mnuDisableAllTasks;
        private System.Windows.Forms.ToolStripSeparator mnuUnderEnableDisableAllTasks;
        private System.Windows.Forms.ToolStripMenuItem mnuReduceAllTasks;
        private TechnicalTools.UI.Controls.StateIndicator stateIndicator;
        private DevExpress.XtraLayout.LayoutControlItem stateIndicator_LayoutItem;
        private System.Windows.Forms.ToolStripMenuItem mnuEditSchedule;
        private System.Windows.Forms.ToolTip addToolTipPropertyOnAllControl;
        private DevExpress.XtraEditors.TextEdit txtDailyScheduleHistoryLength;
        private DevExpress.XtraLayout.LayoutControlItem txtDailyScheduleHistoryLength_LayoutItem;
        private JobDefinitionView jobDefinitionView;
        private DevExpress.XtraLayout.LayoutControlItem jobDefinitionView_LayoutItem;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private JobDependencyEditView jobDependencyEditView;
        private DevExpress.XtraLayout.LayoutControlItem jobDependencyDefinitionView_LayoutItem;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private JobExecutionsView jobExecutionsView;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup lytGrpExecutions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.ContextMenuStrip mnuStateIndicator;
        private System.Windows.Forms.ToolStripMenuItem mnuStart;
        private System.Windows.Forms.ToolStripMenuItem mnuStopAfterCurrentExecutingJobs;
        private DevExpress.XtraEditors.SimpleButton btnEditSharedResources;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}
