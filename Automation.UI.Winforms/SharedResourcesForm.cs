﻿using System;

using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.Helpers;

using Automation.Model;


namespace Automation.UI.Winforms
{
    public partial class SharedResourcesForm : EnhancedXtraForm
    {
        public SharedResourceManager ResourceManager  { get; }

        [Obsolete("For designer only")]
        public SharedResourcesForm() : this(null) { }

        public SharedResourcesForm(SharedResourceManager resourceManager)
        {
            ResourceManager = resourceManager;
            InitializeComponent();
            Text = "Shared resources";
            _viewHelper = new GridViewForEditableSet<SharedResource, ISharedResource>();
        }
        readonly GridViewForEditableSet<SharedResource, ISharedResource> _viewHelper;

        void SharedResourcesForm_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            Configure_SharedResources_Edition();
        }

        void Configure_SharedResources_Edition()
        {
            _viewHelper.Install(gvSharedResource, ResourceManager);

            gcSharedResource.DataSource = ResourceManager.EditingSet;
            gvSharedResource.BestFitColumns();
        }

    }
}
